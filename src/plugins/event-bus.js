import Vue from 'vue'
export const eventBus = new Vue()

export default Vue => {
  // mount the event bus to Vue
  Object.defineProperties(Vue, {
    eventBus: { get: () => eventBus }
  })

  // mount the event bus to Vue component instance
  Object.defineProperties(Vue.prototype, {
    $eventBus: { get: () => eventBus }
  })
}

//GROUP
//syncGroupData //load all data for group, ex. group services, members etc
