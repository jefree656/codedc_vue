/**
 * My plugins
 */

import axios from './axios'
import title from './title'
import services from './services'
import nprogress from './nprogress'
import eventBus from './event-bus'
import authorize from './authorize' //check login state, retrigger login api

export default {
  install (Vue) {
    axios(Vue)
    title(Vue, { property: 'title', separator: ' « ' })
    services(Vue)
    nprogress(Vue)
    eventBus(Vue)
    authorize(Vue) //we can remove this if we don't need checking
  }
}
