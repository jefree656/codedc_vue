import {
  ClientService,
  TokenService,
  UserService,
  ServiceManager,
  BranchesService,
  ReportsService,
  GroupService,
  ServiceProfile,
  LogsService,
  Documents,
  Attendance,
  FinancingService
} from '../services'

export default Vue => {
  // alias
  const services = {
    token: TokenService,
    user: UserService,
    client: ClientService,
    service_manager: ServiceManager,
    brances: BranchesService,
    reports: ReportsService,
    groups: GroupService,
    service_profile: ServiceProfile,
    logs: LogsService,
    documents: Documents,
    attendance: Attendance,
    financing: FinancingService,
  }

  // mount the services to Vue
  Object.defineProperties(Vue, {
    services: { get: () => services }
  })

  // mount the services to Vue component instance
  Object.defineProperties(Vue.prototype, {
    $services: { get: () => services }
  })
}
