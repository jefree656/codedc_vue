// business routes
import mainRoutes from './main-routes'

/**
 * 路由表配置
 */
export default [
  // ## login page
  {
    name: 'login',
    path: '/login',
    meta: { requiresAuth: false },
    component: () => import('../views/login')
  },
  // ## main page
  {
    path: '/',
    meta: { requiresAuth: true },
    component: () => import('../views/layout'),
    children: mainRoutes
    // children: mainRoutes.concat(demoRoutes)
  },
  // ## not found page
  {
    name: 'not-found',
    path: '*',
    meta: { requiresAuth: false },
    component: () => import('../views/error')
  },

  {
    name: 'product-menu',
    path: '/product-menu',
    meta: { requiresAuth: false },
    component: () => import('../views/orders/product-menu')
  },

  {
    name: 'pay-service-qr-code',
    path: '/pay-service-qr-code/:id',
    meta: { requiresAuth: false },
    component: () => import('../views/clients/components/pay-service-qr-code')
  },
  
  ]
