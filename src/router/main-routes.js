export default [
  // - Dashboard
  {
    name: 'dashboard',
    path: '',
    meta: { requiresAuth: true },
    component: () => import('../views/dashboard/index')
  },


  {
      name: 'download-app',
      path: '#',
      meta: { requiresAuth: true },
      props(route) {
        window.open('http://api.ibet656.com/apk/fourways.apk');
      },
      component: () => import('../views/dashboard/index')
  },


  {
    name: 'today-services',
    path: 'today-services',
    meta: { requiresAuth: true },
    component: () => import('../views/dashboard/today-services')
  },

  {
    name: 'on-process-services',
    path: 'on-process-services',
    meta: { requiresAuth: true },
    component: () => import('../views/dashboard/on-process-services')
  },

  {
    name: 'pending-services',
    path: 'pending-services',
    meta: { requiresAuth: true },
    component: () => import('../views/dashboard/pending-services')
  },

  {
    name: 'cost-summary',
    path: 'cost-summary',
    meta: { requiresAuth: true },
    component: () => import('../views/dashboard/cost-summary')
  },

  {
    name: 'filed-services',
    path: 'filed-services',
    meta: { requiresAuth: true },
    component: () => import('../views/dashboard/filed-services')
  },

  {
    name: 'today-task',
    path: 'today-task',
    meta: { requiresAuth: true },
    component: () => import('../views/dashboard/today-task')
  },

  {
    name: 'tomorrow-task',
    path: 'tomorrow-task',
    meta: { requiresAuth: true },
    component: () => import('../views/dashboard/tomorrow-task')
  },

  {
    name: 'passport-expiration-reminder',
    path: 'passport-expiration-reminder',
    meta: { requiresAuth: true },
    component: () => import('../views/dashboard/passport-expiration-reminder')
  },

  {
    name: 'event-calendar',
    path: 'event-calendar',
    meta: { requiresAuth: true },
    component: () => import('../views/dashboard/event-calendar')
  },

  // End Dashboard

  // CLIENTS
  {
    name: 'clients',
    path: 'clients/:page?',
    meta: { requiresAuth: true },
    component: () => import('../views/clients/index')
  },

  {
    name: 'clients-profile',
    path: 'clients/profile/:id?/:clientServiceId?',
    meta: { requiresAuth: true },
    props(route) {
      let { id, clientServiceId} = route.params;
      return {
        id: JSON.parse(id),
        clientServiceId: (typeof clientServiceId != 'undefined') ? JSON.parse(clientServiceId) : 0
      }
    },
    component: () => import('../views/clients/profile')
  },

  {
    name: 'add-client',
    path: 'client/create',
    meta: { requiresAuth: true },
    component: () => import('../views/clients/add-client')
  },

  {
    name: 'edit-client',
    path: 'clients/profile/:id?/edit',
    meta: { requiresAuth: true },
    component: () => import('../views/clients/edit-client')
  },

  {
    name: 'client-transaction-logs',
    path: 'client/transaction-logs/:id?',
    meta: { requiresAuth: true },
    props(route) {
      let { id } = route.params;
      return {
        id: id
      }
    },
    component: () => import('../views/clients/components/transaction-logs')
  },

  {
    name: 'clients-funds-summary',
    path: 'clients/view-funds-summary/:id',
    meta: { requiresAuth: true },
    component: () => import('../views/groups/components/view-funds-summary')
  },
  // END CLIENTS

  {
    name: 'presets',
    path: 'presets/:page?',
    meta: { requiresAuth: true },
    component: () => import('../views/presets/index')
  },

  {
    name: 'presets-profile',
    path: 'presets/profile/:id?',
    meta: { requiresAuth: true },
    props(route) {
      let { id } = route.params;
      return {
        id: JSON.parse(id)
      }
    },
    component: () => import('../views/presets/preset-profile')
  },

    {
    name: 'bundles',
    path: 'bundles/:page?',
    meta: { requiresAuth: true },
    component: () => import('../views/bundles/index')
  },

  {
    name: 'bundles-profile',
    path: 'bundles/profile/:id?',
    meta: { requiresAuth: true },
    props(route) {
      let { id } = route.params;
      return {
        id: JSON.parse(id)
      }
    },
    component: () => import('../views/bundles/preset-profile')
  },

  {
    name: 'suppliers',
    path: 'suppliers/:page?',
    meta: { requiresAuth: true },
    component: () => import('../views/suppliers/index')
  },

  {
    name: 'billers',
    path: 'billers/:page?',
    meta: { requiresAuth: true },
    component: () => import('../views/billers/index')
  },

  {
    name: 'cars',
    path: 'cars/:page?',
    meta: { requiresAuth: true },
    component: () => import('../views/cars/index')
  },

  {
    name: 'banks',
    path: 'banks/:page?',
    meta: { requiresAuth: true },
    component: () => import('../views/banks/index')
  },

  {
    name: 'bank-history',
    path: 'bank/history/:id?',
    meta: { requiresAuth: true },
    props(route) {
      let { id } = route.params;
      return {
        id: JSON.parse(id)
      }
    },
    component: () => import('../views/banks/components/history')
  },

  {
    name: 'project-finance',
    path: 'financing/:page?',
    meta: { requiresAuth: true },
    component: () => import('../views/financing/project-finance')
  },

  {
    name: 'project-finance-profile',
    path: 'financing/project-profile/:id?/:nickname?/:redirect?',
    meta: { requiresAuth: true },
    props(route) {
      let { id, nickname, redirect } = route.params;
      return {
        id: JSON.parse(id),
        nickname: nickname,
        redirect
      }
    },
    component: () => import('../views/financing/project-profile')
  },

  {
    name: 'projects',
    path: 'projects/:page?',
    meta: { requiresAuth: true },
    component: () => import('../views/projects/index')
  },

  {
    name: 'projects-profile',
    path: 'projects/profile/:id?/:nickname?/:redirect?',
    meta: { requiresAuth: true },
    props(route) {
      let { id, nickname, redirect } = route.params;
      return {
        id: JSON.parse(id),
        nickname: nickname,
        redirect
      }
    },
    component: () => import('../views/projects/profile')
  },

  {
    name: 'project-task-purchase-order',
    path: 'projects/purchase-order/:projectId?/:taskId?/:redirect?',
    meta: { requiresAuth: true },
    props(route) {

      let { projectId, taskId, redirect } = route.params;

      return {
        projectId,
        taskId,
        redirect
        // redirect: JSON.parse(redirect)
      }
    },
    component: () => import('../views/projects/purchase-order')
  },

  {
    name: 'project-task-edit-purchase-order',
    path: 'projects/edit-purchase-order/:projectId?/:taskId?/:orderId?/:redirect?',
    meta: { requiresAuth: true },
    props(route) {

      let { projectId, taskId, orderId, redirect } = route.params;

      return {
        projectId,
        taskId,
        orderId,
        redirect
        // redirect: JSON.parse(redirect)
      }
    },
    component: () => import('../views/projects/edit-purchase-order')
  },

  {
    name: 'project-view-quotation',
    path: 'projects/view-quotation/:projectId?/:redirect?',
    meta: { requiresAuth: true },
    props(route) {

      let { projectId, redirect } = route.params;

      return {
        projectId,
        redirect
      }
    },
    component: () => import('../views/projects/view-quotation')
  },

  {
    name: 'groups',
    path: 'groups/:page?',
    meta: { requiresAuth: true },
    component: () => import('../views/groups/index')
  },

  {
    name: 'groups-profile',
    path: 'groups/profile/:id?/:clientServiceId?/:client_id?',
    meta: { requiresAuth: true },
    props(route) {
      let { id, clientServiceId, client_id} = route.params;
      return {
        id: id,
        clientServiceId: (typeof clientServiceId != 'undefined') ? JSON.parse(clientServiceId) : 0,
        client_id : (typeof client_id != 'undefined') ? JSON.parse(client_id) : 0,
      }
    },
    component: () => import('../views/groups/profile')
  },

  {
    name: 'groups-funds',
    path: 'groups/view-funds/:id?',
    meta: { requiresAuth: true },
    props(route) {
      let { id } = route.params;
      return {
        id: id
      }
    },
    component: () => import('../views/groups/components/view-funds')
  },

  {
    name: 'groups-funds-summary',
    path: 'groups/view-funds-summary/:id',
    meta: { requiresAuth: true },
    component: () => import('../views/groups/components/view-funds-summary')
  },

  {
    name: 'transaction-logs',
    path: 'groups/transaction-logs/:id?',
    meta: { requiresAuth: true },
    props(route) {
      let { id } = route.params;
      return {
        id: id
      }
    },
    component: () => import('../views/groups/components/transaction-logs')
  },

  /* REPORTS */
    {
      name: 'write-report',
      path: 'reports/write',
      meta: { requiresAuth: true },
      component: () => import('../views/reports/write-report')
    },

    {
      name: 'write-report-clients',
      path: 'reports/write/clients/:clientIds?',
      meta: { requiresAuth: true },
      props(route) {
        let { clientIds } = route.params;
        return {
          clientIds: JSON.parse(clientIds)
        }
      },
      component: () => import('../views/reports/write-report-clients')
    },

    {
      name: 'write-report-client-services',
      path: 'reports/write/clients/:clientIds?/client-services/:clientServiceIds?/:groupId?/:redirect?',
      meta: { requiresAuth: true },
      props(route) {

        let { clientIds, clientServiceIds, groupId, redirect } = route.params;

        return {
          clientIds: JSON.parse(clientIds),
          clientServiceIds: JSON.parse(clientServiceIds),
          groupId,
          redirect
          // redirect: JSON.parse(redirect)
        }
      },
      component: () => import('../views/reports/write-report-client-services')
    },

    {
      name: 'read-reports',
      path: 'reports/:page?',
      meta: { requiresAuth: true },
      component: () => import('../views/reports/read-reports')
    },

    {
      name: 'materials-report',
      path: 'materials-report',
      meta: { requiresAuth: true },
      component: () => import('../views/reports/materials-report')
    },

    {
      name: 'centralized-report',
      path: 'centralized-report',
      meta: { requiresAuth: true },
      component: () => import('../views/reports/centralized-report')
    },


    {
      name: 'centralized-report-documents',
      path: 'centralized-report-documents/:clientIds?/:action?/:group_id?/:redirect?',
      meta: { requiresAuth: true },
      props(route) {
        let { clientIds, action, redirect, group_id } = route.params;
        return {
          ids: JSON.parse(clientIds),
          action: action,
          group_id: group_id,
          redirect
        }
      },
      component: () => import('../views/reports/centralized-report-documents')
    },



  /* END OF REPORTS */

  {
    name: 'news',
    path: 'news/:page?',
    meta: { requiresAuth: true },
    component: () => import('../views/news/index')
  },

  {
    name: 'add-service',
    path: 'services/add',
    meta: { requiresAuth: true },
    component: () => import('../views/services/add-service')
  },

  {
    name: 'edit-service',
    path: 'services/edit/:id?',
    meta: { requiresAuth: true },
    component: () => import('../views/services/edit-service')
  },

  {
    name: 'list-of-documents',
    path: 'services/documents',
    meta: { requiresAuth: true },
    component: () => import('../views/services/list-of-documents')
  },

  {
    name: 'list-of-services',
    path: 'services/:page?',
    meta: { requiresAuth: true },
    component: () => import('../views/services/index')
  },

  {
    name: 'add-service-documents',
    path: 'services/documents/add',
    meta: { requiresAuth: true },
    component: () => import('../views/services/add-document')
  },

  {
    name: 'edit-service-documents',
    path: 'services/documents/edit/:id?',
    meta: { requiresAuth: true },
    component: () => import('../views/services/edit-document')
  },

  {
    name: 'view-received-documents',
    path: 'received-documents/:id?',
    meta: { requiresAuth: true },
    component: () => import('../views/clients/components/view-received-documents')
  },

  /* SERVICE PROCEDURES */
  {
    name: 'service-procedures',
    path: 'services/:service_id/procedures',
    meta: { requiresAuth: true },
    component: () => import('../views/service_procedures/service-procedures')
  },

  {
    name: 'add-service-procedure',
    path: 'services/:service_id/procedures/add',
    meta: { requiresAuth: true },
    component: () => import('../views/service_procedures/add-service-procedure')
  },

  {
    name: 'edit-service-procedure',
    path: 'services/:service_id/procedures/:procedure_id/edit',
    meta: { requiresAuth: true },
    component: () => import('../views/service_procedures/edit-service-procedure')
  },
  /* END OF SERVICE PROCEDURES */

  {
    name: 'branch-manager',
    path: 'branches/:page?',
    meta: { requiresAuth: true },
    component: () => import('../views/branches/index')
  },

  {
    name: 'cpanel-accounts',
    path: 'accounts/:page?',
    meta: { requiresAuth: true },
    component: () => import('../views/accounts/index')
  },

  {
    name: 'add-cpanel-account',
    path: 'account/add',
    meta: { requiresAuth: true },
    component: () => import('../views/accounts/add-cpanel-account')
  },

  {
    name: 'edit-cpanel-account',
    path: 'account/edit/:id?',
    meta: { requiresAuth: true },
    component: () => import('../views/accounts/edit-cpanel-account')
  },

  {
    name: 'documents-on-hand',
    path: 'documents-on-hand',
    meta: { requiresAuth: true },
    component: () => import('../views/documents/index')
  },

  {
    name: 'financial-monitor',
    path: 'financing/:page?',
    meta: { requiresAuth: true },
    component: () => import('../views/financing/index')
  },

  {
    name: 'gantt-chart',
    path: 'gantt-chart/:id?',
    meta: { requiresAuth: true },
    component: () => import('../views/gantt-chart/index')
  },

  {
    name: 'payments-payroll',
    path: 'payments/payroll',
    meta: { requiresAuth: true },
    component: () => import('../views/payments/payroll')
  },

  {
    name: 'payments',
    path: 'payments/orders',
    meta: { requiresAuth: true },
    component: () => import('../views/payments/index')
  },

  {
    name: 'profile',
    path: 'profile',
    meta: { requiresAuth: true },
    component: () => import('../views/user/profile')
  },

  // Temporary view
  {
    name: 'common',
    path: 'common',
    meta: { requiresAuth: true },
    component: () => import('../views/common/index')
  },

  // Attendance View
  {
    name: 'attendance monitoring',
    path: 'attendance',
    meta: { requiresAuth: true },
    component: () => import('../views/attendance/index')
  },

  // <Service Profiles>
  {
    name: 'service-profiles',
    path: 'service-profiles',
    meta: { requiresAuth: true },
    component: () => import('../views/service_profiles/index')
  },
  // <Service Profiles />

  // <Service Manager>
  {
    name: 'up-docs',
    path: 'services/uploaded-documents/list',
    meta: { requiresAuth: true },
    component: () => import('../views/services/uploaded-documents')
  },

  {
    name: 'up-doc-details',
    path: 'services/uploaded-documents/:id?',
    meta: { requiresAuth: true },
    component: () => import('../views/services/uploaded-document-details')
  },
  // <Service Manager />
  {
    name: 'orders',
    path: 'orders',
    meta: { requiresAuth: true },
    component: () => import('../views/orders/list')
  },

  {
    name: 'add-orders',
    path: 'orders/create',
    meta: { requiresAuth: true },
    component: () => import('../views/orders/order-details')
  },

  {
    name: 'update-orders',
    path: 'orders/update/:id?',
    meta: { requiresAuth: true },
    component: () => import('../views/orders/edit-order-details')
  },

  // {
  //   name: 'product-list',
  //   path: 'product/list',
  //   meta: { requiresAuth: true },
  //   component: () => import('../views/orders/product-list')
  // },

  {
    name: 'product-list',
    path: 'product/list',
    meta: { requiresAuth: true },
    component: () => import('../views/orders/product-list-new')
  },

  {
    name: 'product-add-category',
    path: 'product/category/add',
    meta: { requiresAuth: true },
    component: () => import('../views/orders/product-add-category')
  },

  {
    name: 'product-edit-category',
    path: 'product/category/edit',
    meta: { requiresAuth: true },
    component: () => import('../views/orders/product-add-category')
  },

  {
      name: 'view-order',
      path: 'order/view/:orderId?/:redirect?',
      meta: { requiresAuth: true },
      props(route) {
        let { orderId, redirect } = route.params;

        return {
          orderId: orderId,
          redirect
        }
      },
      component: () => import('../views/orders/view-order')
  },

  {
      name: 'view-order-log',
      path: 'order/view-log/:orderId?/:redirect?',
      meta: { requiresAuth: true },
      props(route) {
        let { orderId, redirect } = route.params;

        return {
          orderId: orderId,
          redirect
        }
      },
      component: () => import('../views/orders/view-order-log')
  },

  {
      name: 'order-summary',
      path: 'order/summary/:orderIds?/:redirect?',
      meta: { requiresAuth: true },
      props(route) {
        let { orderIds, redirect } = route.params;

        return {
          orderIds: orderIds,
          redirect
        }
      },
      component: () => import('../views/orders/summary')
  },

  {
    name: 'product-printview',
    path: '/product/printview',
    meta: { requiresAuth: true },
    component: () => import('../views/orders/product-printview')
  },

  // {
  //   name: 'inventory-list',
  //   path: 'inventory/list',
  //   meta: { requiresAuth: true },
  //   component: () => import('../views/inventories')
  // },

  {
    name: 'inventory-list',
    path: 'inventory/list/:id?/:status?',
    meta: { requiresAuth: true },
    props(route) {
      let { id, status } = route.params;
      return {
        id: (typeof id != 'undefined') ? JSON.parse(id) : 0,
        status : (typeof status != 'undefined') ? JSON.parse(status) : 0,
      }
    },
    component: () => import('../views/inventories')
  },

  {
    name: 'inventory-item-profile',
    path: 'inventory/list/:inv_id',
    meta: { requiresAuth: true },
    component: () => import('../views/inventories/item-profile')
  },

  // {
  //   name: 'inventory-latest',
  //   path: 'inventory/latest',
  //   meta: { requiresAuth: true },
  //   component: () => import('../views/inventories/latest')
  // },

  {
    name: "rider-list",
    path: "rider-evaluation",
    meta: { requiresAuth: true },
    component: () => import("../views/rider-evaluation")
  },

  {
    name: "rider-evaluation",
    path: "rider-evaluation/:rider_id/:month/:halfMonth/:year",
    meta: { requiresAuth: true },
    component: () => import("../views/rider-evaluation/components/evaluation")
  },

  {
    name: "rider-summary-half-month",
    path: "rider-summary-half-month/:rider_id/:month/:halfMonth/:year",
    meta: { requiresAuth: true },
    component: () => import("../views/rider-evaluation/components/summary-half-month")
  }
]

// // - Default
// {
//   name: 'default',
//   path: '',
//   meta: { requiresAuth: true },
//   redirect: { name: 'dashboard' }
// },
