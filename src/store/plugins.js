import { axios, storage } from '../utils'
import { CHANGE_SESSION, TOGGLE_SIDEBAR_COLLAPSE, MODAL_STATE, LIGHTBOX_STATE } from './mutation-types'

const storagePlugin = store => {
  // called when the store is initialized
  store.subscribe((mutation, state) => {
    // called after every mutation.
    // The mutation comes in the format of `{ type, payload }`.
    switch (mutation.type) {
      case CHANGE_SESSION:
        // save session
        storage.set('wyc_session_info', state.session)
        break
      case TOGGLE_SIDEBAR_COLLAPSE:
        // save sidebar collapse
        storage.set('wyc_sidebar_collapse', state.sidebar.collapse)
        break

      case MODAL_STATE:
        // save modal state
        storage.set('wyc_modal', state.modal.open)
       break

       case LIGHTBOX_STATE:
        // save modal state
        storage.set('wyc_lightbox', state.lightbox.open)
       break
    }
  })
}

const axiosPlugin = store => {
  // called when the store is initialized
  store.subscribe((mutation, state) => {
    // called after every mutation.
    // The mutation comes in the format of `{ type, payload }`.
    if (mutation.type !== CHANGE_SESSION) return

    // change axios default request auth token
    if (state.session && state.session.token) {
      // change axios authorization header
      axios.defaults.headers.Authorization = `Bearer ${state.session.token}`
    }
  })
}

export default [
  storagePlugin,
  axiosPlugin
]
