import { TokenService, UserService, CountriesService, BranchesService } from '../services'
import { CHANGE_SESSION, TOGGLE_SIDEBAR_COLLAPSE, MODAL_STATE, LIGHTBOX_STATE } from './mutation-types'
import { eventBus } from './../plugins/event-bus.js' // for vue components

export default {

  changeTitle: ({ commit }, title) => {
    commit(CHANGE_SESSION, { title: title })
  },

  createToken: ({ commit }, model) => {
    return TokenService.getAccessToken({
        email: model.email,
    	  password: model.password,
    	  source: model.source
    })
      .then(res => {
        console.log(res.data);
        if(res.data.status == 'Success'){
          commit(CHANGE_SESSION, { token: res.data.data.token })
        }
        return res.data
      })
  },

  /*
  *
  Check login state
  we can remove this if we don't need checking
  */

  checkToken: ({ commit, getters }) => {
    return new Promise((resolve, reject) => {
      // validate local store
      if (!getters.session.token) {
        return resolve(false)
      }
      // remote
      UserService.getCurrentUser()
        .then(res => resolve(true))
        .catch(err => {
          console.error(err)
          commit(CHANGE_SESSION, { token: null })
          resolve(false)
        })
    })
  },

  deleteToken: ({ commit, getters }) => {
    return TokenService.removeToken()
      .then(res => {
        console.log('session deleted' + getters.session.token)
        commit(CHANGE_SESSION, { token: null })
      })
  },

  setToken: ({ commit }, token) => {
        // console.log('session test' + token)
        commit(CHANGE_SESSION, { token: token })
  },

  // need to adjust, use token instead of ID
  getCurrentUser: ({ commit }) => {
    return UserService.getCurrentUser()
      .then(res => {
        commit(CHANGE_SESSION, { user: res.data.data.information })
        return res.data.data.information
      })
  },

  toggleSidebarCollapse: ({ commit }) => {
    commit(TOGGLE_SIDEBAR_COLLAPSE)
  },

  callModal: ({ commit }, data, self) => {
    commit(MODAL_STATE, { data })
    eventBus.$emit('openModal')
  },

  callLightbox: ({ commit }, data, self) => {
    commit(LIGHTBOX_STATE, { data })
    eventBus.$emit('openLightbox')
  },

  getAllCountries: () => {
    return CountriesService.getAllCountries()
      .then(res => {
        return res.data.data.countries
      })
      .catch(err => console.log(err))
  },

  getAllNationalities: () => {
    return CountriesService.getAllNationalities()
      .then(res => {
        return res.data.data.nationalities
      })
      .catch(err => console.log(err))
  },

  getAllBranches: () => {
    return BranchesService.getBranches()
      .then(res => {
        return res.data.data.branches
      })
      .catch(err => console.log(err))
  },

  // Only numbers allowed
  numbersOnly: ({ commit }, event) => {
    var charCode = (event.which) ? event.which : event.keyCode

    if ((charCode > 31 && (charCode < 48 || charCode > 57)) && charCode !== 43) {
      event.preventDefault()
    } else {
      return true
    }
  }

}
