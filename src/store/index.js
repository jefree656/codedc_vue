/**
 * Vuex docs
 * https://vuex.vuejs.org/zh-cn
 * https://github.com/vuejs/vue-hackernews-2.0
 */

import Vue from 'vue'
import Vuex from 'vuex'

// // Make sure state writeable

import state from './state' // for menu
import getters from './getters' // states of title, session, sidebar
import mutations from './mutations' // assigning of state or chainging of state like reducer //mutations of sidebar, session
import actions from './actions' // actions like fetching of data //changeTitle
import modules from './modules' // directory of setters getters and actions
import plugins from './plugins' // reducer of plugins// change all state

Vue.use(Vuex)

const strict = process.env.NODE_ENV !== 'production'

const store = new Vuex.Store({ state, getters, mutations, actions, modules, plugins, strict })

// ## Hot module replacement
if (module.hot) {
  module.hot.accept([
    './getters',
    './mutations',
    './actions',
    './modules/options',
    './modules/users',
    './modules/groups',
    './modules/employee',
    './modules/clients',
    './modules/service-manager',
    './modules/reports',
    './modules/service-profiles',
    './modules/branches',
    './modules/actions',
    './modules/service-procedures',
    './modules/logs',
    './modules/documents',
    './modules/accounts',
    './modules/roles',
    './modules/attendance',
    './modules/departments',
    './modules/financing',
    './modules/form',
    './modules/orders',
    './modules/inventories',
    './modules/suppliers',
  ], () => {
    store.hotUpdate({
      getters: require('./getters'),
      mutations: require('./mutations'),
      actions: require('./actions'),
      modules: {
        options: require('./modules/options'),
        users: require('./modules/users'),
        groups: require('./modules/groups'),
        employee: require('./modules/employee'),
        clients: require('./modules/clients'),
        service_manager: require('./modules/service-manager'),
        reports: require('./modules/reports'),
        service_profiles: require('./modules/service-profiles'),
        branches: require('./modules/branches'),
        actions: require('./modules/actions'),
        service_procedures: require('./modules/service-procedures'),
        logs: require('./modules/logs'),
        documents: require('./modules/documents'),
        accounts: require('./modules/accounts'),
        roles: require('./modules/roles'),
        attendance: require('./modules/attendance'),
        departments: require('./modules/departments'),
        financing: require('./modules/financing'),
        form: require('./modules/form'),
        orders: require('./modules/orders'),
        inventories: require('./modules/inventories'),
        suppliers: require('./modules/suppliers'),
      }
    })
  })
}

export default store
