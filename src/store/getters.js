export default {

  title: state => state.title,

  session: state => state.session,

  header: state => state.header,

  sidebar: state => state.sidebar,

  modal: state => state.modal,

  lightbox: state => state.lightbox,

  months: state => state.months

}
