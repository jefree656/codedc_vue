import { CHANGE_SESSION, TOGGLE_SIDEBAR_COLLAPSE, MODAL_STATE, LIGHTBOX_STATE } from './mutation-types'

export default {

  [CHANGE_SESSION]: (state, session) => {
    // TODO: new session mixin
    Object.assign(state.session, session)
  },

  [TOGGLE_SIDEBAR_COLLAPSE]: state => {
    state.sidebar.collapse = !state.sidebar.collapse
  },

  [MODAL_STATE]: (state, session)=> {
      Object.assign(state.modal, session)
  },

  [LIGHTBOX_STATE]: (state, session)=> {
      Object.assign(state.lightbox, session)
  }

}
