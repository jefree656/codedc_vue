import { Attendance } from '../../services'

const state = {
  
}

const getters = {
  
}

const mutations = {

}

const actions = {

    getAllAttendance: ({ commit }, model) => {
        return Attendance.getAllAttendance(model)
            .then(res => {
                return res.data
        })
    },

    generatePayroll: ({ commit }, model) => {
        return Attendance.generatePayroll(model)
            .then(res => {
                return res.data
        })
    },

    computePayroll: ({ commit }, model) => {
        return Attendance.computePayroll(model)
            .then(res => {
                return res.data
        })
    },
    
    testKernel: ({ commit }, model) => {
        return Attendance.testKernel(model)
            .then(res => {
                return res.data
        })
    },
}

export default { state, getters, mutations, actions }
