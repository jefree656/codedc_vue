import { Branch } from '../../services'

const state = {
  branches: {}
}

const getters = {
  branches: state => state.branches
}

const mutations = {

}

const actions = {

  getBranches: ({ commit }, model) => {
    return Branch.getBranches(model)
      .then(res => {
        return res.data
      })
  },

  getBranch: ({ commit }, model) => {
    return Branch.getBranch(model)
      .then(res => {
        return res.data
      })
  },

  addBranch: ({ commit }, model) => {
    return Branch.addBranch(model)
      .then(res => {
        return res.data
      })
  },

  updateBranch: ({ commit }, model) => {
    return Branch.updateBranch(model)
      .then(res => {
        return res.data
      })
  },

  deleteBranch: ({ commit }, data) => {
    return Branch.deleteBranch(data)
      .then(res => {
        return res.data
      })
  }

}

export default { state, getters, mutations, actions }
