import { Documents } from '../../services'
/**
 * Initial state
 */
const state = {
    documents: {}
}

/**
 * Getters
 */
const getters = {
    documents: state => state.documents
}

/**
 * Mutations
 */
const mutations = {
    CHANGE_CLIENT_DOCS_DATA: (state, documents) => {
        Object.assign(state.documents, documents)
    }
}

/**
 * Actions
 */
const actions = {

    uploadDocuments ({ commit }, model) {
        return Documents.uploadDocuments(model)
            .then(res => {
                return res.data;
            });
    },

    getUploadedClientDocuments ({ commit }, id) {
        return Documents.getUploadedClientDocuments(id)
            .then(res => {
                return res.data;
            });
    },

    getDocumentTypeList ({ commit }) {
        return Documents.getDocumentTypeList()
            .then(res => {
                return res.data;
            })
    },

    uploadDocumentsByClient ({ commit }, model) {
        return Documents.uploadDocumentsByClient(model)
            .then(res => {
                return res.data;
            })
    },

    // uploadPaymentDocuments ({ commit }, model) {
    //     return Documents.uploadPaymentDocuments(model)
    //         .then(res => {
    //             return res.data;
    //         })
    // },

    getDocsByClient: async ({ commit }, model) => {
        return Documents.getDocsByClient(model)
        .then(res => {
            commit('CHANGE_CLIENT_DOCS_DATA', { client_docs_list: res.data.data })
            return res.data;
        })
    },

    deleteDocsByClient: async ({ commit }, model) => {
        return Documents.deleteDocsByClient(model)
        .then(res => {
            return res.data;
        })
    },

    getDocumentsOnHand ({ commit }, model) {
        return Documents.getDocumentsOnHand(model)
            .then(res => {
                return res.data;
            })
    }

}

// Export module
export default { state, getters, mutations, actions }
