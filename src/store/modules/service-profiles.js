import { ServiceProfile } from '../../services'

const state = {
  service_profiles: {}
}

const getters = {
  service_profiles: state => state.service_profiles
}

const mutations = {

}

const actions = {

  getAllServiceProfiles: ({ commit }, model) => {
    return ServiceProfile.getAllServiceProfiles(model)
      .then(res => {
        return res.data
      })
  },

  getServiceProfile: ({ commit }, model) => {
    return ServiceProfile.getServiceProfile(model)
      .then(res => {
        return res.data
      })
  },

  addServiceProfile: ({ commit }, model) => {
    return ServiceProfile.addServiceProfile(model)
      .then(res => {
        return res.data
      })
  },

  updateServiceProfile: ({ commit }, model) => {
    return ServiceProfile.updateServiceProfile(model)
      .then(res => {
        return res.data
      })
  },

  deleteServiceProfile: ({ commit }, data) => {
    return ServiceProfile.deleteServiceProfile(data)
      .then(res => {
        return res.data
      })
  },

  getClientsGroups: ({ commit }, data) => {
    return ServiceProfile.getClientsGroups(data)
      .then(res => {
        return res.data
      })
  }

}

export default { state, getters, mutations, actions }
