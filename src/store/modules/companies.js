import { CompanyService } from '../../services'

const state = {}

/**
 * Getters
 */
const getters = {}

/**
 * Mutations
 */
const mutations = {}

const actions = {

  getCompany: ({ commit }, options) => {
    return CompanyService.getCompany(options)
      .then(res => {
        return res.data
      })
  },

  addCompany: ({ commit }, options) => {
    return CompanyService.addCompany(options)
      .then(res => {
        return res.data
      })
  },

  editCompany: ({ commit }, options) => {
    return CompanyService.editCompany(options)
      .then(res => {
        return res.data
      })
  },

  deleteCompany: ({ commit }, options) => {
    return CompanyService.deleteCompany(options)
      .then(res => {
        return res.data
      })
  },

}

// Export module
export default {
  state,
  getters,
  mutations,
  actions
}
