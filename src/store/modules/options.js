/**
 * Initial state
 */
const state = {
  options: {}
}

/**
 * Getters
 */
const getters = {
  options: state => state.options
}

/**
 * Mutations
 */
const mutations = {
  CHANGE_OPTIONS: (state, options) => {
    Object.assign(state.options, options)
  }
}

/**
 * Actions
 */
const actions = {
  changeOptions: async ({ commit }, options) => {
    commit('CHANGE_OPTIONS', options)
  }
}

// Export module
export default { state, getters, mutations, actions }
