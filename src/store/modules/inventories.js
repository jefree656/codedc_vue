import { InventoryService } from '../../services'

const state = {

}

/**
 * Getters
 */
const getters = {}

/**
 * Mutations
 */
const mutations = {

}

const actions = {

  getInventory: async ({ commit }, options) => {
    return InventoryService.getInventory(options)
      .then(res => {
        return res;
      })
  },

  checkOrderDetailQty: async ({ commit }, data) => {
      return InventoryService.checkOrderDetailQty(data.payload)
        .then(res => {
          return res.data
        })
  },  

  checkInventoryQty: async ({ commit }, data) => {
      return InventoryService.checkInventoryQty(data.payload)
        .then(res => {
          return res.data
        })
  },

  checkInventoryQty2: async ({ commit }, data) => {
      return InventoryService.checkInventoryQty2(data.payload)
        .then(res => {
          return res.data
        })
  },

  loadToContainer: async ({ commit }, data) => {
      return InventoryService.loadToContainer(data.payload)
        .then(res => {
          return res.data
        })
  },

  saveToContainer: async ({ commit }, data) => {
      return InventoryService.saveToContainer(data.payload)
        .then(res => {
          return res.data
        })
  },

  revertSPC: ({ commit }, model) => {
    return InventoryService.revertSPC(model)
      .then(res => {
        return res.data;
      })
  },

  deleteSiteOrder: ({ commit }, model) => {
    return InventoryService.deleteSiteOrder(model)
      .then(res => {
        return res.data;
      })
  },

  rcvChina: async ({ commit }, data) => {
      return InventoryService.rcvChina(data.payload)
        .then(res => {
          return res.data
        })
  },

  toPayment: async ({ commit }, data) => {
      return InventoryService.toPayment(data.payload)
        .then(res => {
          return res.data
        })
  },

  toPrep: async ({ commit }, data) => {
      return InventoryService.toPrep(data.payload)
        .then(res => {
          return res.data
        })
  },

  toShip: async ({ commit }, data) => {
      return InventoryService.toShip(data.payload)
        .then(res => {
          return res.data
        })
  },

  toRcv: async ({ commit }, data) => {
      return InventoryService.toRcv(data.payload)
        .then(res => {
          return res.data
        })
  },  

  transferDelete: async ({ commit }, data) => {
      return InventoryService.transferDelete(data.payload)
        .then(res => {
          return res.data
        })
  },

  transferToRcv: async ({ commit }, data) => {
      return InventoryService.transferToRcv(data.payload)
        .then(res => {
          return res.data
        })
  },  

  freeToRcv: async ({ commit }, data) => {
      return InventoryService.freeToRcv(data.payload)
        .then(res => {
          return res.data
        })
  },  

  freetransferRcv: async ({ commit }, data) => {
      return InventoryService.freetransferRcv(data.payload)
        .then(res => {
          return res.data
        })
  },

  markAsRcv: async ({ commit }, data) => {
      return InventoryService.markAsRcv(data.payload)
        .then(res => {
          return res.data
        })
  },

  getSiteOrders: async ({ commit }, options) => {
    return InventoryService.getSiteOrders(options)
      .then(res => {
        return res;
      })
  },

  getToPrepOrders: async ({ commit }, options) => {
    return InventoryService.getToPrepOrders(options)
      .then(res => {
        return res;
      })
  },

  getToShipOrders: async ({ commit }, options) => {
    return InventoryService.getToShipOrders(options)
      .then(res => {
        return res;
      })
  },

  getToShipTransfers: async ({ commit }, options) => {
    return InventoryService.getToShipTransfers(options)
      .then(res => {
        return res;
      })
  },

  getToShipFreeTransfers: async ({ commit }, options) => {
    return InventoryService.getToShipFreeTransfers(options)
      .then(res => {
        return res;
      })
  },  

  getToRcvFreeTransfers: async ({ commit }, options) => {
    return InventoryService.getToRcvFreeTransfers(options)
      .then(res => {
        return res;
      })
  },
  
  getPocPayments: async ({ commit }, options) => {
    return InventoryService.getPocPayments(options)
      .then(res => {
        return res;
      })
  },

  getToRcvOrders: async ({ commit }, options) => {
    return InventoryService.getToRcvOrders(options)
      .then(res => {
        return res;
      })
  },  

  getPurchaseOrders: async ({ commit }, options) => {
    return InventoryService.getPurchaseOrders(options)
      .then(res => {
        return res;
      })
  },

  exportProjectQuotation: async ({ commit }, options) => {
    return InventoryService.exportProjectQuotation(options)
      .then(res => {
        return res.data
      })
  },  

  exportPoc: async ({ commit }, options) => {
    return InventoryService.exportPoc(options)
      .then(res => {
        return res.data
      })
  },

  exportPoc2: async ({ commit }, options) => {
    return InventoryService.exportPoc2(options)
      .then(res => {
        return res.data
      })
  },

  // exportExcelGroupSummary: async ({ commit }, options) => {
  //   return GroupService.exportExcelGroupSummary(options)
  //     .then(res => {
  //           return res.data;
  //     })
  // },

  getSpcOrders: async ({ commit }, options) => {
    return InventoryService.getSpcOrders(options)
      .then(res => {
        return res;
      })
  },

  getCompletedOrders: async ({ commit }, options) => {
    return InventoryService.getCompletedOrders(options)
      .then(res => {
        return res;
      })
  },

  getAlllOrders: async ({ commit }, options) => {
    return InventoryService.getAlllOrders(options)
      .then(res => {
        return res;
      })
  },

  getAllCategories: async ({ commit }, options) => {
    return InventoryService.getAllCategories(options)
      .then(res => {
        return res;
      })
  },

  getAllDrivers: async ({ commit }, options) => {
    return InventoryService.getAllDrivers(options)
      .then(res => {
        return res;
      })
  },

  getAllCars: async ({ commit }, options) => {
    return InventoryService.getAllCars(options)
      .then(res => {
        return res;
      })
  },

  getAllBanks: async ({ commit }, options) => {
    return InventoryService.getAllBanks(options)
      .then(res => {
        return res;
      })
  },

  getAllBillers: async ({ commit }, options) => {
    return InventoryService.getAllBillers(options)
      .then(res => {
        return res;
      })
  },

  addeditMaterial: async ({ commit }, data) => {
    if (data.method === 'add') {
      return InventoryService.addMaterial(data.payload)
        .then(res => {
          // commit('CHANGE_GROUP_DATA', null)
          return res.data
        })
    } else {
      return InventoryService.editMaterial(data.payload.id, data.payload)
        .then(res => {
          return res.data
        })
    }
  },

  editOrder: async ({ commit }, data) => {
    return InventoryService.editOrder(data.payload.id, data.payload)
        .then(res => {
          return res.data
        })
  },

  addeditPurchaseOrder: async ({ commit }, data) => {
    if (data.method === 'add') {
      return InventoryService.newPurchaseOrder(data.payload)
        .then(res => {
          // commit('CHANGE_GROUP_DATA', null)
          return res.data
        })
    } else {
      return InventoryService.editPurchaseOrder(data.payload.id, data.payload)
        .then(res => {
          return res.data
        })
    }
  },

  addeditPurchaseOrderChina: async ({ commit }, data) => {
    if (data.method === 'add') {
      return InventoryService.newPurchaseOrderChina(data.payload)
        .then(res => {
          // commit('CHANGE_GROUP_DATA', null)
          return res.data
        })
    } else {
      return InventoryService.editPurchaseOrderChina(data.payload.id, data.payload)
        .then(res => {
          return res.data
        })
    }
  },

  transferMaterials: async ({ commit }, data) => {
      return InventoryService.transferMaterials(data.payload)
        .then(res => {
          return res.data
        })
  },

  transferToShip: async ({ commit }, data) => {
      return InventoryService.transferToShip(data.payload)
        .then(res => {
          return res.data
        })
  },

  getPurchaseOrderDetails: ({ commit }, model) => {
    var id = model.id;
    var clientOptions = {};
    return InventoryService.getPurchaseOrderDetails(id, clientOptions)
      .then(res => {
        return res.data
      })
  },

  getPoDetails: ({ commit }, data) => {
    return InventoryService.getPoDetails(data.payload)
      .then(res => {
        return res.data
      })
  },

  getPaymentDetails: ({ commit }, model) => {
    var id = model.id;
    var clientOptions = {};
    return InventoryService.getPaymentDetails(id, clientOptions)
      .then(res => {
        return res.data
      })
  },

  getPaymentDetails2: ({ commit }, model) => {
    var id = model.id;
    var clientOptions = {};
    return InventoryService.getPaymentDetails2(id, clientOptions)
      .then(res => {
        return res.data
      })
  },

  getAllDrafts: async ({ commit }, options) => {
    return InventoryService.getAllDrafts(options)
      .then(res => {
        return res;
      })
  },

  getAllPaidPoc: async ({ commit }, options) => {
    return InventoryService.getAllPaidPoc(options)
      .then(res => {
        return res;
      })
  },  

  getAllContainers: async ({ commit }, options) => {
    return InventoryService.getAllContainers(options)
      .then(res => {
        return res;
      })
  },

  getAllContainers2: async ({ commit }, options) => {
    return InventoryService.getAllContainers2(options)
      .then(res => {
        return res;
      })
  },

  getAllPayments: async ({ commit }, options) => {
    return InventoryService.getAllPayments(options)
      .then(res => {
        return res;
      })
  },  

  getBankHistory: async ({ commit }, options) => {
    return InventoryService.getBankHistory(options)
      .then(res => {
        return res;
      })
  },

  getAllPaymentCategories: async ({ commit }, options) => {
    return InventoryService.getAllPaymentCategories(options)
      .then(res => {
        return res;
      })
  },

  changePaymentCategory: async ({ commit }, data) => {
    return InventoryService.changePaymentCategory(data)
      .then(res => {
        return res.data
      })
  },

  approveMaterial: ({ commit }, model) => {
    return InventoryService.approveMaterial(model.id)
      .then(res => {
        return res.data;
      })
  },

  approveBudget: ({ commit }, model) => {
    return InventoryService.approveBudget(model.id)
      .then(res => {
        return res.data;
      })
  },

  rcvPurchaseOrder: ({ commit }, model) => {
    // console.log(model);
    return InventoryService.rcvPurchaseOrder(model.payload)
      .then(res => {
        return res.data;
      })
  },

  rcvSpc: ({ commit }, model) => {
    // console.log(model);
    return InventoryService.rcvSpc(model.payload)
      .then(res => {
        return res.data;
      })
  },  

  rcvPoc: ({ commit }, model) => {
    // console.log(model);
    return InventoryService.rcvPoc(model.payload)
      .then(res => {
        return res.data;
      })
  },

  rcvTransfer: ({ commit }, model) => {
    // console.log(model);
    return InventoryService.rcvTransfer(model.payload)
      .then(res => {
        return res.data;
      })
  },

  addEditPayment: async ({ commit }, data) => {
    if (data.method === 'add') {
      return InventoryService.addPayment(data.payload)
        .then(res => {
          // commit('CHANGE_GROUP_DATA', null)
          return res.data
        })
    } else {
      return InventoryService.editPayment(data.payload.id, data.payload)
        .then(res => {
          return res.data
        })
    }
  },  

  addEditPaymentMultipleBank: async ({ commit }, data) => {
      return InventoryService.addPaymentMultipleBank(data.payload)
        .then(res => {
          // commit('CHANGE_GROUP_DATA', null)
          return res.data
        })
  },

  addEditPayment2: async ({ commit }, data) => {
    if (data.method === 'add') {
      return InventoryService.addPayment2(data.payload)
        .then(res => {
          // commit('CHANGE_GROUP_DATA', null)
          return res.data
        })
    } else {
      return InventoryService.editPayment2(data.payload.id, data.payload)
        .then(res => {
          return res.data
        })
    }
  },

  addFreeTransfer: async ({ commit }, data) => {
      return InventoryService.addFreeTransfer(data.payload)
        .then(res => {
          // commit('CHANGE_GROUP_DATA', null)
          return res.data
        })
  },

  uploadPaymentDocuments ({ commit }, model) {
    return InventoryService.uploadPaymentDocuments(model)
        .then(res => {
            return res.data;
        })
  },

  uploadFileDocuments ({ commit }, model) {
    return InventoryService.uploadFileDocuments(model)
        .then(res => {
            return res.data;
        })
  },  

  uploadPaymentsFileDocuments ({ commit }, model) {
    return InventoryService.uploadPaymentsFileDocuments(model)
        .then(res => {
            return res.data;
        })
  },

  receiveMaterials ({ commit }, model) {
    return InventoryService.receiveMaterials(model)
        .then(res => {
            return res.data;
        })
  },  

  toShipMaterials ({ commit }, model) {
    return InventoryService.toShipMaterials(model)
        .then(res => {
            return res.data;
        })
  },

  changeCategory: async ({ commit }, data) => {
    return InventoryService.changeCategory(data)
      .then(res => {
        return res.data
      })
  },

  changeUser: async ({ commit }, data) => {
    return InventoryService.changeUser(data)
      .then(res => {
        return res.data
      })
  },

  updateOrderDetail: async ({ commit }, data) => {

      return InventoryService.updateOrderDetail(data.payload)
        .then(res => {
          return res.data
        })
    
  },  
  updateContainerDetail: async ({ commit }, data) => {

      return InventoryService.updateContainerDetail(data.payload)
        .then(res => {
          return res.data
        })
    
  },

  updateBankTransfer: async ({ commit }, data) => {

      return InventoryService.updateBankTransfer(data.payload)
        .then(res => {
          return res.data
        })
    
  },
}

// Export module
export default {
  state,
  getters,
  mutations,
  actions
}
