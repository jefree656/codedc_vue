import { RolesService } from '../../services'
/**
 * Initial state
 */
const state = {
  roles: {}
}

/**
 * Getters
 */
const getters = {
  roles: state => state.roles
}

/**
 * Mutations
 */
const mutations = {

}

/**
 * Actions
 */
const actions = {
   	getRoles: ({ commit }) => {
    return RolesService.getRoles()
      .then(res => {
        return res.data
      })
  	},

  	getPermissions: ({ commit }) => {
    return RolesService.getPermissions()
      .then(res => {
        return res.data
      })
  	},

  getRole: ({ commit },options) => {
    return RolesService.getRole(options)
      .then(res => {
        return res.data
      })
  },

  addRole: ({ commit }, options) => {
    return RolesService.addRole(options)
      .then(res => {
        return res.data
      })
  },

  getPermissionsByRole: ({ commit }, data) => {
    return RolesService.getPermissionsByRole(data)
      .then(res => {
        return res.data
      })
  },

  updateRoleAccess: ({ commit }, options) => {
    return RolesService.updateRoleAccess(options)
      .then(res => {
        return res.data
      })
  },

}

// Export module
export default { state, getters, mutations, actions }
