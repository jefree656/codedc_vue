import { PresetService, ClientService } from '../../services';
/**
 * Initial state
 */
const state = {
  presets: {}
}

/**
 * Getters
 */
const getters = {
  presets: state => state.presets
}

/**
 * Mutations
 */
const mutations = {

  CHANGE_PRESET_DATA: (state, presets) => {
    Object.assign(state.presets, presets)
  }

}

/**
 * Actions
 */
const actions = {

  changePresetData: async ({ commit }, groups) => {
    commit('CHANGE_PRESET_DATA', presets)
  },

  loadPresetData: async ({ commit }, options) => {
    return PresetService.getManagePresets(options)
      .then(res => {
        commit('CHANGE_PRESET_DATA', { preset_list: res.data })
        return res.data
      })
  },

  loadBundleData: async ({ commit }, options) => {
    return PresetService.getAllBundles(options)
      .then(res => {
        // commit('CHANGE_PRESET_DATA', { preset_list: res.data })
        return res.data
      })
  },

  getAllMaterials: ({ commit }, data) => {
    return PresetService.getAllMaterials(data.payload)
      .then(res => {
        return res.data
      })
  },

  loadAllPresets: ({ commit }, data) => {
    return PresetService.loadAllPresets(data.payload)
      .then(res => {
        return res.data
      })
  },

  loadPresetsSelected: ({ commit }, data) => {
    return PresetService.loadPresetsSelected(data.payload)
      .then(res => {
        return res.data
      })
  },  

  getSelectedMaterials: ({ commit }, data) => {
    return PresetService.getSelectedMaterials(data.payload)
      .then(res => {
        return res.data
      })
  },

  getBundlePresets: ({ commit }, data) => {
    return PresetService.getBundlePresets(data.payload)
      .then(res => {
        return res.data
      })
  },
  
  updatePresetMaterials: ({ commit }, data) => {
    return PresetService.updatePresetMaterials(data.payload)
      .then(res => {
        return res.data
      })
  },  

  updateBundlePresets: ({ commit }, data) => {
    return PresetService.updateBundlePresets(data.payload)
      .then(res => {
        return res.data
      })
  },

  // loadSearchResults: async ({ commit }, options) => {
  //   return PresetService.presetSearch(options)
  //     .then(res => {
  //       return res.data.data
  //     })
  // },

  addeditPreset: async ({ commit }, data) => {
    if (data.method === 'add') {
      return PresetService.addPreset(data.payload)
        .then(res => {
          // commit('CHANGE_GROUP_DATA', null)
          return res.data
        })
    } else {
      return PresetService.editPreset(data.payload.id, data.payload)
        .then(res => {
          return res.data
        })
    }
  },

  addeditBundle: async ({ commit }, data) => {
    if (data.method === 'add') {
      return PresetService.addBundle(data.payload)
        .then(res => {
          // commit('CHANGE_GROUP_DATA', null)
          return res.data
        })
    } else {
      return PresetService.editBundle(data.payload.id, data.payload)
        .then(res => {
          return res.data
        })
    }
  },



}

// Export module
export default { state, getters, mutations, actions }
