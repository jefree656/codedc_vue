import { ServiceManager } from '../../services'
/**
 * Initial state
 */
const state = {
  services: {},
  pending_services: {},
  monthly_services: {}
}

/**
 * Getters
 */
const getters = {
  services: state => state.services,
  services: state => state.pending_services,
  services: state => state.monthly_services,
}

/**
 * Mutations
 */
const mutations = {
  CHANGE_SERVICE_DATA: (state, services) => {
    Object.assign(state.services, services)
  },

  CHANGE_PENDING_SERVICE_DATA: (state, pending_services) => {
    Object.assign(state.pending_services, pending_services)
  },

  CHANGE_SUMMARY_DATA: (state, monthly_services) => {
    Object.assign(state.monthly_services, monthly_services)
  }
}

/**
 * Actions
 */
const actions = {
  getAllServices: ({ commit }, model) => {
    return ServiceManager.getAllServices(model)
      .then(res => {
        return res.data
      })
  },

  getAllParentServices: ({ commit }, model) => {
    return ServiceManager.getAllParentServices(model)
      .then(res => {
        return res.data
      })
  },

  addService: ({ commit }, model) => {
    return ServiceManager.addService(model)
	  .then(res => {
	    return res.data
	  })
  },

  getService: ({ commit }, model) => {
    return ServiceManager.getService(model)
      .then(res => {
        return res.data
      })
  },

  getServiceRate: ({ commit }, model) => {
    return ServiceManager.getServiceRate(model)
      .then(res => {
        return res.data
      })
  },

  updateService: ({ commit }, model) => {
    return ServiceManager.updateService(model)
      .then(res => {
        return res.data
      })
  },

  deleteService: ({ commit }, model) => {
    return ServiceManager.deleteService(model)
      .then(res => {
        return res.data
      })
  },

  // POST add service documents
  loadAllDocuments: ({ commit }) => {
    return ServiceManager.getAllDocuments()
	  .then(res => {
	    return res.data.data
	  })
  },
  // POST add service documents
  addDocument: ({ commit }, model) => {
    return ServiceManager.addDocument(model)
	  .then(res => {
	    return res.data
	  })
  },

  // get document details
  getDocument: async ({ commit }, data) => {
	 return ServiceManager.getDocument(data)
	   .then(res => {
	    return res.data.data
	  })
  },

  // update document details
  updateDocument: ({ commit }, model) => {
	 return ServiceManager.updateDocument(model)
	   .then(res => {
	   		return res.data
	  })
  },

  // delete service document
  deleteDocument: ({ commit }, data) => {
    return ServiceManager.deleteDocument(data)
      .then(res => {
        return res.data
      })
  },

  getMonthSummary: ({ commit }, options) => {
    return ServiceManager.getMonthSummary(options).then(res => {
      return res.data;
    });
  },

  getMonthlySummary: ({ commit }, model) => {
    return ServiceManager.getMonthlySummary(model)
      .then(res => {
        commit('CHANGE_SUMMARY_DATA', { client_list: res.data.data })
        return res.data
      })
  },

  getPendingServices: ({ commit }, model) => {
    return ServiceManager.getPendingServices(model)
      .then(res => {
        commit('CHANGE_PENDING_SERVICE_DATA', { client_list: res.data.data })
        return res.data
      })
  },

  getOnProcessServices: ({ commit }, model) => {
    return ServiceManager.getOnProcessServices(model)
      .then(res => {
        return res.data
      })
  },

  getDashboardStatistics: ({ commit }) => {
    return ServiceManager.getDashboardStatistics()
      .then(res => {
        return res.data
      })
  },

  getTodayServices: ({ commit }, model) => {
    return ServiceManager.getTodayServices(model)
      .then(res => {
        return res.data
      })
  },

  getClientDocuments: ({ commit }, model) => {
    return ServiceManager.getClientDocuments(model)
      .then(res => {
        return res.data
      })
  },

  getClientDocumentDetails: ({ commit }, id) => {
    return ServiceManager.getClientDocumentDetails(id)
      .then(res => {
        return res.data
      })
  },

  updateClientDocuments: ({ commit }, model) => {
    return ServiceManager.updateClientDocuments({ name: model.name }, model.id)
      .then(res => {
        return res.data
      })
  },

  addClientDocuments: ({ commit }, model) => {
    return ServiceManager.addClientDocuments(model)
      .then(res => {
        return res.data
      })
  },

  saveBreakdowns: ({ commit }, model) => {
    return ServiceManager.saveBreakdowns(model)
      .then(res => {
        return res.data
      })
  },

  updatePrice: ({ commit }, model) => {
    return ServiceManager.updatePrice(model)
      .then(res => {
        return res.data
      })
  },

  profileSwitch: ({ commit }, model) => {
    return ServiceManager.profileSwitch(model)
      .then(res => {
        return res.data
      })
  },

  getServiceProfilesDetails: ({ commit }, model) => {
    return ServiceManager.getServiceProfilesDetails(model)
      .then(res => {
        return res.data
      })
  },

  getExpandedDetails: ({ commit }, model) => {
    return ServiceManager.getExpandedDetails(model)
      .then(res => {
        return res.data
      })
  }
}

// Export module
export default { state, getters, mutations, actions }
