import { Form } from '../../services'

const state = {
  forms: {}
}

const getters = {
  forms: state => state.forms
}

const mutations = {

}

const actions = {

  getForms: ({ commit }, model) => {
    return Form.getForms(model)
      .then(res => {
        return res.data
      })
  }

}

export default { state, getters, mutations, actions }
