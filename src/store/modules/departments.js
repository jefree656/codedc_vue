import { DepartmentsService } from '../../services'
/**
 * Initial state
 */
const state = {
  departments: {}
}

/**
 * Getters
 */
const getters = {
  departments: state => state.departments
}

/**
 * Mutations
 */
const mutations = {

  CHANGE_CLIENT_DATA: (state, departments) => {
    Object.assign(state.departments, departments)
  }

}

/**
 * Actions
 */
const actions = {

    getAllDepartments: async ({ commit }) => {

    return DepartmentsService.getAllDepartments()
      .then(res => {
        return res.data
      });

  },
}

// Export module
export default { state, getters, mutations, actions }
