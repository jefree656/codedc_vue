import { Action } from '../../services'

const state = {
  actions: {}
}

const getters = {
  actions: state => state.actions
}

const mutations = {

}

const actions = {

  getActions: ({ commit }, model) => {
    return Action.getActions(model)
      .then(res => {
        return res.data
      })
  }

}

export default { state, getters, mutations, actions }
