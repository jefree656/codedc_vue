import { SupplierService, ClientService } from '../../services';
/**
 * Initial state
 */
const state = {
  suppliers: {}
}

/**
 * Getters
 */
const getters = {
  suppliers: state => state.suppliers
}

/**
 * Mutations
 */
const mutations = {

  CHANGE_SUPPLIER_DATA: (state, suppliers) => {
    Object.assign(state.suppliers, suppliers)
  }

}

/**
 * Actions
 */
const actions = {

  changeSupplierData: async ({ commit }, groups) => {
    commit('CHANGE_SUPPLIER_DATA', suppliers)
  },

  loadSupplierData: async ({ commit }, options) => {
    return SupplierService.getManageSuppliers(options)
      .then(res => {
        commit('CHANGE_SUPPLIER_DATA', { supplier_list: res.data })
        return res.data
      })
  },

  addeditSupplier: async ({ commit }, data) => {
    if (data.method === 'add') {
      return SupplierService.addSupplier(data.payload)
        .then(res => {
          // commit('CHANGE_GROUP_DATA', null)
          return res.data
        })
    } else {
      return SupplierService.editSupplier(data.payload.id, data.payload)
        .then(res => {
          return res.data
        })
    }
  },

  loadBillerData: async ({ commit }, options) => {
    return SupplierService.loadBillerData(options)
      .then(res => {
        return res.data
      })
  },  

  loadCarData: async ({ commit }, options) => {
    return SupplierService.loadCarData(options)
      .then(res => {
        return res.data
      })
  },

  addeditBiller: async ({ commit }, data) => {
    if (data.method === 'add') {
      return SupplierService.addBiller(data.payload)
        .then(res => {
          // commit('CHANGE_GROUP_DATA', null)
          return res.data
        })
    } else {
      return SupplierService.editBiller(data.payload.id, data.payload)
        .then(res => {
          return res.data
        })
    }
  },

  addeditCar: async ({ commit }, data) => {
    if (data.method === 'add') {
      return SupplierService.addCar(data.payload)
        .then(res => {
          // commit('CHANGE_GROUP_DATA', null)
          return res.data
        })
    } else {
      return SupplierService.editCar(data.payload.id, data.payload)
        .then(res => {
          return res.data
        })
    }
  },



}

// Export module
export default { state, getters, mutations, actions }
