import { ReportsService } from '../../services'
/**
 * Initial state
 */
const state = {
  reports: {}
}

/**
 * Getters
 */
const getters = {
  reports: state => state.reports
}

/**
 * Mutations
 */
const mutations = {


}

/**
 * Actions
 */
const actions = {

  exportPL: async ({ commit }, options) => {
    return ReportsService.exportPL(options)
      .then(res => {
        return res.data
      })
  },

  exportMaterialReport: async ({ commit }, options) => {
    return ReportsService.exportMaterialReport(options)
      .then(res => {
        return res.data
      })
  },

}

// Export module
export default { state, getters, mutations, actions }
