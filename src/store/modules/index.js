import options from './options'
import users from './users'
import presets from './presets'
import suppliers from './suppliers'
import projects from './projects'
import employee from './employee'
import clients from './clients'
import service_manager from './service-manager'
import reports from './reports'
import service_profiles from './service-profiles'
import branches from './branches'
import actions from './actions'
import service_procedures from './service-procedures'
import logs from './logs'
import documents from './documents'
import accounts from './accounts'
import attendance from './attendance'
import roles from './roles'
import departments from './departments'
import financing from './financing'
import form from './form'
import orders from './orders'
import inventories from './inventories'
import companies from './companies'
import locations from './locations'
import rider_evaluation from './rider-evaluations'

export default {
  options,
  users,
  presets,
  suppliers,
  projects,
  employee,
  clients,
  service_manager,
  reports,
  logs,
  roles,
  accounts,
  financing,
  orders,
  inventories,
  documents,
  attendance
}
