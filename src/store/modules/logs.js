import { LogsService } from '../../services'
/**
 * Initial state
 */
const state = {
  action_logs: {},
  transaction_logs: {},
}

/**
 * Getters
 */
const getters = {
  transaction_logs: state => state.transaction_logs
}

/**
 * Mutations
 */
const mutations = {
  CHANGE_TRANSACTION_LOGS_DATA: (state, transaction_logs) => {
    Object.assign(state.transaction_logs, transaction_logs)
  },

  CHANGE_ACTION_LOGS_DATA: (state, action_logs) => {
    Object.assign(state.action_logs, action_logs)
  },
}

/**
 * Actions
 */
const actions = {

  getTransactionLogs: ({ commit }, model) => {
    return LogsService.getTransactionLogs(model.client_id, model.group_id)
      .then(res => {
        return res.data
      })
  },

  getTransactionHistory: ({ commit }, model) => {
    return LogsService.getTransactionHistory(model.client_id, model.group_id)
      .then(res => {
        return res.data
      })
  },

  getOldTransactionLogs: ({ commit }, model) => {
    return LogsService.getOldTransactionLogs(model.client_id, model.group_id, model.lastBalance)
      .then(res => {
        return res.data
      })
  },

  getGroupTransactionLogs: ({ commit }, model) => {
    return LogsService.getGroupTransactionLogs(model.client_id, model.group_id)
      .then(res => {
        return res.data
      })
  },

  getCommissionLogs: ({ commit }, model) => {
    return LogsService.getCommissionLogs(model.client_id, model.group_id)
      .then(res => {
        return res.data
      })
  },

  getActionLogs: ({ commit }, model) => {
    return LogsService.getActionLogs(model.client_id, model.group_id)
      .then(res => {
        return res.data
      })
  },

  getDocumentLogs: ({ commit }, model) => {
    return LogsService.getDocumentLogs(model.client_id)
      .then(res => {
        return res.data
      })
  },

  getAllLogs: ({ commit }, model) => {
    return LogsService.getAllLogs(model.client_service_id)
      .then(res => {
        return res.data
      })
  },

  deleteLatestDocumentLog: ({ commit }, model) => {
    return LogsService.deleteLatestDocumentLog(model)
      .then(res => {
        return res.data
      })
  },

  getServiceHistory: ({ commit }, model) => {
    return LogsService.getServiceHistory(model.group_id)
      .then(res => {
        return res.data
      })
  },

}

// Export module
export default { state, getters, mutations, actions }
