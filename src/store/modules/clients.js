import { ClientService } from '../../services'
/**
 * Initial state
 */
const state = {
  clients: {}
}

/**
 * Getters
 */
const getters = {
  clients: state => state.clients
}

/**
 * Mutations
 */
const mutations = {

  CHANGE_CLIENT_DATA: (state, clients) => {
    Object.assign(state.clients, clients)
  }

}

/**
 * Actions
 */
const actions = {

  loadClientData: async ({ commit }) => {

    return ClientService.getAllClients()
      .then(res => {
        commit('CHANGE_CLIENT_DATA', { client_list: res.data.data.clients })
        return res.data
      });

  },

  getClientProfile: ({ commit }, model) => {
    var id = model.id;
    var clientOptions = {};
    if(model.options !== undefined) {
      clientOptions = model.options
    }
    return ClientService.getClientProfile(id, clientOptions)
      .then(res => {
        return res.data
      })
  },

  // POST add client
  addClient: ({ commit }, model) => {
    return ClientService.addClient(model)
      .then(res => {
        return res.data;
      })
  },

  // POST update client
  updateClient: ({ commit }, model) => {
    return ClientService.updateClient(model)
      .then(res => {
        return res.data;
      })
  },

  // POST add temporary client
  addTemporaryClient: ({ commit }, model) => {
    return ClientService.addTemporaryClient(model)
      .then(res => {
        return res.data;
      })
  },

  // POST add client service
  addClientService: ({ commit }, model) => {
    return ClientService.addClientService(model)
      .then(res => {
        return res.data;
      })
  },

  // POST add client service
  editClientService: ({ commit }, model) => {
    return ClientService.editClientService(model)
      .then(res => {
        return res.data;
      })
  },

  // POST add client service
  addClientPackage: ({ commit }, model) => {
    return ClientService.addClientPackage(model)
      .then(res => {
        return res.data;
      })
  },

// POST delete client service
  deleteClientPackage: ({ commit }, model) => {
    return ClientService.deleteClientPackage(model)
      .then(res => {
        return res.data;
      })
  },

  // POST add client funds
  addClientFund: ({ commit }, model) => {
    return ClientService.addClientFund(model)
      .then(res => {
        return res.data;
      })
  },

  // POST update risk
  updateRisk: ({ commit }, model) => {
    return ClientService.updateRisk(model)
      .then(res => {
        return res.data;
      })
  },

  // GET get today's task
  getTodayTasks: ({ commit }, model) => {
    return ClientService.getTodayTasks(model)
      .then(res => {
        return res.data;
      })
  },

  // GET get today's task
  getPastTasks: ({ commit }, model) => {
    return ClientService.getPastTasks(model)
      .then(res => {
        return res.data;
      })
  },

  // GET get today's task
  getTomorrowTasks: ({ commit }, model) => {
    return ClientService.getTomorrowTasks(model)
      .then(res => {
        return res.data
      })
  },

  // POST add tomorrow's task
  addTomorrowTasks: ({ commit }, model) => {
    return ClientService.addTomorrowTasks(model)
      .then(res => {
        return res.data;
      })
  },

  // POST update past task
  updatePastTasks: ({ commit }, model) => {
    return ClientService.updatePastTasks(model)
      .then(res => {
        return res.data;
      })
  },

  // POST get employees
  getEmployees: ({ commit }) => {
    return ClientService.getEmployees()
      .then(res => {
        return res.data;
      })
  },

  // POST get reminders
  getReminders: ({ commit }, options) => {
    return ClientService.getReminders(options)
      .then(res => {
        return res.data;
      })
  },

  //payments
  getClientUnpaidService: ({ commit }, options) => {
    return ClientService.getUnpaidService(options)
      .then(res => {

       var services = [];

       var data = res.data.data;

        if(data.services.length > 0){
          data.services.map((item,index) => {
              item['index'] = index; //add index for temporary ids
              services.push(item);
          })
        }

        let totalBalance = (typeof data.total_deposit !=='undefined') ? data.total_deposit : 0;
        let totalAvailableBalance = (typeof data.total_available_balance !=='undefined') ? data.total_available_balance : 0;

        return {
            data: {
              services:services,
              total_balance: (totalBalance - totalAvailableBalance)
            },
            total:  res.data.total
        }


      })
  },

  addClientServicePayment: async ({ commit }, data) => {
    return ClientService.addClientServicePayment(data)
      .then(res => {
          return res.data
      })
  },

  switchClientServiceCost : async ({ commit }, options) => {
    return ClientService.switchClientServiceCost(options)
      .then(res => {
            return res.data;
      })
  },


  getContactType: ({ commit }) => {
    return ClientService.getContactType()
      .then(res => {
        return res.data;
      })
  },

  getClientsByIds: async ({ commit }, options) => {
    return ClientService.getClientsByIds(options)
      .then(res => {
        return res.data;
      })
  },

  addClientsRemark: ({ commit }, options) => {
    return ClientService.addClientsRemark(options)
      .then(res => {
        return res.data;
      })
  },

  getClientsRemarks: ({ commit }, options) => {
    return ClientService.getClientsRemarks(options)
      .then(res => {
        return res.data;
      })
  },

  getPayServices: ({ commit }, options) => {
    return ClientService.getPayServices(options)
      .then(res => {
        return res.data;
      })
  },

  addClientPayment: async ({ commit }, options) => {
    return ClientService.addClientPayment(options)
      .then(res => {
            return res.data;
      })
  },

  getQRData: async ({ commit }, options) => {
    return ClientService.getQRData(options)
      .then(res => {
            return res.data;
      })
  },

  updatePassportMonitor: ({ commit }, model) => {
    return ClientService.updatePassportMonitor(model)
      .then(res => {
        return res.data;
      })
  },
}

// Export module
export default { state, getters, mutations, actions }
