import { FinancingService } from '../../services'
/**
 * Initial state
 */
const state = {
}

/**
 * Getters
 */
const getters = {

}

/**
 * Mutations
 */
const mutations = {

}

/**
 * Actions
 */
const actions = {

  getBanks: ({ commit }, options) => {
    return FinancingService.getBanks(options)
      .then(res => {
        return res.data
      })
  },

  addeditBank: async ({ commit }, data) => {
    if (data.method === 'add') {
      return FinancingService.addBank(data.payload)
        .then(res => {
          // commit('CHANGE_GROUP_DATA', null)
          return res.data
        })
    } else {
      return FinancingService.editBank(data.payload.id, data.payload)
        .then(res => {
          return res.data
        })
    }
  },

  addeditQuotation: async ({ commit }, data) => {
    if (data.method === 'add') {
      return FinancingService.addQuotation(data.payload)
        .then(res => {
          // commit('CHANGE_GROUP_DATA', null)
          return res.data
        })
    } else {
      return FinancingService.editQuotation(data.payload.id, data.payload)
        .then(res => {
          return res.data
        })
    }
  },

  getProjectCollectables: async ({ commit }, options) => {
    return FinancingService.getProjectCollectables(options)
      .then(res => {
        return res;
      })
  },

  getFinancing: ({ commit }, model) => {
    return FinancingService.getFinancing(model.date, model.branch_id)
      .then(res => {
        return res.data
      })
  },

  getBorrowed: ({ commit }, model) => {
    return FinancingService.getBorrowed(model.trans_type, model.branch_id)
      .then(res => {
        return res.data
      })
  },

  getReqUsers: ({ commit }, model) => {
    return FinancingService.getReqUsers()
      .then(res => {
        return res.data
      })
  },

  addFinance: async ({ commit }, data) => {
    return FinancingService.addFinance(data.payload)
        .then(res => {
          return res.data
        })
  },

  updateFinance: async ({ commit }, data) => {
    return FinancingService.updateFinance(data.payload.borrowed_id, data.payload)
        .then(res => {
          return res.data
        })
  },

  // financing delivery

  getFinancingDelivery: ({ commit }, model) => {
    return FinancingService.getFinancingDelivery(model.date)
      .then(res => {
        return res.data
      })
  },

  addPurchasingBudget: ({ commit }, data) => {
    return FinancingService.addPurchasingBudget(data)
      .then(res => {
        return res.data
      })
  },

  addDeliveryFinance: async ({ commit }, data) => {
    return FinancingService.addDeliveryFinance(data.payload)
        .then(res => {
          return res.data
        })
  },

  updateDeliveryFinance: async ({ commit }, data) => {
    return FinancingService.updateDeliveryFinance(data.payload.finance_id, data.payload)
        .then(res => {
          return res.data
        })
  },

  updateDeliveryRow: async ({ commit }, data) => {
    return FinancingService.updateDeliveryRow(data.payload.id, data.payload)
        .then(res => {
          return res.data
        })
  },

  deleteDeliveryRow: async ({ commit }, data) => {
    return FinancingService.deleteDeliveryRow(data.payload.id, data.payload)
        .then(res => {
          return res.data
        })
  },

  getReturnList: ({ commit }, model) => {
    return FinancingService.getReturnList(model.trans_type)
      .then(res => {
        return res.data
      })
  },

  exportFinancial: ({ commit }, model) => {
    return FinancingService.exportFinancial(model)
      .then(res => {
        return res.data
      })
  },

}

// Export module
export default { state, getters, mutations, actions }
