import { ServiceProcedure } from '../../services'

const state = {
  service_procedures: {}
}

const getters = {
  service_procedures: state => state.service_procedures
}

const mutations = {

}

const actions = {

  getServiceProcedures: ({ commit }, model) => {
    return ServiceProcedure.getServiceProcedures(model)
      .then(res => {
        return res.data
      })
  },

  getServiceProcedure: ({ commit }, model) => {
    return ServiceProcedure.getServiceProcedure(model)
      .then(res => {
        return res.data
      })
  },

  addServiceProcedure: ({ commit }, model) => {
    return ServiceProcedure.addServiceProcedure(model)
      .then(res => {
        return res.data
      })
  },

  updateServiceProcedure: ({ commit }, model) => {
    return ServiceProcedure.updateServiceProcedure(model)
      .then(res => {
        return res.data
      })
  },

  deleteServiceProcedure: ({ commit }, data) => {
    return ServiceProcedure.deleteServiceProcedure(data)
      .then(res => {
        return res.data
      })
  },

  sortServiceProcedures: ({ commit }, model) => {
    return ServiceProcedure.sortServiceProcedures(model)
      .then(res => {
        return res.data
      })
  }

}

export default { state, getters, mutations, actions }
