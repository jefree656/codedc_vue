import { AccountsService } from '../../services'
/**
 * Initial state
 */
const state = {
  accounts: {}
}

/**
 * Getters
 */
const getters = {
  accounts: state => state.accounts
}

/**
 * Mutations
 */
const mutations = {

}

/**
 * Actions
 */
const actions = {
  // GET  Cpanel Users
  getAllCpanelUsers: ({ commit }) => {
	    return AccountsService.getCpanelUsers()
	      .then(res => {
	        return res.data.data
	      })
  },

  // POST add cpanel user
  addCpanelUser: ({ commit }, model) => {
    return AccountsService.addCpanelUser(model)
      .then(res => {
        return res.data
      })
  },

  // get specific cpanel user
  getCpanelUser: ({ commit }, data) => {
    return AccountsService.getCpanelUser(data)
      .then(res => {
        return res.data.data
      })
  },

  // EDIT CPANEL USER
  updateCpanelUser: ({ commit }, model) => {
   		return AccountsService.updateCpanelUser(model)
	     .then(res => {
	        return res.data
    	})
  },

  // DELETE CPANEL USER
  deleteCpanelUser: ({ commit }, data) => {
    return AccountsService.deleteCpanelUser(data)
      .then(res => {
        return res
      })
  },

  /*********** WORKER **************/

  // get specific worker details
  getWorker: ({ commit }, data) => {
    return AccountsService.getWorker(data)
      .then(res => {
        return res.data.data
      })
  },

  // EDIT WORKER
  updateWorker: ({ commit }, model) => {
      return AccountsService.updateWorker(model)
       .then(res => {
          return res.data
      })
  },

  // ADD NEW WORKER
  addNewWorker: ({ commit }, model) => {
      return AccountsService.addNewWorker(model)
       .then(res => {
          return res.data
      })
  },

  getAllWorkers: ({ commit }) => {
    return AccountsService.getAllWorkers()
      .then(res => {
        return res.data
      })
  },

  // POST add worker attendance
  saveWorkerAttendance: ({ commit }, model) => {
    return AccountsService.saveWorkerAttendance(model)
      .then(res => {
        return res.data
      })
  },

  // POST add worker attendance
  saveWorkerAttendanceMultiple: ({ commit }, model) => {
    return AccountsService.saveWorkerAttendanceMultiple(model)
      .then(res => {
        return res.data
      })
  },

  getDayAttendance: ({ commit }, model) => {
    return AccountsService.getDayAttendance(model)
      .then(res => {
        return res.data
      })
  },

}

// Export module
export default { state, getters, mutations, actions }
