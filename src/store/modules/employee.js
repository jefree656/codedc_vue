import { EmployeeService } from '../../services';
/**
 * Initial state
 */
const state = {

}

/**
 * Getters
 */
const getters = {

}

/**
 * Mutations
 */
const mutations = {

}

/**
 * Actions
 */
const actions = {

  getEmployeeDocsOnHand: ({ commit }, options) => {
    return EmployeeService.getEmployeeDocsOnHand(options)
      .then(res => {
        return res.data
      })
  },

  getClientsInDocsOnHand: ({ commit }, options) => {
    return EmployeeService.getClientsInDocsOnHand(options)
      .then(res => {
        return res.data
      })
  },

  exportEmployeeDocsOnHand: ({ commit }, options) => {
    return EmployeeService.exportEmployeeDocsOnHand(options)
      .then(res => {
        return res.data
      })
  },


}

// Export module
export default { state, getters, mutations, actions }
