import { LocationService } from '../../services'

const state = {}

/**
 * Getters
 */
const getters = {}

/**
 * Mutations
 */
const mutations = {}

const actions = {

  getLocation: ({ commit }, options) => {
    return LocationService.getLocation(options)
      .then(res => {
        return res.data
      })
  },

  getLocationDetail: ({ commit }, options) => {
    return LocationService.getLocationDetail(options)
      .then(res => {
        return res.data
      })
  },

  getLocationConsumable: ({ commit }, options) => {
    return LocationService.getLocationConsumable(options)
      .then(res => {
        return res.data
      })
  },

}

// Export module
export default {
  state,
  getters,
  mutations,
  actions
}
