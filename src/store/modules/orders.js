import { OrderService } from '../../services'
/**
 * Initial state
 */
const state = {
   orders: {}
}

/**
 * Getters
 */
const getters = {
    orders: state => state.orders
}

/**
 * Mutations
 */
const mutations = {
  TEMP_ORDER_DATA: (state, orders) => {
    Object.assign(state.orders, orders)
  }
}

/**
 * Actions
 */
const actions = {

  getAllOrders: ({ commit }, options) => {
    return OrderService.getAllOrders(options)
      .then(res => {
        return res.data
      })
  },

  viewOrder: async ({ commit }, data) => {
    return OrderService.viewOrder(data.order_id)  .then(res => {
        return res.data
      })
  },

  viewOrderLog: async ({ commit }, data) => {
    return OrderService.viewOrderLog(data.order_id)  .then(res => {
        return res.data
      })
  },

  addProductCategory: ({ commit }, data) => {
    return OrderService.addProductCategory(data)
      .then(res => {
        console.log('add category', res)
        return res.data
      })
  },

  updateProductCategory: ({ commit }, data) => {
    return OrderService.updateProductCategory(data)
      .then(res => {
        console.log('result data', res)
        return res.data
      })
  },

  removeProductCategory: ({ commit }, data) => {
    return OrderService.removeProductCategory(data)
      .then(res => {
        console.log('result data', res)
        return res.data
      })
  },

  moveProductCategory: ({ commit }, data) => {
    return OrderService.moveProductCategory(data)
      .then(res => {
        console.log('result data', res)
        return res.data
      })
  },

  getProductCategories: ({ commit }) => {
    return OrderService.getProductCategories()
      .then(res => {
        return res.data
      })
  },

  getProductCategoryDetails: ({ commit }, data) => {
    return OrderService.getProductCategoryDetails(data)
      .then(res => {
        return res.data
      })
  },

  getProductByCatId: ({ commit }, data) => {
    return OrderService.getProductByCatId(data)
      .then(res => {
        return res.data
      })
  },

  getProductCategoriesByID: ({ commit }, id) => {
    return OrderService.getProductCategoriesByID(id)
      .then(res => {
        return res.data
      })
  },

  getCategoriesAndProductByID: ({ commit }, id) => {
    return OrderService.getCategoriesAndProductByID(id)
      .then(res => {
        return res.data
      })
  },

  getAllProductCategories: ({ commit }) => {
    return OrderService.getAllProductCategories()
      .then(res => {
        return res.data
      })
  },


  storeTempOrders: async ({ commit }, payload) => {

     var data = payload.list;

     if(payload.data !== null && payload.data !== ""){
       data.push(payload.data)
     }
     else if(payload.data === ""){

     }
     else{
       data = [];
     }
     commit('TEMP_ORDER_DATA', {data})
     return data;
  },

  saveOrders: ({ commit }, data) => {
    return OrderService.saveOrders(data)
      .then(res => {
        return res.data
      })
  },


  updateOrders: ({ commit }, data) => {
    return OrderService.updateOrders(data)
      .then(res => {
        return res.data
      })
  },

  deleteOrder: ({ commit }, data) => {
    return OrderService.deleteOrder(data)
      .then(res => {
        return res.data
      })
  },

  markComplete: ({ commit }, data) => {
    return OrderService.markComplete(data)
      .then(res => {
        return res.data
      })
  },

  addProduct: ({ commit }, data) => {
    return OrderService.addProduct(data)
      .then(res => {
        return res.data
      })
  },

  updateProduct: ({ commit }, data) => {
    return OrderService.updateProduct(data)
      .then(res => {
        return res.data
      })
  },

  orderSummary: ({ commit }, data) => {
    return OrderService.orderSummary(data)
      .then(res => {
        return res.data
      })
  },

  uploadProduct ({ commit }, model) {
      return OrderService.uploadProduct(model)
          .then(res => {
              return res.data;
          })
  },

  checkProductsInOrderDetails ({ commit }, id) {
      return OrderService.checkProductsInOrderDetails(id)
          .then(res => {
              return res.data;
          })
  },

  removeProduct ({ commit }, data) {
      return OrderService.removeProduct(data)
          .then(res => {
              return res.data;
          })
  },  

  moveProduct ({ commit }, data) {
      return OrderService.moveProduct(data)
          .then(res => {
              return res.data;
          })
  },   

  userOrderList ({ commit }, data) {
      return OrderService.userOrderList(data.id, data.data)
          .then(res => {
            return res.data.data;
          })
  }, 

}

// Export module
export default { state, getters, mutations, actions }
