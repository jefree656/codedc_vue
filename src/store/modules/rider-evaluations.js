import { RiderEvaluationService } from "../../services";

const state = {
  histAll: [],
  menu: []
};

/**
 * Getters
 */
const getters = {};

/**
 * Mutations
 */
const mutations = {};

const actions = {
  updateQA: ({ commit }, options) => {
    return RiderEvaluationService.updateQA(options).then(res => {
      return res.data;
    });
  },

  addEvaluation: ({ commit }, options) => {
    return RiderEvaluationService.addEvaluation(options).then(res => {
      return res.data;
    });
  },

  updateEvaluation: ({ commit }, options) => {
    return RiderEvaluationService.updateEvaluation(options).then(res => {
      return res.data;
    });
  },

  deleteEvaluation: ({ commit }, options) => {
    return RiderEvaluationService.deleteEvaluation(options).then(res => {
      return res.data;
    });
  },

  getQA: ({ commit }, options) => {
    return RiderEvaluationService.getQA(options).then(res => {
      return res.data;
    });
  },

  getEvaluation: ({ commit }, options) => {
    return RiderEvaluationService.getEvaluation(options).then(res => {
      return res.data;
    });
  },

  getEvaluationDay: ({ commit }, options) => {
    return RiderEvaluationService.getEvaluationDay(options).then(res => {
      return res.data;
    });
  },

  getDailyEvaluationDetails: ({ commit }, options) => {
    return RiderEvaluationService.getDailyEvaluationDetails(options).then(res => {
      return res.data;
    });
  },

  getEvaluationMonth: ({ commit }, options) => {
    return RiderEvaluationService.getEvaluationMonth(options).then(res => {
      return res.data;
    });
  },

  getSummaryEvaluationHalfMonth: ({ commit }, options) => {
    return RiderEvaluationService.getSummaryEvaluationHalfMonth(options).then(res => {
      return res.data;
    });
  }
};

// Export module
export default {
  state,
  getters,
  mutations,
  actions
};
