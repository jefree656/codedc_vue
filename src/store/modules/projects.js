import { ProjectService } from '../../services';
/**
 * Initial state
 */
const state = {
  projects: {}
}

/**
 * Getters
 */
const getters = {
  projects: state => state.projects
}

/**
 * Mutations
 */
const mutations = {

  CHANGE_PROJECT_DATA: (state, projects) => {
    Object.assign(state.projects, projects)
  }

}

/**
 * Actions
 */
const actions = {

  changeProjectData: async ({ commit }, groups) => {
    commit('CHANGE_PROJECT_DATA', projects)
  },

  loadProjectData: async ({ commit }, options) => {
    return ProjectService.getManageProjects(options)
      .then(res => {
        commit('CHANGE_PROJECT_DATA', { project_list: res.data })
        return res.data
      })
  },

  addeditProject: async ({ commit }, data) => {
    if (data.method === 'add') {
      return ProjectService.addProject(data.payload)
        .then(res => {
          // commit('CHANGE_GROUP_DATA', null)
          return res.data
        })
    } else {
      return ProjectService.editProject(data.payload.id, data.payload)
        .then(res => {
          return res.data
        })
    }
  },

  getProjectTransactions: async ({ commit }, data) => {
    return ProjectService.getProjectTransactions(data.id)
      .then(res => {
        return res.data
      })
  },

  getAllTasks: async ({ commit }, options) => {
    return ProjectService.getAllTasks(options)
      .then(res => {
        return res;
      })
  },

  getPerTeamTasks: async ({ commit }, options) => {
    return ProjectService.getPerTeamTasks(options)
      .then(res => {
        return res;
      })
  },  

  getPerTeamCount: async ({ commit }, options) => {
    return ProjectService.getPerTeamCount(options)
      .then(res => {
        return res;
      })
  },  

  uploadTaskImages: async ({ commit }, data) => {
      return ProjectService.uploadTaskImages(data.payload)
        .then(res => {
          // commit('CHANGE_GROUP_DATA', null)
          return res.data
        })
  },  

  bankTransfer: async ({ commit }, data) => {
      return ProjectService.bankTransfer(data.payload)
        .then(res => {
          // commit('CHANGE_GROUP_DATA', null)
          return res.data
        })
  }, 

  getProjectProfile: async ({ commit }, data) => {
    return ProjectService.getProjectProfile(data.id)
      .then(res => {
          commit('CHANGE_PROJECT_DATA', res.data.data)
        return res.data
      })
  },

  getProjectProfile2: async ({ commit }, data) => {
    return ProjectService.getProjectProfile2(data.id)
      .then(res => {
        return res.data
      })
  },

  getProjectDetails: async ({ commit }, data) => {
    return ProjectService.getProjectDetails(data.payload)
      .then(res => {
          // commit('CHANGE_PROJECT_DATA', res.data.data)
        return res.data
      })
  },

  loadSearchResults: async ({ commit }, options) => {
    return ProjectService.projectSearch(options)
      .then(res => {
        return res.data.data
      })
  },

  saveSection: async ({ commit }, data) => {
    if (data.method === 'add') {
      return ProjectService.addProjectTask(data.payload)
        .then(res => {
          commit('CHANGE_PROJECT_DATA', null)
          return res.data
        })
    } else {
      return ProjectService.editProjectTask(data.payload)
        .then(res => {
          return res.data
        })
    }
  },

  toggleTaskPriority: async ({ commit }, data) => {
      return ProjectService.toggleTaskPriority(data)
        .then(res => {
          return res.data
        })
  },

  markTaskComplete: async ({ commit }, data) => {
      return ProjectService.markTaskComplete(data)
        .then(res => {
          return res.data
        })
  },

  markTaskIncomplete: async ({ commit }, data) => {
      return ProjectService.markTaskIncomplete(data)
        .then(res => {
          return res.data
        })
  },

  getProjectTask: ({ commit }, model) => {
    return ProjectService.getProjectTask(model.task_id)
      .then(res => {
        return res.data
      })
  },    

  getProjectTaskWithOrders: ({ commit }, model) => {
    return ProjectService.getProjectTaskWithOrders(model.task_id)
      .then(res => {
        return res.data
      })
  },  

  getPaymentInfo: ({ commit }, model) => {
    return ProjectService.getPaymentInfo(model.payment_id)
      .then(res => {
        return res.data
      })
  },

  getOrderDetail: ({ commit }, model) => {
    return ProjectService.getOrderDetail(model.order_id)
      .then(res => {
        return res.data
      })
  },

  getOrderDetail2: ({ commit }, model) => {
    return ProjectService.getOrderDetail2(model.order_id)
      .then(res => {
        return res.data
      })
  },

  getOrderDetailMultiple: async ({ commit }, data) => {
    // console.log(data);
    return ProjectService.getOrderDetailMultiple(data.payload)
      .then(res => {
          // commit('CHANGE_PROJECT_DATA', res.data.data)
        return res.data
      })
  },

  getOrderDetailMultiple2: async ({ commit }, data) => {
    // console.log(data);
    return ProjectService.getOrderDetailMultiple2(data.payload)
      .then(res => {
          // commit('CHANGE_PROJECT_DATA', res.data.data)
        return res.data
      })
  },

  getAllPresets: ({ commit }, data) => {
    return ProjectService.getAllPresets(data.payload)
      .then(res => {
        return res.data
      })
  },  

  getAllPresets2: ({ commit }, data) => {
    return ProjectService.getAllPresets2(data.payload)
      .then(res => {
        return res.data
      })
  },

  getAllSuppliers: ({ commit }) => {
    return ProjectService.getAllSuppliers()
      .then(res => {
        return res.data
      })
  },

  getSelectedPresets: ({ commit }, data) => {
    return ProjectService.getSelectedPresets(data.payload)
      .then(res => {
        return res.data
      })
  },

  // POST add purchase order
  addPurchaseOrder: ({ commit }, model) => {
    return ProjectService.addPurchaseOrder(model)
      .then(res => {
        return res.data;
      })
  },  

  addPurchaseOrder2: ({ commit }, model) => {
    return ProjectService.addPurchaseOrder2(model)
      .then(res => {
        return res.data;
      })
  },  

  clearMob: ({ commit }, model) => {
    return ProjectService.clearMob(model.payload)
      .then(res => {
        return res.data;
      })
  },

  requestMaterials: ({ commit }, model) => {
    return ProjectService.requestMaterials(model.payload)
      .then(res => {
        return res.data;
      })
  },

  requestLowQty: ({ commit }, model) => {
    return ProjectService.requestLowQty(model.payload)
      .then(res => {
        return res.data;
      })
  },

  deleteTask: ({ commit }, model) => {
    return ProjectService.deleteTask(model)
      .then(res => {
        return res.data;
      })
  },

  deleteComment: ({ commit }, model) => {
    return ProjectService.deleteComment(model)
      .then(res => {
        return res.data;
      })
  },  

  deletePaymentComment: ({ commit }, model) => {
    return ProjectService.deletePaymentComment(model)
      .then(res => {
        return res.data;
      })
  },  

  deletePayment: ({ commit }, model) => {
    return ProjectService.deletePayment(model)
      .then(res => {
        return res.data;
      })
  },

  allUsers: ({ commit }, model) => {
    return ProjectService.allUsers(model)
      .then(res => {
        return res.data;
      })
  },

  deleteTaskImage: ({ commit }, options) => {
    return ProjectService.deleteTaskImage(options)
      .then(res => {
        return res.data
      })
  },  

  deleteMaterialImage: ({ commit }, options) => {
    return ProjectService.deleteMaterialImage(options)
      .then(res => {
        return res.data
      })
  },  

  markAsRead: ({ commit }, options) => {
    return ProjectService.markAsRead(options)
      .then(res => {
        return res.data
      })
  },


}

// Export module
export default { state, getters, mutations, actions }
