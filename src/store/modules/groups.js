import { GroupService, ClientService } from '../../services';
/**
 * Initial state
 */
const state = {
  groups: {}
}

/**
 * Getters
 */
const getters = {
  groups: state => state.groups
}

/**
 * Mutations
 */
const mutations = {

  CHANGE_GROUP_DATA: (state, groups) => {
    Object.assign(state.groups, groups)
  }

}

/**
 * Actions
 */
const actions = {

  changeGroupData: async ({ commit }, groups) => {
    commit('CHANGE_GROUP_DATA', groups)
  },

  loadGroupData: async ({ commit }, options) => {
    return GroupService.getManageGroups(options)
      .then(res => {
        commit('CHANGE_GROUP_DATA', { group_list: res.data })
        return res.data
      })
  },



  loadSearchResults: async ({ commit }, options) => {
    return GroupService.groupSearch(options)
      .then(res => {
        return res.data.data
      })
  },

  assignGroupRole: async ({ commit }, options) => {
    return GroupService.assignGroupRole(options)
      .then(res => {
        return res
      })
  },

  addEditData: async ({ commit }, data) => {
    if (data.method === 'add') {
      return GroupService.addGroup(data.payload)
        .then(res => {
          commit('CHANGE_GROUP_DATA', null)
          return res.data
        })
    } else {
      return GroupService.editGroup(data.payload.id, data.payload)
        .then(res => {
          return res.data
        })
    }
  },

  getGroupProfile: async ({ commit }, data) => {
    return GroupService.getGroupProfile(data.id)
      .then(res => {
          commit('CHANGE_GROUP_DATA', res.data.data)
        return res.data
      })
  },


  deleteGroupMember: async ({ commit }, options) => { // Delete a specific document
    return GroupService.deleteGroupMember(options)
      .then(res => {
          return res.data
      })
  },

  getGroupMemberServices: async ({ commit }, options) => { // Delete a specific document
    return GroupService.getGroupMemberServices(options)
      .then(res => {

            var payload = [];
            var packages = [];

            packages.push({
              "id": 0,
              "tracking": 0,
              "group_id": options.group_id,
              "description": 'Generate new package'
            });

            if(res.data.data.packages.length > 0){
              res.data.data.packages.map((itemPackage,indexPackage) => {
                        packages.push({
                          "id": itemPackage.id,
                          "tracking": itemPackage.tracking,
                          "group_id": itemPackage.group_id,
                          "description": itemPackage.created_at
                        })
              })
            }

            res.data.data.services.map((item,index) => {
                  payload.push({
                        ctr: index,
                        sdate: item.sdate,
                        service_id: item.service_id,
                        id: item.id,
                        tracking: item.tracking,
                        status: item.status,
                        detail: item.detail,
                        created_at: item.created_at,
                        packages: packages
                  });
            })

          return payload;

      })
  },

  getGroupMembers: async ({ commit }, options) => {

    return GroupService.getGroupMembers({
        id: options.id,
        page: options.page,
        sort: 'sort=' + options.sort,
        search: options.search,
        perPage: options.perPage,
        leader_id: options.leader_id
    })
      .then(res => {

        // var sortedData = [];
        // var tempData = res.data.data;
        //
        // var payload  = {
        //   current_page: res.data.current_page,
        //   data:tempData,
        //   first_page_url: res.data.first_page_url,
        //   from: res.data.from,
        //   last_page: res.data.last_page,
        //   last_page_url: res.data.last_page_url,
        //   next_page_url: res.data.next_page_url,
        //   path: res.data.path,
        //   per_page: res.data.per_page,
        //   prev_page_url: res.data.prev_page_url,
        //   to: res.data.to,
        //   total: res.data.total
        // }

        //return payload
        return res.data;
      })
  },


//By Members
  getGroupByMembers: async ({ commit }, options) => {

    return GroupService.getGroupByMembers({
        id: options.id,
        page: options.page,
        sort: 'sort=' + options.sort,
        search: options.search,
        ids: options.ids,
        perPage: options.perPage,
        leader_id: options.leader_id,
        from: options.from
    })
      .then(res => {

        var isleaderOnTop = true;
        var sortedData = [];
        var tempData = res.data.data;

        if(options.sort !== 'name-desc'){
            isleaderOnTop = false;
            sortedData = tempData;
        }else{

            //sort vice leader
            var viceLeaders = [];

            tempData.sort(function(a, b){return b.is_vice_leader-a.is_vice_leader});

            //sort by leader
            var index = tempData.findIndex(v => v.user_id === options.leader_id);

            if(index !== -1){
               var groupLeaderData = res.data.data[index]
               tempData.splice(index, 1);
               tempData.unshift(groupLeaderData)
            }

            sortedData = tempData;
        }

        var data = [];
        tempData.map((item,index) => {

          var paidBatch = true;

            item.packages.map((itemPackages,i) => {
                if(itemPackages.is_full_payment !== 1){
                  paidBatch = false;
                }
            });

            data.push({
                index: index,
                id: item.id,
                is_vice_leader: item.is_vice_leader,
                name: item.name,
                group_id: item.group_id,
                packages: item.packages,
                total_service_cost: item.total_service_cost,
                user_id: item.user_id,
                is_paid: paidBatch,
                total_sub: item.total_sub,
                status: item.status,
                status_list: item.status_list
            });
        });

        var payload  = {
          current_page: res.data.current_page,
          data:data,
          first_page_url: res.data.first_page_url,
          from: res.data.from,
          last_page: res.data.last_page,
          last_page_url: res.data.last_page_url,
          next_page_url: res.data.next_page_url,
          path: res.data.path,
          per_page: res.data.per_page,
          prev_page_url: res.data.prev_page_url,
          to: res.data.to,
          total: res.data.total
        }

        return payload;

      })
  },


  addGroupMembers: async ({ commit }, options) => {
    return GroupService.addGroupMembers(options)
      .then(res => {
        return res
      })
  },

  addGroupService: async ({ commit }, options) => {

    var clients = [];
    var packages = [];
    var services = [];
    var months = [];



    //filter clients and packages
    // options.clientData.map((item,index) => {
    //   if(!options.isClientCheck[index] && typeof options.packages[index] !== 'undefined'){ //false if selected
    //     clients.push(item.client_id);
    //     packages.push(options.packages[index]);
    //   }
    // })

    console.log("d", options.clientSelectedRow);

    options.clientSelectedRow.map((item,index) => {
        clients.push(item.client_id);
        packages.push(options.tempPackages[item.client_id]);
        months.push(options.months[item.client_id]);
    })
    services = options.servicesData.services;

    return GroupService.addGroupService({
      branch_id: options.branch_id,
      group_id: options.group_id,
      clients: clients,
      packages: packages,
      services: services,
      note: options.servicesData.note,
      charge: options.servicesData.charge,
      cost: options.servicesData.cost,
      tip: options.servicesData.tip,
      client_com_id: options.client_com_id,
      agent_com_id: options.agent_com_id,
      month:months
    }).then(res => {
        return res
    });
  },


  getGroupMemberPackages: async ({ commit }, options) => {
    return GroupService.getGroupMemberPackages(options)
      .then(res => {
        return res
      })
  },

  //BY_BATCH_MEMBER
  getMemberByBatch : async ({ commit }, options) => {
    return GroupService.getMemberByBatch(options)
      .then(res => {
        return res.data;
      })
  },

  getMemberByService : async ({ commit }, options) => {
    return GroupService.getMemberByService(options)
      .then(res => {
        return res.data;
      })
  },

  getServicesByMember : async ({ commit }, options) => {
    return GroupService.getServicesByMember(options)
      .then(res => {
        return res.data;
      })
  },


  getCurrentBatchPage: async ({ commit }, options) => {
    return GroupService.getCurrentBatchPage(options)
      .then(res => {
        return res.data;
      })
  },

  //Group Batch
  getGroupByBatch: async ({ commit }, options) => {
    return GroupService.getGroupByBatch(options)
      .then(res => {

        var temData = [];

         if(res.data.data.length > 0){
            res.data.data.map((item,index) => {

              var paidBatch = true;

              if(item.members.length > 0){

              item.members.map((itemMember,i) => {
                itemMember.services.map((itemService,i) => {
                  if(itemService.is_full_payment !== 1){
                    paidBatch = false;
                  }
                });
              });

              }

                temData.push({
                    id: index,
                    detail: item.detail,
                    group_id: item.group_id,
                    members: item.members,
                    sdate: item.sdate,
                    service_date: item.service_date,
                    total_service_cost: item.total_service_cost,
                    total_members: item.total_members,
                    total_sub: item.total_sub,
                    status: item.status,
                    status_list: item.status_list,
                    is_paid: paidBatch,
                    status_temp: item.status_temp
                });
            });
            res.data.data = temData;
         }
        return res
      })
  },
//By Service
  getGroupByService: async ({ commit }, options) => {
    return GroupService.getGroupByService(options)
      .then(res => {

        var temData = [];

         if(res.data.data.length > 0){
            res.data.data.map((item,index) => {

              var paidBatch = true;

                item.bydates.map((itemDate,i) => {
                  itemDate.members.map((itemMember,i) => {
                    if(itemMember.service.is_full_payment !== 1){
                      paidBatch = false;
                    }
                  });
                });

                temData.push({
                    index: index,
                    service_id: item.service_id,
                    detail: item.detail,
                    group_id: item.group_id,
                    bydates: item.bydates,
                    sdate: item.sdate,
                    service_count: item.service_count,
                    service_date: item.service_date,
                    total_service: item.total_service,
                    total_service_cost: item.total_service_cost,
                    latest_date: item.latest_date,
                    status: item.status,
                    total_sub: item.total_sub,
                    status_list: item.status_list,
                    is_paid: paidBatch
                });
            });
            res.data.data = temData;
         }

        return res;
      })
  },

  getByService: async ({ commit }, options) => {
    return GroupService.getByService(options)
      .then(res => {
        return res
      })
  },

  getByBatch: async ({ commit }, options) => {
    return GroupService.getByBatch(options)
      .then(res => {
        return res
      })
  },

  distributePayment: async ({ commit }, options) => {
    return GroupService.distributePayment(options)
      .then(res => {
        return res.data
      })
  },



  getUnpaidService: async ({ commit }, options) => {
    return GroupService.getUnpaidService(options)
      .then(res => {

        var serviceIds = [];
        var payments = [];
        var totalPayment = 0;
        var services = [];
        var totalBalanceAvailable = 0;
        var totalBalance = 0;

       if(res.data.data.length > 0){
          res.data.data.map((item,index) => {

                totalBalanceAvailable = item.total_available_balance;
                totalBalance = item.total_balance;

                item.members.map((itemMember,indexMember) => {
                    if(itemMember.services.length > 0){
                        itemMember.services.map((itemService,indexService) => {

                          if(itemService.is_full_payment === 0){
                              serviceIds.push(itemService.id);
                              payments.push(itemService.payment);
                          }else{
                              totalPayment += itemService.payment_amount;
                          }
                        })
                    }
                })

              services.push(item);
          })
        }

        return {
            data: {
              services:services,
              ids: serviceIds,
              payments: payments,
              total_payment: totalPayment,
              total_available_balance: (totalBalance - totalBalanceAvailable),
              total_balance: totalBalance
            },
            total:  res.data.total
        }

      })
  },

  addGroupServicePayment: async ({ commit }, data) => {
    return GroupService.addGroupServicePayment(data)
      .then(res => {
          return res.data
      })
  },

  editGroupServicePayment: async ({ commit }, data) => {
    return GroupService.editGroupServicePayment(data)
      .then(res => {
          return res.data
      })
  },


  getListOfGroup: async ({ commit }, data) => {
    return GroupService.getAllGroups()
      .then(res => {
        return res.data
      })
  },





  groupServiceSteps: async ({ commit }, data) => {
     commit('CHANGE_GROUP_DATA', { add_group_service_data: data })
     return data;
  },

  storeClientServices: async ({ commit }, data) => {
    var serviceIds = [];
    var hasSameData = true;
    var hasSameCost = true;
    var hasSameTip = true;
    var hasSameStatus = true;
    var hasSameDiscount = true;
    var hasSameCharge = true;

     data.map((item,index) => {

       if(data.find(o => (o.charge !== item.charge))){
           hasSameCharge = false;
           hasSameData = false;
       }

        if(data.find(o => (o.cost !== item.cost))){
            hasSameCost = false;
            hasSameData = false;
        }
        if(data.find(o => (o.tip !== item.tip))){
            hasSameTip = false;
            hasSameData = false;
        }
        if(data.find(o => (o.status !== item.status))){
            hasSameStatus = false;
            hasSameData = false;
        }
        if(data.find(o => (o.discount !== item.discount))){
            hasSameDiscount = false;
            hasSameData = false;
        }

        serviceIds[index] = item.id;
     })

     var serviceData = {
       hasSameCharge: hasSameCharge,
       hasSameCost: hasSameCost,
       hasSameTip: hasSameTip,
       hasSameStatus: hasSameStatus,
       hasSameDiscount: hasSameDiscount,
       data: data,
       serviceIds: serviceIds
     };

     commit('CHANGE_GROUP_DATA', { client_selected_services: serviceData })
     return data;
  },

  editGroupServices: async ({ commit }, options) => {
    return GroupService.editGroupServices(options)
      .then(res => {
          return res.data;
       });
  },

  //clientSearch
  searchMembers: async ({ commit }, options) => {
    return ClientService.comSearch(options)
      .then(res => {
          return res;
       });
  },

  getGroupServicesByProfile: async ({ commit }, options) => {
    return GroupService.getGroupServicesByProfile(options)
      .then(res => {
          return res.data;
      })
  },

  getGroupServices: async ({ commit }, options) => {
    return GroupService.getGroupServices(options)
      .then(res => {
          return res.data;
      })
  },

  getMembersPackages: async ({ commit }, options) => {
    return GroupService.getMembersPackages(options)
      .then(res => {

        var membersPackages = [];
        var ctr = 0;

        console.log("res data: ", res.data);

        if(res.data.data.length > 0){

          res.data.data.map((item,index) => {
            var packages = [];

            packages.push({
              "id": 0,
              "tracking": 0,
              "group_id": item.group_id,
              "description": 'Generate new package'
            });

            if(item.packages.length > 0){
              item.packages.map((itemPackage,indexPackage) => {
                        packages.push({
                          "id": itemPackage.id,
                          "tracking": itemPackage.tracking,
                          "group_id": itemPackage.group_id,
                          "description": itemPackage.created_at
                        })
              })
            }

            membersPackages.push({
              "id": ctr,
              "client_id": item.client_id,
              "name": item.name,
              "packages": packages
            });
            ctr++;
          })

          return {
              data: membersPackages,
              total:  res.data.total
          }

        }else{
          return {
              data: [],
              total: 0
          };
        }
      })
  },


  getGroupFunds: async ({ commit }, options) => {
    return GroupService.getGroupFunds(options)
      .then(res => {
        return res.data
      })
  },

  addFunds: async ({ commit }, options) => {
    return GroupService.addFunds(options)
      .then(res => {
        return res.data
      })
  },


  transferService : async ({ commit }, options) => {
    return GroupService.transferService(options)
      .then(res => {
            return res.data;
      })
  },

  transferMember : async ({ commit }, options) => {
    return GroupService.transferMember(options)
      .then(res => {
            return res.data;
      })
  },

  checkIfMemberExist : async ({ commit }, options) => {
    return GroupService.checkIfMemberExist(options)
      .then(res => {
            return res.data;
      })
  },


  switchBranch : async ({ commit }, options) => {
    return GroupService.switchBranch(options)
      .then(res => {
            return res.data;
      })
  },

  switchServiceCost : async ({ commit }, options) => {
    return GroupService.switchServiceCost(options)
      .then(res => {
            return res.data;
      })
  },

  updateGroupRisk: async ({ commit }, options) => {
    return GroupService.updateRisk(options)
      .then(res => {
            return res.data;
      })
  },

//id: this.$route.params.id }
  exportExcelGroupSummary: async ({ commit }, options) => {
    return GroupService.exportExcelGroupSummary(options)
      .then(res => {
            return res.data;
      })
  },

  getServiceDates: async ({ commit }, options) => {
    return GroupService.getServiceDates(options)
      .then(res => {
            return res.data;
      })
  },

  getServiceAdded: async ({ commit }, options) => {
    return GroupService.getServiceAdded(options)
      .then(res => {
            return res.data;
      })
  },

  searchLeaderByBranchId: async ({ commit }, options) => {
    return ClientService.clientSearch(options)
      .then(res => {
            return res.data.data;
      })
  },

  previewReport: async ({ commit }, options) => {
    return GroupService.previewReport(options)
      .then(res => {
            return res.data;
      })
  },

  addGroupPayment: async ({ commit }, options) => {
    return GroupService.addGroupPayment(options)
      .then(res => {
            return res.data;
      })
  },

  addGroupRemark: async ({ commit }, options) => {
    return await GroupService.addGroupRemark(options)
      .then(res => {
        return res.data;
      })
  },

  getGroupHistory: async ({ commit }, options) => {
    return await GroupService.getGroupHistory(options)
      .then(res => {
        return res.data;
      })
  },



  getGroupDocumentLogs: async ({ commit }, data) => {
    return GroupService.getGroupDocumentLogs(data)
      .then(res => {
        return res.data
      })
  },

  getGroupDocumentsOnhand: async ({ commit }, data) => {
    return GroupService.getGroupDocumentsOnhand(data)
      .then(res => {
        return res.data
      })
  },

  getFundList: async ({ commit }, data) => {
    return GroupService.getFundList(data)
      .then(res => {
        return res.data
      })
  },

  exportFundsSummary: ({ commit }, options) => {
    return GroupService.exportFundsSummary(options)
      .then(res => {
        return res.data
      })
  },



}

// Export module
export default { state, getters, mutations, actions }
