import { axios, storage } from '../utils'
import i18n from '../i18n'

const state = {
  /**
   * 客户端会话信息
   * @type {Object}
   * TODO: storage - local or session
   */
  session: storage.get('wyc_session_info') || {},

  /**
   * 顶部工具栏
   * @type {Object}
   */
  header: {
    /**
     * 站点名称
     * @type {String}
     */
    name: 'CODEDC'
  },

  /*
  *
  * @type {Object}
  *
  */
  modal: storage.get('wyc_modal') || {},

  lightbox: storage.get('wyc_lightbox') || {},

  /**
   * 侧边导航栏
   * @type {Object}
   */
  sidebar: {
    /**
     * 版权所属
     * @type {String}
     */
    copyright: 'CODEDC',

    /**
     * @type {Boolean}
     */
    collapse: storage.get('wyc_sidebar_collapse'),

    /**
     * @type {Array}
     */
    menus: [
      {
        text: 'Navigation.dashboard',
        icon: 'meter',
        name: 'dashboard',
        children: [
          { text: 'Navigation.home', name: 'dashboard', params: { name: 1 } },
          // { text: 'Navigation.today-serv', name: 'today-services' },
          // { text: 'Navigation.on-process-serv', name: 'on-process-services' },
          // { text: 'Navigation.pending-serv', name: 'pending-services' },
          // { text: 'Navigation.filed-serv', name: 'filed-services' },
          // { text: 'Navigation.today-task', name: 'today-task' },
          // { text: 'Navigation.tomorrow-task', name: 'tomorrow-task' },
          // { text: 'Navigation.passport-expiration-reminder', name: 'passport-expiration-reminder' },
          // { text: 'Navigation.event-calendar', name: 'event-calendar' },
          // { text: 'Navigation.download-app', name: 'download-app' }
        ]
      },
      {
        divider: true
      },

      {
        text: 'Navigation.inventory',
        icon: 'stack',
        name: 'inventory-list',
        params: { type: 'home' },
        // children: [
        //   { text: 'Home', name: 'inventory-list', params: { type: 'home' } },
        //   { text: 'Latest', name: 'inventory-latest', params: { type: 'latest' } },
        // ]
      },

      {
        divider: true
      },

      // {
      //   text: 'Project Presets',
      //   icon: 'stack',
      //   name: 'presets',
      //   params: { type: 'page' }
      // },

      {
        text: 'Projects',
        icon: 'user',
        name: 'projects',
        params: { type: 'page' }
      },

      {
        divider: true
      },

      // {
      //   text: 'Suppliers',
      //   icon: 'users',
      //   name: 'suppliers',
      //   params: { type: 'page' }
      // },


      // {
      //   divider: true
      // },


      // {
      //   divider: true
      // },

      // {
      //   text: 'Navigation.gantt-chart',
      //   icon: 'map',
      //   name: 'gantt-chart',
      // },
      // {
      //   divider: true
      // },

      // {
      //   text: 'Navigation.Quotation',
      //   icon: 'calendar',
      //   name: 'news',
      //   // children: [
      //   //   { text: 'Navigation.news', name: 'news' },
      //   //   { text: 'Navigation.advertisements', name: 'advertisement' },
      //   //   { text: 'Navigation.gallery', name: 'gallery' }
      //   // ]
      // },
      {
        divider: true
      },
      // {
      //   text: 'Navigation.service-mngr',
      //   icon: 'cog',
      //   name: 'list-of-services',
      //   params: { type: 'page' },
      //   children: [
      //     { text: 'Navigation.list-services', name: 'list-of-services' },
      //     { text: 'Navigation.service-profile', name: 'service-profile' },
      //     { text: 'Navigation.docs', name: 'list-of-documents' },
      //     { text: 'Navigation.up-docs', name: 'up-docs' }
      //   ]
      // },

      // {
      //   divider: true
      // },

      // {
      //   text: 'Navigation.branch-mngr',
      //   icon: 'map',
      //   name: 'branch-manager'
      // },

      {
        text: 'Navigation.accts-mngr',
        icon: 'user-tie',
        name: 'cpanel-accounts',
        children: [
          { text: 'Navigation.cpanel-accts', name: 'cpanel-accounts' },
          { text: 'Navigation.access-control', name: 'access-control' }
        ]
      },

      {
        divider: true
      },

      // {
      //   text: 'Navigation.documents-on-hand',
      //   icon: 'file-text2',
      //   name: 'documents-on-hand'
      // },

      {
        text: 'Payments',
        icon: 'files-empty',
        name: 'payments'
      },
      {
        text: 'Navigation.financing',
        icon: 'coin-dollar',
        name: 'banks',
        children: [
          { text: 'Bank Accounts', name: 'banks' },
          { text: 'Projects', name: 'project-finance' },
          { text: 'Suppliers', name: 'banks' },
        ]
      },

      {
        text: 'Navigation.reports',
        icon: 'file-pdf',
        name: 'materials-report',
        children: [
          { text: 'Materials Request', name: 'materials-report' },
          { text: 'P&L', name: 'read-reports' },
          //{ text: 'Navigation.daily-cost', name: 'daily-cost' }
        ]
      },

      // {
      //   text: 'Navigation.dev-logs',
      //   icon: 'file-text2',
      //   name: 'development-logs'
      // },
      // {
      //   text: 'Navigation.attendance-monitoring',
      //   icon: 'clock',
      //   name: 'attendance monitoring'
      // },
       {
        text: 'Databases',
        icon: 'equalizer',
        name: 'presets',
        children: [
          // { text: 'Navigation.financing', name: 'banks', params: { type: 'page' } },
          { text: 'Project Presets', name: 'presets', params: { type: 'page' } },
          { text: 'Bundles', name: 'bundles', params: { type: 'page' } },
          { text: 'Suppliers', name: 'suppliers', params: { type: 'page' } },
          { text: 'Billers', name: 'billers', params: { type: 'page' } },
          { text: 'Cars', name: 'cars', params: { type: 'page' } },
          { text: 'Drivers', name: 'drivers', params: { type: 'page' } },
        ]
      },

            // {
      //   text: 'Project Presets',
      //   icon: 'stack',
      //   name: 'presets',
      //   params: { type: 'page' }
      // },
      
      // {
      //   text: 'Navigation.orders',
      //   icon: 'file-text2',
      //   name: 'orders'
      // },

      // {
      //   text: 'Rider Evaluation',
      //   icon: 'file-text',
      //   name: 'rider-list',
      // },
    ]
  },

  // List of Months
  months: [
    {
      id: 0,
      month: 'Months.jan'
    },
    {
      id: 1,
      month: 'Months.feb'
    },
    {
      id: 2,
      month: 'Months.march'
    },
    {
      id: 3,
      month: 'Months.april'
    },
    {
      id: 4,
      month: 'Months.may'
    },
    {
      id: 5,
      month: 'Months.jun'
    },
    {
      id: 6,
      month: 'Months.jul'
    },
    {
      id: 7,
      month: 'Months.aug'
    },
    {
      id: 8,
      month: 'Months.sep'
    },
    {
      id: 9,
      month: 'Months.oct'
    },
    {
      id: 10,
      month: 'Months.nov'
    },
    {
      id: 11,
      month: 'Months.dec'
    }
  ]
}

if (state.session && state.session.token) {
  // init axios headers
  axios.defaults.headers.Authorization = `Bearer ${state.session.token}`
}

export default state
