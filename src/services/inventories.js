/**
 * Inventories
 */

import Resource from './resource'
import APIs from './apis'

class Inventories extends Resource {
  constructor () {
    super('inventory')
  }
  getInventory (param) {
      return this.axios.get(`${APIs.GET_INVENTORY}`+ '?' + param.page + '&' + param.sort + '&' + param.search+ '&' + param.inv_id + '&' + param.perPage + '&' + param.tags)
  }

  toPayment (options) {
    return this.axios.post(`${APIs.TO_PAYMENT}`, options)
  }  

  loadToContainer (options) {
    return this.axios.post(`${APIs.LOAD_TO_CONTAINER}`, options)
  }

  saveToContainer (options) {
    return this.axios.post(`${APIs.SAVE_TO_CONTAINER}`, options)
  }

  revertSPC(options) { // revert spc to site order state
    return this.axios.post(`${APIs.REVERT_SPC}`, options)
  }  

  deleteSiteOrder(options) { // get all logs
    return this.axios.post(`${APIs.DELETE_SITE_ORDER}`, options)
  }

  rcvChina (options) {
    return this.axios.post(`${APIs.RCV_CHINA}`, options)
  }

  toPrep (options) {
    return this.axios.post(`${APIs.TO_PREP}`, options)
  }

  toShip (options) {
    return this.axios.post(`${APIs.TO_SHIP}`, options)
  }

  toRcv (options) {
    return this.axios.post(`${APIs.TO_RCV}`, options)
  }  

  transferDelete (options) {
    return this.axios.post(`${APIs.TRANSFER_DELETE}`, options)
  } 

  transferToRcv (options) {
    return this.axios.post(`${APIs.TRANSFER_TO_RCV}`, options)
  }  

  freeToRcv (options) {
    return this.axios.post(`${APIs.FREE_TO_RCV}`, options)
  }  

  freetransferRcv (options) {
    return this.axios.post(`${APIs.FREETRANSFER_RCV}`, options)
  }

  markAsRcv (options) {
    return this.axios.post(`${APIs.MARK_AS_RCV}`, options)
  }

  checkOrderDetailQty (options) {
    return this.axios.post(`${APIs.CHECK_OD_QTY}`, options)
  }   

  checkInventoryQty (options) {
    return this.axios.post(`${APIs.CHECK_INVENTORY_QTY}`, options)
  }  

  checkInventoryQty2 (options) {
    return this.axios.post(`${APIs.CHECK_INVENTORY_QTY2}`, options)
  }

  getPoDetails (options) {
    return this.axios.post(`${APIs.GET_PO_DETAILS}`, options)
  }

  getSiteOrders (param) {
      return this.axios.get(`${APIs.GET_SITE_ORDERS}`+ '?' + param.page + '&' + param.sort + '&' + param.search+ '&' + param.mode + '&' + param.perPage)
  }

  getToPrepOrders (param) {
      return this.axios.get(`${APIs.GET_TOPREP_ORDERS}`+ '?' + param.page + '&' + param.sort + '&' + param.search)
  }

  getToShipOrders (param) {
      return this.axios.get(`${APIs.GET_TOSHIP_ORDERS}`+ '?' + param.page + '&' + param.sort + '&' + param.search)
  }  

  getToShipTransfers (param) {
      return this.axios.get(`${APIs.GET_TOSHIP_TRANSFERS}`+ '?' + param.page + '&' + param.sort + '&' + param.search + '&' + param.status)
  }  

  getToRcvFreeTransfers (param) {
      return this.axios.get(`${APIs.GET_TORCV_FREETRANSFERS}`+ '?' + param.page + '&' + param.sort + '&' + param.search + '&' + param.status)
  }  

  getToShipFreeTransfers (param) {
      return this.axios.get(`${APIs.GET_TOSHIP_FREETRANSFERS}`+ '?' + param.page + '&' + param.sort + '&' + param.search + '&' + param.status)
  }

  getPocPayments (param) {
      return this.axios.get(`${APIs.GET_POC_PAYMENTS}`+ '?' + param.page + '&' + param.sort + '&' + param.search)
  }

  exportProjectQuotation (options) {
    const config = {
     method: 'post',
     responseType: 'blob',
     data: options,
     //responseType: 'application/vnd.ms-excel',
     headers: { 'Content-Type': 'application/json;charset=UTF-8'},
     url: `${APIs.EXPORT_PROJECT_QUOTATION}`,
     xsrfCookieName: 'XSRF-TOKEN',
     xsrfHeaderName: 'X-XSRF-TOKEN',
    };

    return this.axios(config)
    //return this.axios.get(`${APIs.EXPORT_POC}`+ '?' + param.ids)
  }

  exportPoc (options) {
    const config = {
     method: 'post',
     responseType: 'blob',
     data: options,
     //responseType: 'application/vnd.ms-excel',
     headers: { 'Content-Type': 'application/json;charset=UTF-8'},
     url: `${APIs.EXPORT_POC}`,
     xsrfCookieName: 'XSRF-TOKEN',
     xsrfHeaderName: 'X-XSRF-TOKEN',
    };

    return this.axios(config)
    //return this.axios.get(`${APIs.EXPORT_POC}`+ '?' + param.ids)
  }

  exportPoc2 (options) {
    const config = {
     method: 'post',
     responseType: 'blob',
     data: options,
     //responseType: 'application/vnd.ms-excel',
     headers: { 'Content-Type': 'application/json;charset=UTF-8'},
     url: `${APIs.EXPORT_POC2}`,
     xsrfCookieName: 'XSRF-TOKEN',
     xsrfHeaderName: 'X-XSRF-TOKEN',
    };

    return this.axios(config)
    //return this.axios.get(`${APIs.EXPORT_POC}`+ '?' + param.ids)
  }

  getToRcvOrders (param) {
      return this.axios.get(`${APIs.GET_TORCV_ORDERS}`+ '?' + param.page + '&' + param.sort + '&' + param.search)
  }

  getPurchaseOrders (param) {
      return this.axios.get(`${APIs.GET_PURCHASE_ORDERS}`+ '?' + param.page + '&' + param.sort + '&' + param.search + '&' + param.status)
  }

  getSpcOrders (param) {
      return this.axios.get(`${APIs.GET_SPC_ORDERS}`+ '?' + param.page + '&' + param.sort + '&' + param.search)
  }

  getCompletedOrders (param) {
      return this.axios.get(`${APIs.GET_COMPLETED_ORDERS}`+ '?' + param.page + '&' + param.sort + '&' + param.search)
  }

  getAlllOrders (param) {
      return this.axios.get(`${APIs.GET_ALLL_ORDERS}`+ '?' + param.page + '&' + param.sort + '&' + param.search)
  }

  getAllCategories (param) {
      return this.axios.get(`${APIs.GET_ALL_CATEGORIES}`)
  }

  getAllDrivers (param) {
      return this.axios.get(`${APIs.GET_ALL_DRIVERS}`)
  }

  getAllCars (param) {
      return this.axios.get(`${APIs.GET_ALL_CARS}`)
  }

  getAllBanks (param) {
      return this.axios.get(`${APIs.GET_ALL_BANKS}`)
  }

  getAllBillers (param) {
      return this.axios.get(`${APIs.GET_ALL_BILLERS}`)
  }

  addMaterial (options) {
    return this.axios.post(`${APIs.ADD_MATERIAL}`, options)
  }

  editMaterial (id, options) {
    return this.axios.post(`${APIs.EDIT_MATERIAL}` + '/' + id, options)
  }  

  editOrder (id, options) {
    return this.axios.post(`${APIs.EDIT_ORDER}` + '/' + id, options)
  }

  newPurchaseOrder (options) {
    return this.axios.post(`${APIs.NEW_PURCHASE_ORDER}`, options)
  }

  newPurchaseOrderChina (options) {
    return this.axios.post(`${APIs.NEW_PURCHASE_ORDER_CHINA}`, options)
  }

  getPurchaseOrderDetails (id, options) { // po details
    return this.axios.get(`${APIs.GET_PURCHASE_ORDER_DETAILS}` + '/' + id, options)
  }

  getPaymentDetails (id, options) { // po details
    return this.axios.get(`${APIs.GET_PAYMENT_DETAILS}` + '/' + id, options)
  }  

  getPaymentDetails2 (id, options) { // po details
    return this.axios.get(`${APIs.GET_PAYMENT_DETAILS2}` + '/' + id, options)
  }

  editPurchaseOrder (id, options) {
    return this.axios.post(`${APIs.EDIT_PURCHASE_ORDER}` + '/' + id, options)
  }  

  transferMaterials (options) {
    return this.axios.post(`${APIs.TRANSFER_MATERIALS}`, options)
  }

  transferToShip (options) {
    return this.axios.post(`${APIs.TRANSFER_TO_SHIP}`, options)
  }

  getAllDrafts (param) {
      return this.axios.get(`${APIs.GET_ALL_DRAFTS}`)
  }  

  getAllPaidPoc (param) {
      return this.axios.get(`${APIs.GET_ALL_PAID_POC}`)
  }  

  getAllContainers (param) {
      return this.axios.get(`${APIs.GET_ALL_CONTAINERS}`)
  }  

  getAllContainers2 (param) {
      return this.axios.get(`${APIs.GET_ALL_CONTAINERS2}`)
  }

  getAllPayments (param) {
      return this.axios.get(`${APIs.GET_ALL_PAYMENTS}`+ '?' + param.page + '&' + param.sort + '&' + param.search + '&' + param.viewMode + '&' + param.statusMode + '&' + param.perPage)
  }  

  getBankHistory (param) {
      return this.axios.get(`${APIs.GET_BANK_HISTORY}`+ '?' + param.page + '&' + param.sort + '&' + param.search + '&' + param.viewMode + '&' + param.statusMode + '&' + param.bankId)
  }

  getAllPaymentCategories (param) {
      return this.axios.get(`${APIs.GET_ALL_PAYMENT_CATEGORIES}`)
  }

  changePaymentCategory (options) {
    return this.axios.post(`${APIs.CHANGE_PAYMENT_CATEGORY}`, options)
  }

  approveMaterial (id) {
    return this.axios.post(`${APIs.APPROVE_MATERIAL}` + '/' + id)
  }

  approveBudget (id) {
    return this.axios.post(`${APIs.APPROVE_BUDGET}` + '/' + id)
  }

  rcvPurchaseOrder (id) {
    return this.axios.post(`${APIs.RCV_PURCHASE_ORDER}` + '/' + id)
  }

  rcvSpc (id) {
    return this.axios.post(`${APIs.RCV_SPC}` + '/' + id)
  }  

  rcvPoc (id) {
    return this.axios.post(`${APIs.RCV_POC}` + '/' + id)
  }  

  rcvTransfer (id) {
    return this.axios.post(`${APIs.RCV_TRANSFER}` + '/' + id)
  }

  addPayment (options) {
    return this.axios.post(`${APIs.ADD_PAYMENT}`, options)
  }    

  addPaymentMultipleBank (options) {
    return this.axios.post(`${APIs.ADD_PAYMENT_MULTIPLE_BANK}`, options)
  }   

  addPayment2 (options) {
    return this.axios.post(`${APIs.ADD_PAYMENT2}`, options)
  }  

  addFreeTransfer (options) {
    return this.axios.post(`${APIs.ADD_FREE_TRANSFER}`, options)
  }

  editPayment (id, options) {
    return this.axios.post(`${APIs.EDIT_PAYMENT}` + '/' + id, options)
  }  

  editPayment2 (id, options) {
    return this.axios.post(`${APIs.EDIT_PAYMENT2}` + '/' + id, options)
  }

  uploadPaymentDocuments (options) { 
    return this.axios.post(`${APIs.UPLOAD_PAYMENT_DOCUMENTS}` + '/' + options.order_id, options)
  }

  uploadFileDocuments (options) { 
    return this.axios.post(`${APIs.UPLOAD_FILE_DOCUMENTS}` , options)
  }  

  uploadPaymentsFileDocuments (options) { 
    return this.axios.post(`${APIs.UPLOAD_PAYMENT_FILE_DOCUMENTS}` , options)
  }

  receiveMaterials (options) { 
    return this.axios.post(`${APIs.RECEIVE_MATERIALS}` , options)
  }  

  toShipMaterials (options) { 
    return this.axios.post(`${APIs.TOSHIP_MATERIALS}` , options)
  }

  changeCategory (options) {
    return this.axios.post(`${APIs.CHANGE_CATEGORY}`, options)
  }

  changeUser (options) {
    return this.axios.post(`${APIs.CHANGE_USER}`, options)
  }

  updateOrderDetail (options) {
    return this.axios.post(`${APIs.UPDATE_ORDER_DETAIL}`, options)
  }  
  updateContainerDetail (options) {
    return this.axios.post(`${APIs.UPDATE_CONTAINER_DETAIL}`, options)
  }
  updateBankTransfer (options) {
    return this.axios.post(`${APIs.UPDATE_BANK_TRANSFER}`, options)
  }
}

export default new Inventories()
