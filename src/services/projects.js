/**
 * Group services
 */

import Resource from './resource'
import APIs from './apis'

class Projects extends Resource {
  constructor () {
    super('projects')
  }

  getManageProjects (options) {
    return this.axios.get(`${APIs.GET_PROJECTS_PAGINATE}` + options.options)
  }

  addProject (options) {
    return this.axios.post(`${APIs.GET_PROJECTS}`, options)
  }

  editProject (id, options) {
    return this.axios.patch(`${APIs.GET_PROJECTS}` + '/' + id, options)
  }

  getProjectTransactions (id, options) {
    return this.axios.post(`${APIs.GET_PROJECT_TRANSACTIONS}` + '/' + id, options)
  }

  addProjectTask (options) {
    return this.axios.post(`${APIs.ADD_PROJECT_TASKS}`, options)
  }

  editProjectTask (options) {
    return this.axios.post(`${APIs.EDIT_PROJECT_TASKS}`, options)
  }

  getProjectTask(task_id) { // get all logs
    return this.axios.get(`${APIs.GET_PROJECT_TASK}`+ '/' +  task_id)
  }    

  getProjectTaskWithOrders(task_id) { // get all logs
    return this.axios.get(`${APIs.GET_PROJECT_TASK_WITH_ORDERS}`+ '/' +  task_id)
  }  

  getPaymentInfo(payment_id) { // get all logs
    return this.axios.get(`${APIs.GET_PAYMENT_INFO}`+ '/' +  payment_id)
  }

  getOrderDetail(order_id) { // get all logs
    return this.axios.get(`${APIs.GET_ORDER_DETAILS}`+ '/' +  order_id)
  }

  getOrderDetail2(order_id) { // get all logs
    return this.axios.get(`${APIs.GET_ORDER_DETAILS2}`+ '/' +  order_id)
  }

  getOrderDetailMultiple(order) { // get all logs
    return this.axios.post(`${APIs.GET_ORDER_DETAIL_MULTIPLE}`, order)
  }  

  getOrderDetailMultiple2(order) { // get all logs
    return this.axios.post(`${APIs.GET_ORDER_DETAIL_MULTIPLE2}`, order)
  }

  projectSearch (param) { // group search
    return this.axios.get(`${APIs.PROJECTS_SEARCH}` + '?search=' + param.keyword)
  }

  getAllTasks (param) {
      return this.axios.get(`${APIs.GET_ALL_TASKS}`+ '?' + param.page + '&' + param.sort + '&' + param.search + '&'  + param.project_id+ '&'  + param.filter)
  }   

  getPerTeamTasks (param) {
      return this.axios.get(`${APIs.GET_PERTEAM_TASKS}`+ '?' + param.page + '&' + param.sort + '&' + param.search + '&'  + param.team )
  } 

  getPerTeamCount (id) {
    return this.axios.get(`${APIs.GET_PERTEAM_COUNT}`)
  }

  uploadTaskImages (options) {
    return this.axios.post(`${APIs.UPLOAD_TASK_IMAGES}`, options)
  }  

  bankTransfer (options) {
    return this.axios.post(`${APIs.BANK_TRANSFER}`, options)
  }

  getProjectProfile (id) {
    return this.axios.get(`${APIs.GET_PROJECTS}` + '/' + id)
  }

  getProjectProfile2 (id) {
    return this.axios.get(`${APIs.GET_PROJECT_PROFILE}` + '/' + id)
  }

  getProjectDetails (options) {
    return this.axios.post(`${APIs.GET_PROJECT_DETAILS}`, options)
  }

  getAllPresets (options) { // for demo purposes, it needs pagination to work properly
    return this.axios.post(`${APIs.GET_PRESETS}`, options)
  }  

  getAllPresets2 (options) { // for demo purposes, it needs pagination to work properly
    return this.axios.post(`${APIs.GET_PRESETS2}`, options)
  }

  getAllSuppliers (options) { 
    return this.axios.get(`${APIs.GET_ALL_SUPPLIERS}`, options)
  }

  getSelectedPresets (options) {
    return this.axios.post(`${APIs.GET_SELECTED_PRESETS}`, options)
  }

  addPurchaseOrder(options) { // POST method add client service
    return this.axios.post(`${APIs.ADD_PURCHASE_ORDER}`, options)
  }  

  addPurchaseOrder2(options) { // POST method add client service
    return this.axios.post(`${APIs.ADD_PURCHASE_ORDER2}`, options)
  }  

  clearMob(options) { // POST method add client service
    return this.axios.post(`${APIs.CLEAR_MOB}`, options)
  }

  requestMaterials(options) { // POST method add client service
    return this.axios.post(`${APIs.REQUEST_MATERIALS}`, options)
  }

  requestLowQty(options) { // POST method add client service
    return this.axios.post(`${APIs.REQUEST_LOW_QTY}`, options)
  }

  deleteTask(options) { // get all logs
    return this.axios.post(`${APIs.DELETE_TASK}`, options)
  }

  deleteComment(options) { // get all logs
    return this.axios.post(`${APIs.DELETE_COMMENT}`, options)
  }  

  deletePaymentComment(options) { // get all logs
    return this.axios.post(`${APIs.DELETE_PAYMENT_COMMENT}`, options)
  }  

  deletePayment(options) { // get all logs
    return this.axios.post(`${APIs.DELETE_PAYMENT}`, options)
  }

  allUsers(options) { // get all logs
    return this.axios.get(`${APIs.ALL_USERS}`, options)
  }

  toggleTaskPriority (options) {
    return this.axios.post(`${APIs.TOGGLE_TASK_PRIORITY}`, options)
  }

  markTaskComplete (options) {
    return this.axios.post(`${APIs.MARK_TASK_COMPLETE}`, options)
  }

  markTaskIncomplete (options) {
    return this.axios.post(`${APIs.MARK_TASK_INCOMPLETE}`, options)
  }

  deleteTaskImage (data) {
    return this.axios.post(`${APIs.DELETE_TASK_IMAGE}`, data)
  }  

  deleteMaterialImage (data) {
    return this.axios.post(`${APIs.DELETE_MATERIAL_IMAGE}`, data)
  }

  markAsRead (data) {
    return this.axios.post(`${APIs.MARK_AS_READ}`, data)
  }

}

export default new Projects()
