/**
 * Inventories
 */

import Resource from './resource'
import APIs from './apis'

class Locations extends Resource {
  constructor () {
    super('location')
  }

  getLocation (data) {
    return this.axios.get(`${APIs.GET_LOCATION}?type=${data.type}`)
  }

  getLocationDetail (data) {
    return this.axios.get(`${APIs.GET_LOCATION_DETAIL}?id=${data.loc_id}`)
  }

  getLocationConsumable (data) {
    return this.axios.post(`${APIs.GET_LOCATION_CONSUMABLE}`, data)
  }

}

export default new Locations()
