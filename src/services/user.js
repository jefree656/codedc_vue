/**
 * Users service
 */

import Resource from './resource'
import APIs from './apis'

class User extends Resource {
  constructor () {
    super('user')
  }

  getCurrentUser () {
    return this.axios.get(`${APIs.GET_CURRENT_USER}`)
  }

}

export default new User()
