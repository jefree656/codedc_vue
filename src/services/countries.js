/**
 * Country services
 */

import Resource from './resource'
import APIs from './apis'

class Countries extends Resource {
  constructor () {
    super('countries')
  }

  getAllCountries () {
    return this.axios.get(`${APIs.GET_COUNTRIES}`)
  }

  getAllNationalities () {
    return this.axios.get(`${APIs.GET_NATIONALITIES}`)
  }
}

export default new Countries()
