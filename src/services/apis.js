/*
  Current APIs
*/

module.exports = {
  LOGIN_USER: 'v1/login',
  LOGOUT_USER: 'v1/logout',
  GET_CURRENT_USER: 'v1/user-information',

  GET_ROLES: 'v1/roles',
  GET_ROLE: 'v1/roles/get-role',
  ADD_ROLE: 'v1/roles/add-role',
  GET_PERMISSIONS_BY_ROLE: 'v1/roles/get-role-permissions',
  UPDATE_ROLE_ACCESS: 'v1/roles/update-role-access',
  GET_PERMISSIONS: 'v1/permissions',

  /** DASHBOARD **/

  /** END DASHBOARD **/

  /** CUSTOMERS **/
  GET_CLIENTS: 'v1/clients/manage-clients',
  GET_CLIENTS_PAGINATE: 'v1/clients/manage-clients-paginate',
  CLIENTS_SEARCH: 'v1/clients/search',
  ADD_CLIENT: 'v1/clients',
  /** END CUSTOMERS **/

  /** PRESETS **/
  GET_PRESETS_PAGINATE: 'v1/presets/manage-presets-paginate',
  GET_MANAGE_PRESETS: 'v1/presets/manage-presets',
  PRESETS_SEARCH: 'v1/presets/search',
  GET_PRESET: 'v1/presets',
  ADD_BUNDLE: 'v1/presets/add-bundle',
  GET_MATERIALS: 'v1/presets/get-materials',
  LOAD_ALL_PRESETS: 'v1/presets/load-all-presets',
  LOAD_PRESETS_SELECTED: 'v1/presets/load-presets-selected',
  GET_ALL_BUNDLES: 'v1/presets/manage-bundles-paginate',
  GET_SELECTED_MATERIALS: 'v1/presets/get-selected-materials',
  GET_BUNDLE_PRESETS: 'v1/presets/get-bundle-presets',
  UPDATE_PRESET_MATERIALS: 'v1/presets/update-preset-materials',
  UPDATE_BUNDLE_PRESETS: 'v1/presets/update-bundle-presets',
  /** END PRESETS **/


  /** SUPPLIERS **/
  GET_SUPPLIERS_PAGINATE: 'v1/suppliers/manage-suppliers-paginate',
  GET_SUPPLIER: 'v1/suppliers',
  GET_ALL_SUPPLIERS: 'v1/suppliers/get-all-suppliers',
  LOAD_BILLER_DATA: 'v1/suppliers/load-biller-data',
  ADD_BILLER: 'v1/suppliers/add-biller',
  EDIT_BILLER: 'v1/suppliers/edit-biller',
  LOAD_CAR_DATA: 'v1/suppliers/load-car-data',
  ADD_CAR: 'v1/suppliers/add-car',
  EDIT_CAR: 'v1/suppliers/edit-car',
  /** END SUPPLIERS **/

  /** PROJECTS **/
  GET_PROJECTS_PAGINATE: 'v1/projects/manage-projects-paginate',
  GET_MANAGE_PROJECTS: 'v1/projects/manage-projects',
  PROJECTS_SEARCH: 'v1/projects/search',
  GET_PROJECTS: 'v1/projects',
  GET_ALL_TASKS: 'v1/projects/get-all-tasks',
  GET_PERTEAM_TASKS: 'v1/projects/get-perteam-tasks',
  GET_PERTEAM_COUNT: 'v1/projects/get-perteam-count',
  GET_PROJECT_PROFILE: 'v1/projects/get-profile',
  GET_PROJECT_DETAILS: 'v1/projects/get-details',
  ADD_PROJECT_TASKS: 'v1/projects/add-task',
  EDIT_PROJECT_TASKS: 'v1/projects/edit-task',
  GET_PROJECT_TASK: 'v1/projects/get-task',
  GET_PROJECT_TASK_WITH_ORDERS: 'v1/projects/get-task-with-orders',
  GET_PAYMENT_INFO: 'v1/purchase-order/get-payment-info',
  GET_ORDER_DETAILS: 'v1/projects/get-order-details',
  GET_ORDER_DETAILS2: 'v1/projects/get-order-details2',
  GET_ORDER_DETAIL_MULTIPLE: 'v1/projects/get-order-detail-multiple',
  GET_ORDER_DETAIL_MULTIPLE2: 'v1/projects/get-order-detail-multiple2',
  GET_PRESETS: 'v1/projects/get-presets',
  GET_PRESETS2: 'v1/projects/get-presets2',
  GET_SELECTED_PRESETS: 'v1/projects/get-selected-presets',
  ADD_PURCHASE_ORDER: 'v1/projects/add-purchase-order',
  ADD_PURCHASE_ORDER2: 'v1/projects/add-purchase-order2',
  CLEAR_MOB: 'v1/projects/clear-mob',
  REQUEST_MATERIALS: 'v1/projects/request-materials',
  REQUEST_LOW_QTY: 'v1/projects/request-low-qty',
  GET_PROJECT_TRANSACTIONS: 'v1/projects/get-transactions',
  DELETE_TASK: 'v1/projects/delete-task',
  DELETE_COMMENT: 'v1/projects/delete-comment',
  DELETE_PAYMENT_COMMENT: 'v1/purchase-order/delete-payment-comment',
  DELETE_PAYMENT: 'v1/purchase-order/delete-payment',
  ALL_USERS: 'v1/projects/all-users',
  TOGGLE_TASK_PRIORITY: 'v1/projects/toggle-task-priority',
  MARK_TASK_COMPLETE : 'v1/projects/mark-task-complete',
  MARK_TASK_INCOMPLETE : 'v1/projects/mark-task-incomplete',
  /** END PROJECTS **/

  /** LOGS **/
  GET_TRANSACTION_LOGS: 'v1/logs/get-transaction-logs',
  GET_TRANSACTION_HISTORY: 'v1/logs/get-transaction-history',
  GET_OLD_TRANSACTION_LOGS: 'v1/logs/get-old-transaction-logs',
  GET_GROUP_TRANSACTION_LOGS: 'v1/logs/get-group-transaction-logs',
  GET_COMMISSION_LOGS: 'v1/logs/get-commission-logs',
  GET_ACTION_LOGS: 'v1/logs/get-action-logs',
  GET_DOCUMENT_LOGS: 'v1/logs/get-document-logs',
  GET_ALL_LOGS: 'v1/logs/get-all-logs',
  DELETE_LATEST_DOCUMENT_LOG: 'v1/logs/delete-latest-document-log',
  GET_SERVICE_HISTORY: 'v1/logs/get-service-history',
  /** END LOGS **/

  /** FINANCING **/
  GET_BANKS: 'v1/financing/get-banks',
  ADD_BANK: 'v1/financing/add-bank',
  EDIT_BANK: 'v1/financing/edit-bank',
  ADD_QUOTATION: 'v1/financing/add-quotation',
  EDIT_QUOTATION: 'v1/financing/edit-quotation',
  GET_PROJECT_COLLECTABLES: 'v1/financing/get-project-collectables',
  ADD_FINANCE: 'v1/financing',
  GET_FINANCING: 'v1/financing/show',
  GET_BORROWED: 'v1/financing/get-borrowed',
  GET_REQ_USERS: 'v1/financing/get-req-users',
  EXPORT_FINANCIAL: 'v1/financing/export-financial',

  // Finaning delivery
  GET_FINANCING_DELIVERY: 'v1/financing/delivery/show',
  ADD_PURCHASING_BUDGET: 'v1/financing/add-purchasing-budget',
  ADD_DELIVERY_FINANCE: 'v1/financing/add-delivery-finance',
  UPDATE_DELIVERY_FINANCE: 'v1/financing/update-delivery-finance',
  UPDATE_DELIVERY_ROW: 'v1/financing/update-delivery-row',
  DELETE_DELIVERY_ROW: 'v1/financing/delete-delivery-row',
  GET_RETURN_LIST: 'v1/financing/get-return-list',

  /** END SERVICE PROCEDURES **/


  /** ACCOUNTS MANAGER **/
  GET_ALL_CPANEL_USERS: 'v1/accounts/get-cpanel-users',
  GET_CPANEL_USER: 'v1/accounts',
  ADD_CPANEL_USER: 'v1/accounts',
  UPDATE_CPANEL_USER: 'v1/accounts',
  DELETE_CPANEL_USER: 'v1/accounts',
  /** END ACCOUNTS MANAGER **/

  /** WORKER **/
  GET_WORKER: 'v1/accounts/get-worker',
  UPDATE_WORKER: 'v1/accounts/update-worker',
  ADD_NEW_WORKER: 'v1/accounts/add-new-worker',
  SAVE_WORKER_ATTENDANCE: 'v1/accounts/save-worker-attendance',
  SAVE_WORKER_ATTENDANCE_MULTIPLE: 'v1/accounts/save-worker-attendance-multiple',
  GET_DAY_ATTENDANCE: 'v1/accounts/get-day-attendance',
  GET_ALL_WORKERS: 'v1/accounts/get-all-workers',
  /** END WORKER**/


  /** EMPLOYEE DOCUMENTS ON HAND **/
  GET_EMPLOYEE_ON_HAND_DOCUMENTS: 'v1/logs/get-employee-documents-onhand',
  GET_CLIENTS_ON_HAND_DOCUMENTS: 'v1/logs/get-clients-in-docs-onhand',
  EXPORT_EMPLOYEE_ON_HAND_DOCUMENTS: 'v1/logs/export-employee-documents-onhand',
  /** END EMPLOYEE DOCUMENTS ON HAND **/

  /** REPORTS MANAGER **/
  REPORTS: 'v1/reports',
  GET_REPORTS: 'v1/reports',
  PL_REPORTS: 'v1/reports/get-pl-reports',
  MATERIAL_REPORTS: 'v1/reports/get-material-reports',
  GET_PL_DETAILS: 'v1/reports/get-pl-details',
  GET_PL_DETAILS2: 'v1/reports/get-pl-details2',
  EXPORT_PL_REPORT: 'v1/reports/export-pl-report',
  EXPORT_MATERIAL_REPORT: 'v1/reports/export-material-report',
  /** END REPORTS MANAGER **/

  
  /** ORDERS **/
  // GET_ALL_ORDERS: 'v1/order/list',
  VIEW_ORDER: 'v1/order',
  VIEW_ORDER_LOG: 'v1/order/view-log',
  ADD_PRODUCT_CATEGORY: 'v1/order/add-product-category',
  UPDATE_PRODUCT_CATEGORY: 'v1/order/update-product-category',
  REMOVE_PRODUCT_CATEGORY: 'v1/order/remove-product-category',
  MOVE_PRODUCT_CATEGORY: 'v1/order/move-product-category',
  GET_PRODUCT_CATEGORIES: 'v1/order/product-category',
  GET_PRODUCT_CATEGORY_DETAILS: 'v1/order/get-category-details',
  GET_PRODUCT_CATEGORIES_BY_ID : 'v1/order/get-product-category',
  GET_CATEGORIES_AND_PRODUCT_BY_ID : 'v1/order/get-category-and-products',
  GET_ALL_CATEGORIES : 'v1/order/get-all-product-category',
  GET_PRODUCTS_BY_CAT_ID: 'v1/order/products',
  CHECK_PRODUCTS_IN_ORDER_DETAILS: 'v1/order/check-products-in-order-details',
  REMOVE_PRODUCT: 'v1/order/remove-product',
  MOVE_PRODUCT: 'v1/order/move-product',
  SAVE_ORDERS: 'v1/order',
  DELETE_ORDER: 'v1/order',
  MARK_COMPLETE: 'v1/order/mark-complete',
  UPDATE_PRODUCT: 'v1/order/update-product',
  ADD_PRODUCT: 'v1/order/add-product',
  UPDATE_ORDERS: 'v1/order',
  ORDER_SUMMARY: 'v1/order/summary',
  UPLOAD_PRODUCT: 'v1/order/product-upload',
  USER_ORDER_LIST: 'v1/order/order-list',
  /** END ORDERS **/

  /* DOCUMENTS */
  UPLOAD_PAYMENT_DOCUMENTS: 'v1/inventory/upload-payment-documents',
  UPLOAD_FILE_DOCUMENTS: 'v1/inventory/upload-file-documents',
  UPLOAD_PAYMENT_FILE_DOCUMENTS: 'v1/inventory/upload-payment-file-documents',
  /* DOCUMENTS */


  /** INVENTORIES **/

  UPDATE_ORDER_DETAIL: 'v1/inventory/update-order-detail',
  UPDATE_CONTAINER_DETAIL: 'v1/inventory/update-container-detail',
  UPDATE_BANK_TRANSFER: 'v1/purchase-order/update-bank-transfer',
  DELETE_SITE_ORDER: 'v1/inventory/delete-site-order',
  REVERT_SPC: 'v1/inventory/revert-spc',
  GET_INVENTORY: 'v1/inventory/get-inventory',
  CHECK_OD_QTY: 'v1/inventory/check-od-qty',
  CHECK_INVENTORY_QTY: 'v1/inventory/check-inventory-qty',
  CHECK_INVENTORY_QTY2: 'v1/inventory/check-inventory-qty2',
  GET_PO_DETAILS: 'v1/inventory/get-po-details',
  TO_PAYMENT: 'v1/inventory/to-payment',
  LOAD_TO_CONTAINER: 'v1/inventory/load-to-container',
  SAVE_TO_CONTAINER: 'v1/inventory/save-to-container',
  RCV_CHINA: 'v1/inventory/rcv-china',
  TO_PREP: 'v1/inventory/to-prep',
  TO_SHIP: 'v1/inventory/to-ship',
  TO_RCV: 'v1/inventory/to-rcv',
  TRANSFER_DELETE: 'v1/inventory/transfer-delete',
  TRANSFER_TO_RCV: 'v1/inventory/transfer-to-rcv',
  FREE_TO_RCV: 'v1/inventory/freetransfer-to-rcv',
  FREETRANSFER_RCV: 'v1/inventory/freetransfer-rcv',
  RECEIVE_MATERIALS: 'v1/inventory/receive-materials',
  TOSHIP_MATERIALS: 'v1/inventory/toship-materials',
  MARK_AS_RCV: 'v1/inventory/mark-as-rcv',
  GET_SITE_ORDERS: 'v1/inventory/get-site-orders',
  GET_TOPREP_ORDERS: 'v1/inventory/get-toprep-orders',
  GET_TOSHIP_ORDERS: 'v1/inventory/get-toship-orders',
  GET_TOSHIP_TRANSFERS: 'v1/inventory/get-toship-transfers',
  GET_TOSHIP_FREETRANSFERS: 'v1/inventory/get-toship-freetransfers',
  GET_TORCV_FREETRANSFERS: 'v1/inventory/get-torcv-freetransfers',
  ADD_FREE_TRANSFER: 'v1/inventory/add-free-transfer',
  GET_POC_PAYMENTS: 'v1/inventory/get-poc-payments',
  EXPORT_POC: 'v1/inventory/export-poc',
  EXPORT_POC2: 'v1/inventory/export-poc2',
  //EXPORT_POC2: 'v1/inventory/export-poc2',
  //EXPORT_EXCEL_GROUP_SUMMARY: 'v1/groups/group-summary',
  EXPORT_PROJECT_QUOTATION: 'v1/projects/export-project-quotation',
  GET_TORCV_ORDERS: 'v1/inventory/get-torcv-orders',
  GET_COMPLETED_ORDERS: 'v1/inventory/get-completed-orders',
  GET_ALLL_ORDERS: 'v1/inventory/get-alll-orders',
  GET_ALL_CATEGORIES: 'v1/inventory/get-all-categories',
  GET_ALL_DRIVERS: 'v1/inventory/get-all-drivers',
  GET_ALL_CARS: 'v1/inventory/get-all-cars',
  ADD_MATERIAL: 'v1/inventory/add-material',
  BANK_TRANSFER: 'v1/projects/bank-transfer',
  UPLOAD_TASK_IMAGES: 'v1/projects/upload-task-images',
  DELETE_TASK_IMAGE : 'v1/projects/delete-task-image',
  DELETE_MATERIAL_IMAGE : 'v1/projects/delete-material-image',
  MARK_AS_READ : 'v1/projects/mark-as-read',
  EDIT_MATERIAL: 'v1/inventory/edit-material',
  EDIT_ORDER: 'v1/inventory/edit-order',
  TRANSFER_MATERIALS: 'v1/inventory/transfer-materials',
  TRANSFER_TO_SHIP: 'v1/inventory/transfer-to-ship',
  APPROVE_MATERIAL: 'v1/inventory/approve-material',
  APPROVE_BUDGET: 'v1/inventory/approve-budget',
  CHANGE_CATEGORY: 'v1/inventory/change-category',
  CHANGE_USER: 'v1/inventory/change-user',
  /** END INVENTORIES **/

  RCV_PURCHASE_ORDER: 'v1/purchase-order/rcv-purchase-order',
  RCV_SPC: 'v1/purchase-order/rcv-spc',
  RCV_POC: 'v1/purchase-order/rcv-poc',
  RCV_TRANSFER: 'v1/inventory/rcv-transfer',
  NEW_PURCHASE_ORDER: 'v1/purchase-order',
  NEW_PURCHASE_ORDER_CHINA: 'v1/purchase-order/pochina',
  GET_PURCHASE_ORDERS: 'v1/purchase-order/get-all-purchase-orders',
  GET_SPC_ORDERS: 'v1/purchase-order/get-all-spc-orders',
  GET_PURCHASE_ORDER_DETAILS: 'v1/purchase-order',
  EDIT_PURCHASE_ORDER: 'v1/purchase-order',
  GET_ALL_DRAFTS: 'v1/purchase-order/get-all-drafts',
  GET_ALL_PAID_POC: 'v1/purchase-order/get-all-paid-poc',
  GET_ALL_CONTAINERS: 'v1/purchase-order/get-all-containers',
  GET_ALL_CONTAINERS2: 'v1/purchase-order/get-all-containers2',
  GET_ALL_BANKS: 'v1/purchase-order/get-all-banks',
  GET_ALL_BILLERS: 'v1/purchase-order/get-all-billers',
  GET_BANK_HISTORY: 'v1/purchase-order/get-bank-history',
  GET_ALL_PAYMENTS: 'v1/purchase-order/get-all-payments',
  GET_ALL_PAYMENT_CATEGORIES: 'v1/purchase-order/get-all-payment-categories',
  GET_PAYMENT_DETAILS: 'v1/purchase-order/get-payment-details',
  GET_PAYMENT_DETAILS2: 'v1/purchase-order/get-payment-details2',
  CHANGE_PAYMENT_CATEGORY: 'v1/purchase-order/change-payment-category',
  ADD_PAYMENT: 'v1/purchase-order/add-payment',
  ADD_PAYMENT_MULTIPLE_BANK: 'v1/purchase-order/add-payment-multiple-bank',
  ADD_PAYMENT2: 'v1/purchase-order/add-payment2',
  EDIT_PAYMENT: 'v1/purchase-order/edit-payment',
  EDIT_PAYMENT2: 'v1/purchase-order/edit-payment2',

  /** ATTENDANCE **/
  GET_ALL_ATTENDANCE: 'v1/attendance',
  GENERATE_PAYROLL: 'v1/attendance/generate-payroll',
  COMPUTE_PAYROLL: 'v1/attendance/compute-payroll',
}
