/**
 * Tokens service
 */

import Resource from './resource'
import APIs from './apis'

class Tokens extends Resource {
  constructor () {
    super('tokens')
  }

  getAccessToken (options) {
    return this.axios.post(`${APIs.LOGIN_USER}`, options)
  }

  removeToken (){
    return this.axios.post(`${APIs.LOGOUT_USER}`)
  }

}

export default new Tokens()
