/**
 * Roles
 */

import Resource from './resource'
import APIs from './apis'

class Roles extends Resource {
  constructor () {
    super('roles')
  }

  getRoles (options) {
    return this.axios.get(`${APIs.GET_ROLES}`, options)
  }

  getPermissions (options) {
    return this.axios.get(`${APIs.GET_PERMISSIONS}`, options)
  }

  getRole (options) {
    return this.axios.get(`${APIs.GET_ROLE}` + '/' + options.perPage + '?' + options.page + '&' + options.sort + '&' + options.search)
  }

  addRole (data) {
    return this.axios.post(`${APIs.ADD_ROLE}`, data)
  }

  getPermissionsByRole(data) {
    return this.axios.get(`${APIs.GET_PERMISSIONS_BY_ROLE}`+ '/' + data)
  }

  updateRoleAccess (data) {
    return this.axios.post(`${APIs.UPDATE_ROLE_ACCESS}`, data)
  }
}

export default new Roles()
