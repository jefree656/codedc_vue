/**
 * Group services
 */

import Resource from './resource'
import APIs from './apis'

class Suppliers extends Resource {
  constructor () {
    super('suppliers')
  }

  getManageSuppliers (options) {
    return this.axios.get(`${APIs.GET_SUPPLIERS_PAGINATE}` + options.options)
  }


  addSupplier (options) {
    return this.axios.post(`${APIs.GET_SUPPLIER}`, options)
  }

  editSupplier (id, options) {
    return this.axios.patch(`${APIs.GET_SUPPLIER}` + '/' + id, options)
  }

  loadBillerData (options) {
    return this.axios.get(`${APIs.LOAD_BILLER_DATA}` + options.options)
  }  

  loadCarData (options) {
    return this.axios.get(`${APIs.LOAD_CAR_DATA}` + options.options)
  }

  addBiller (options) {
    return this.axios.post(`${APIs.ADD_BILLER}`, options)
  }

  editBiller (id, options) {
    return this.axios.patch(`${APIs.EDIT_BILLER}` + '/' + id, options)
  }

  addCar (options) {
    return this.axios.post(`${APIs.ADD_CAR}`, options)
  }

  editCar (id, options) {
    return this.axios.patch(`${APIs.EDIT_CAR}` + '/' + id, options)
  }


}

export default new Suppliers()
