/**
 * Logs
 */

import Resource from './resource'
import APIs from './apis'

class Logs extends Resource {
  constructor () {
    super('logs')
  }

  getTransactionLogs (client_id,group_id) { // get transaction logs
    return this.axios.get(`${APIs.GET_TRANSACTION_LOGS}`+ '/' +  client_id + '/' + group_id)
  }

  getTransactionHistory (client_id,group_id) { // get transaction history
    return this.axios.get(`${APIs.GET_TRANSACTION_HISTORY}`+ '/' +  client_id + '/' + group_id)
  }

  getOldTransactionLogs (client_id,group_id,lastBalance) { // get transaction logs
    return this.axios.get(`${APIs.GET_OLD_TRANSACTION_LOGS}`+ '/' +  client_id + '/' + group_id+ '/' + lastBalance)
  }

  getGroupTransactionLogs (client_id,group_id) { // get transaction logs
    return this.axios.get(`${APIs.GET_GROUP_TRANSACTION_LOGS}`+ '/' +  client_id + '/' + group_id)
  }

  getCommissionLogs (client_id,group_id) { // get transaction logs
    return this.axios.get(`${APIs.GET_COMMISSION_LOGS}`+ '/' +  client_id + '/' + group_id)
  }

  getActionLogs (client_id,group_id) { // get action logs
    return this.axios.get(`${APIs.GET_ACTION_LOGS}`+ '/' +  client_id + '/' + group_id)
  }

  getDocumentLogs(client_id) {
    return this.axios.get(`${APIs.GET_DOCUMENT_LOGS}`+ '/' +  client_id)
  }

  getAllLogs(client_service_id) { // get all logs
    return this.axios.get(`${APIs.GET_ALL_LOGS}`+ '/' +  client_service_id)
  }

  deleteLatestDocumentLog(client_id) { // get all logs
    return this.axios.post(`${APIs.DELETE_LATEST_DOCUMENT_LOG}`, client_id)
  }

  getServiceHistory(group_id){
    return this.axios.get(`${APIs.GET_SERVICE_HISTORY}`+ '/' +  group_id)
  }

}

export default new Logs()
