/**
 * Logs
 */

import Resource from './resource'
import APIs from './apis'

class Orders extends Resource {
  constructor () {
    super('orders')
  }

  // getAllOrders(options) { // get all orders
  //   return this.axios.get(`${APIs.GET_ALL_ORDERS}` + options.options)
  // }

  getAllOrders (options) { // orders list with pagination
    if (typeof options.perPage === 'undefined') {
      return this.axios.get(`${APIs.GET_ALL_ORDERS}` + '?' + options.page)
    }

    return this.axios.get(`${APIs.GET_ALL_ORDERS}` + '/' + options.perPage + '?' + options.page)
  }

  viewOrder(id){
      return this.axios.get(`${APIs.VIEW_ORDER}`+ '/' +  id)
  }

  viewOrderLog(id){
      return this.axios.get(`${APIs.VIEW_ORDER_LOG}`+ '/' +  id)
  }

  addProductCategory(data) { // add product categories
    return this.axios.post(`${APIs.ADD_PRODUCT_CATEGORY}`, data)
  }

  updateProductCategory(data) { // add product categories
    return this.axios.post(`${APIs.UPDATE_PRODUCT_CATEGORY}`, data)
  }

  removeProductCategory(data) { // add product categories
    return this.axios.post(`${APIs.REMOVE_PRODUCT_CATEGORY}`, data)
  }

  moveProductCategory(data) { // add product categories
    return this.axios.post(`${APIs.MOVE_PRODUCT_CATEGORY}`, data)
  }

  getProductCategories() { // get product categories
    return this.axios.get(`${APIs.GET_PRODUCT_CATEGORIES}`)
  }

  getProductCategoryDetails(id) { // get product categories
    return this.axios.get(`${APIs.GET_PRODUCT_CATEGORY_DETAILS}` + '/' + id)
  }

  getProductByCatId(data) {
    console.log('testdata : '+data);
    return this.axios.get(`${APIs.GET_PRODUCTS_BY_CAT_ID}`+ '/' + data)
  }

  getProductCategoriesByID(id) { // get product categories
    return this.axios.get(`${APIs.GET_PRODUCT_CATEGORIES_BY_ID}` + '/' + id)
  }

  getCategoriesAndProductByID(id) { // get product categories
    return this.axios.get(`${APIs.GET_CATEGORIES_AND_PRODUCT_BY_ID}` + '/' + id)
  }

  getAllProductCategories() { // get product categories
    return this.axios.get(`${APIs.GET_ALL_CATEGORIES}`)
  }

  saveOrders(data) {
    return this.axios.post(`${APIs.SAVE_ORDERS}`, data)
  }

  updateOrders(data) {
    // console.log(data);
    return this.axios.patch(`${APIs.UPDATE_ORDERS}` + '/' + data.order_id, data.payload)
    //  return this.axios.post(`${APIs.SAVE_ORDERS}`, data.payload)
  }


  deleteOrder(id) {
    return this.axios.delete(`${APIs.DELETE_ORDER}`+ '/' + id)
  }

  markComplete(data) {
    return this.axios.post(`${APIs.MARK_COMPLETE}`, data)
  }

  updateProduct(data) {
    return this.axios.post(`${APIs.UPDATE_PRODUCT}`, data)
  }

  addProduct(data) {
    return this.axios.post(`${APIs.ADD_PRODUCT}`, data)
  }

  orderSummary(data) {
    return this.axios.post(`${APIs.ORDER_SUMMARY}`, data)
  }

  uploadProduct (options) { 
    return this.axios.post(`${APIs.UPLOAD_PRODUCT}` + '/' + options.id, options)
  }

  checkProductsInOrderDetails (id) { 
    return this.axios.get(`${APIs.CHECK_PRODUCTS_IN_ORDER_DETAILS}` + '/' + id)
  }

  removeProduct (data) { 
    return this.axios.post(`${APIs.REMOVE_PRODUCT}`, data)
  }

  moveProduct (data) { 
    return this.axios.post(`${APIs.MOVE_PRODUCT}`, data)
  }

  userOrderList (id, data) { 
    return this.axios.post(`${APIs.USER_ORDER_LIST}` + '/' + id, data)
  }

}

export default new Orders()
