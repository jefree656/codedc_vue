/**
 * Services
 *
 */

import TokenService from './tokens'
import UserService from './user'
import ClientService from './clients'
import PresetService from './presets'
import SupplierService from './suppliers'
import ProjectService from './projects'
import EmployeeService from './employee'
import ReportsService from './reports'
import LogsService from './logs'
import AccountsService from './accounts'
import RolesService from './roles'
import FinancingService from './financing'
import OrderService from './orders'
import InventoryService from './inventories'
import DocumentService from './documents'
import Attendance from './attendance'


export {
  TokenService,
  UserService,
  ClientService,
  PresetService,
  SupplierService,
  ProjectService,
  EmployeeService,
  ReportsService,
  LogsService,
  AccountsService,
  RolesService,
  FinancingService,
  OrderService,
  InventoryService,
  DocumentService,
  Attendance
}
