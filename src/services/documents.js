/**
 * Country services
 */

import Resource from './resource'
import APIs from './apis'

class Documents extends Resource {
  constructor () {
    super('documents')
  }

  uploadDocuments (options) { // for demo purposes, it needs pagination to work properly
    return this.axios.post(`${APIs.ADD_CLIENT_DOCUMENTS}`, options)
  }

  getUploadedClientDocuments (id) { // for demo purposes, it needs pagination to work properly
    return this.axios.get(`${APIs.GET_UPLOADED_CLIENT_DOCUMENTS}` + '/' + id)
  }

  getDocumentTypeList () { 
    return this.axios.post(`${APIs.GET_ALL_DOCUMENT_TYPE}`)
  }

  uploadDocumentsByClient (options) { 
    return this.axios.post(`${APIs.UPLOAD_DOCUMENTS_BY_CLIENT}` + '/' + options.client_id, options)
  }

  // uploadPaymentDocuments (options) { 
  //   return this.axios.post(`${APIs.UPLOAD_PAYMENT_DOCUMENTS}` + '/' + options.order_id, options)
  // }

  getDocsByClient(options) {
    return this.axios.get(`${APIs.GET_DOCUMENTS_BY_CLIENT}` + '/' + options.id)
  }

  deleteDocsByClient(options) {
    return this.axios.post(`${APIs.DELETE_DOCUMENT_BY_CLIENT}`, options)
  }

  getDocumentsOnHand(options) {
    return this.axios.get(`${APIs.GET_DOCUMENTS_ON_HAND}`+ '/' +  options.client_id)
  }
  
}

export default new Documents()
