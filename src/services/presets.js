/**
 * Group services
 */

import Resource from './resource'
import APIs from './apis'

class Presets extends Resource {
  constructor () {
    super('presets')
  }

  getManagePresets (options) {
    return this.axios.get(`${APIs.GET_PRESETS_PAGINATE}` + options.options)
  }

  getAllBundles (options) {
    return this.axios.get(`${APIs.GET_ALL_BUNDLES}` + options.options)
  }

  addPreset (options) {
    return this.axios.post(`${APIs.GET_PRESET}`, options)
  }

  editPreset (id, options) {
    return this.axios.patch(`${APIs.GET_PRESET}` + '/' + id, options)
  }

  presetSearch (param) { // group search
    return this.axios.get(`${APIs.PRESETS_SEARCH}` + '?search=' + param.keyword)
  }

  getAllMaterials (options) { // for demo purposes, it needs pagination to work properly
    return this.axios.post(`${APIs.GET_MATERIALS}`, options)
  }  

  loadAllPresets (options) { // for demo purposes, it needs pagination to work properly
    return this.axios.post(`${APIs.LOAD_ALL_PRESETS}`, options)
  }

  loadPresetsSelected (options) {
    return this.axios.post(`${APIs.LOAD_PRESETS_SELECTED}`, options)
  }  

  getSelectedMaterials (options) {
    return this.axios.post(`${APIs.GET_SELECTED_MATERIALS}`, options)
  }  

  getBundlePresets (options) {
    return this.axios.post(`${APIs.GET_BUNDLE_PRESETS}`, options)
  }

  updatePresetMaterials (options) {
    return this.axios.post(`${APIs.UPDATE_PRESET_MATERIALS}`, options)
  }  

  updateBundlePresets (options) {
    return this.axios.post(`${APIs.UPDATE_BUNDLE_PRESETS}`, options)
  }

  addBundle (options) {
    return this.axios.post(`${APIs.ADD_BUNDLE}`, options)
  }

}

export default new Presets()
