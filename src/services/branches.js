/**
 * Branch services
 */

import Resource from './resource'
import APIs from './apis'

class Branches extends Resource {
  constructor () {
    super('branches')
  }

  getBranches (id, options) {
    return this.axios.get(`${APIs.GET_BRANCHES}`, options)
  }

  getBranch (id, options) {
    return this.axios.get(`${APIs.GET_BRANCHES}` + '/' + id, options)
  }

  addBranch (options) {
    return this.axios.post(`${APIs.GET_BRANCHES}`, options)
  }

  updateBranch (options) {
    return this.axios.patch(`${APIs.GET_BRANCHES}` + '/' + options.id, options)
  }

  deleteBranch (id) {
    return this.axios.delete(`${APIs.GET_BRANCHES}` + '/' + id)
  }
  
}

export default new Branches()
