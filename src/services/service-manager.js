/**
 * Service Manager
 */

import Resource from './resource'
import APIs from './apis'

class ServiceManger extends Resource {
  constructor () {
    super('service_manager')
  }

  getAllServices (id, options) { // for demo purposes, it needs pagination to work properly
    return this.axios.get(`${APIs.GET_SERVICES}`, options)
  }

  getAllParentServices (id, options) { // for demo purposes, it needs pagination to work properly
    return this.axios.get(`${APIs.GET_PARENT_SERVICES}`, options)
  }

  addService (options) {
    return this.axios.post(`${APIs.ADD_SERVICE}`, options)
  }

  getService (id, options) {
    return this.axios.get(`${APIs.GET_SERVICE}` + '/' + id, options)
  }

  getServiceRate (param) {
    return this.axios.get(`${APIs.GET_SERVICE_RATE}` + '/' + param.type + '/' + param.id + '/' + param.service_id)
  }

  updateService (options) {
    return this.axios.patch(`${APIs.SERVICE}` + '/' + options.id, options)
  }

  deleteService (id, options) {
    return this.axios.delete(`${APIs.DELETE_SERVICE}` + '/' + id, options)
  }

  // Delete Service Documents

  // Service Documents
  getAllDocuments (options) { // get all service documents
    return this.axios.get(`${APIs.GET_ALL_DOCUMENTS}`, options)
  }

  addDocument (options) { // POST method adding service documents
    return this.axios.post(`${APIs.ADD_SERVICE_DOCUMENTS}`, options)
  }

  getDocument (id, options) { // GET specific document
    return this.axios.get(`${APIs.GET_DOCUMENT}` + '/' + id, options)
  }

  updateDocument (options) { // Update/Edit a specific document
    return this.axios.patch(`${APIs.UPDATE_SERVICE_DOCUMENTS}` + '/' + options.id, options)
  }

  deleteDocument (id, options) { // Delete a specific document
    return this.axios.delete(`${APIs.DELETE_SERVICE_DOCUMENTS}` + '/' + id, options)
  }

  getMonthlySummary (options) {
    if (typeof options.perPage === 'undefined') {
      return this.axios.get(`${APIs.GET_MONTHLY_SUMMARY}` + '?' + options.page + '&' + options.sort + '&' + options.search + '&' + options.year + '&' + options.month)
    }

    return this.axios.get(`${APIs.GET_MONTHLY_SUMMARY}` + '/' + options.perPage + '?' + options.page + '&' + options.sort + '&' + options.search + '&' + options.year + '&' + options.month)
  }

  getMonthSummary(data) {
    return this.axios.post(`${APIs.GET_MONTH_SUMMARY}`, data);
  }

  getPendingServices (options) {
    if (typeof options.perPage === 'undefined') {
      return this.axios.get(`${APIs.GET_PENDING_SERVICES}` + '?' + options.page + '&' + options.sort + '&' + options.search)
    }

    return this.axios.get(`${APIs.GET_PENDING_SERVICES}` + '/' + options.perPage + '?' + options.page + '&' + options.sort + '&' + options.search)
  }

  getOnProcessServices (options) {
    if (typeof options.perPage === 'undefined') {
      return this.axios.get(`${APIs.GET_ON_PROCESS_SERVICES}` + '?' + options.page + '&' + options.sort + '&' + options.search)
    }

    return this.axios.get(`${APIs.GET_ON_PROCESS_SERVICES}` + '/' + options.perPage + '?' + options.page + '&' + options.sort + '&' + options.search)
  }

  getTodayServices (options) {
    if (typeof options.perPage === 'undefined') {
      return this.axios.get(`${APIs.GET_TODAY_SERVICES}` + '?' + options.page + '&' + options.sort + '&' + options.date + '&' + options.search)
    }

    return this.axios.get(`${APIs.GET_TODAY_SERVICES}` + '/' + options.perPage + '?' + options.page + '&' + options.sort + '&' + options.date + '&' + options.search)
  }

  getDashboardStatistics () { // GET specific document
    return this.axios.get(`${APIs.GET_DASHBOARD_STATISTICS}`)
  }

  getClientDocuments (options) {
    if (typeof options.perPage === 'undefined') {
      return this.axios.get(`${APIs.GET_CLIENT_DOCUMENTS}` + '?' + options.page + '&' + options.sort + '&' + options.search)
    }

    return this.axios.get(`${APIs.GET_CLIENT_DOCUMENTS}` + '/' + options.perPage + '?' + options.page + '&' + options.sort + '&' + options.search)
  }

  getClientDocumentDetails (id) {
    return this.axios.get(`${APIs.GET_CLIENT_DOCUMENTS}` + '/details/' + id)
  }

  updateClientDocuments (data, id) {
    return this.axios.patch(`${APIs.GET_CLIENT_DOCUMENTS}` + '/details/' + id, data)
  }

  addClientDocuments (data) {
    return this.axios.post(`${APIs.GET_CLIENT_DOCUMENTS}`, data)
  }

  // Cost, Charge and Tip Breakdowns
  saveBreakdowns (data) {
    return this.axios.post(`${APIs.SAVE_BREAKDOWNS}`, data)
  }

  updatePrice (data) {
    return this.axios.post(`${APIs.UPDATE_PRICE}`, data)
  }

  profileSwitch (options) {
    return this.axios.patch(`${APIs.PROFILE_SWITCH}`, options)
  }

  getServiceProfilesDetails (id, options) {
    return this.axios.get(`${APIs.GET_SERVICE}` + '/' + id + '/service-profiles-details', options)
  }

  getExpandedDetails (id, options) {
    return this.axios.get(`${APIs.GET_SERVICE}` + '/' + id + '/expanded-details', options)
  }
}

export default new ServiceManger()
