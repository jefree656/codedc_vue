/**
 * Rider Evaluation
 */

import Resource from "./resource";
import APIs from "./apis";

class RiderEvaluations extends Resource {
  constructor() {
    super("rider_evaluation");
  }

  updateQA(data) {
    return this.axios.post(`${APIs.UPDATE_QA}`, data);
  }

  addEvaluation(data) {
    return this.axios.post(`${APIs.ADD_EVALUATION}`, data);
  }

  updateEvaluation(data) {
    return this.axios.post(`${APIs.UPDATE_EVALUATION}`, data);
  }

  deleteEvaluation(data) {
    return this.axios.post(`${APIs.DELETE_EVALUATION}`, data);
  }

  getQA(data) {
    return this.axios.post(`${APIs.GET_QA}`, data);
  }

  getEvaluation(data) {
    return this.axios.post(`${APIs.GET_EVALUATION}`, data);
  }

  getEvaluationDay(options) {
    if (typeof options.perPage === "undefined") {
      return this.axios.get(
        `${APIs.GET_EVALUATION_DAY}` +
          "?" +
          options.page +
          "&" +
          options.sort +
          "&" +
          options.search +
          "&" +
          options.rider_id +
          "&" +
          options.date
      );
    }

    return this.axios.get(
      `${APIs.GET_EVALUATION_DAY}` +
        "/" +
        options.perPage +
        "?" +
        options.page +
        "&" +
        options.sort +
        "&" +
        options.search +
        "&" +
        options.rider_id +
        "&" +
        options.date
    );
  }

  getDailyEvaluationDetails(options) {
    return this.axios.get(
      `${APIs.GET_DAILY_EVALUATION_DETAILS}` +
        "/" +
        options.rider_id +
        "/" +
        options.date
    );
  }

  getEvaluationMonth(data) {
    return this.axios.post(`${APIs.GET_EVALUATION_MONTH}`, data);
  }

  getSummaryEvaluationHalfMonth(data) {
    return this.axios.post(`${APIs.GET_SUMMARY_EVALUATION_HALF_MONTH}`, data);
  }
}

export default new RiderEvaluations();
