/**
 * Clients services
 */

import Resource from './resource'
import APIs from './apis'

class Clients extends Resource {
  constructor () {
    super('clients')
  }

  getAllClients (options) { // for demo purposes, it needs pagination to work properly
    return this.axios.get(`${APIs.GET_CLIENTS}`, options)
  }

  getAllClientsPaginate (options) { // client list with pagination
    if (typeof options.perPage === 'undefined') {
      return this.axios.get(`${APIs.GET_CLIENTS_PAGINATE}` + '?' + options.page + '&' + options.sort+ '&' + options.search + '&' + options.withActiveServiceOnly)
    }

    if(typeof options.from === 'undefined'){
        options.from = '';
    }

    return this.axios.get(`${APIs.GET_CLIENTS_PAGINATE}` + '/' + options.perPage + '?' + options.page + '&' + options.sort + '&' + options.search + '&' + options.withActiveServiceOnly + '&' + options.from)
  }

  getClientProfile (id, options) { // client profile
    return this.axios.get(`${APIs.GET_CLIENTS_PROFILE}` + '/' + id.id, options)
  }

  clientSearch (param) { // client search
    return this.axios.get(`${APIs.CLIENTS_SEARCH}` + '?search=' + param.keyword +'&branch_id=' + param.branch_id +  '&is_member_search=' + ((typeof param.is_member_search !== 'undefined') ? String(param.is_member_search) : false))
  }

  comSearch(param){
     return this.axios.get(`${APIs.CLIENTS_COM_SEARCH}` + '?search=' + param.keyword +'&branch_id=' + param.branch_id +  '&is_member_search=' + ((typeof param.is_member_search !== 'undefined') ? String(param.is_member_search) : false))
  }

  addClient (options) { // POST method
    return this.axios.post(`${APIs.ADD_CLIENT}`, options)
  }

  updateClient (options) { // POST method
    return this.axios.patch(`${APIs.ADD_CLIENT}` + '/' + options.id , options.model)
  }

  getClientServices (param, options) { // get all client services if tracking is 0
    return this.axios.get(`${APIs.GET_CLIENTS_SERVICES}` + '/' + param.id + '/' + param.tracking, options)
  }

  getClientPackages (id , options) { // get all client packages
    return this.axios.get(`${APIs.GET_CLIENTS_PACKAGES}` + '/' + id.id , options)
  }

  getClientGroups (id , options) { // get all client packages
    return this.axios.get(`${APIs.GET_CLIENT_GROUPS}` + '/' + id.id , options)
  }

  getAllUsers () { // get all clients
    return this.axios.get(`${APIs.GET_ALL_USERS}`)
  }

  getContactType () { // get all clients
    return this.axios.get(`${APIs.GET_CONTACT_TYPE}`)
  }

  addTemporaryClient(options) { // POST method add temporary client
    return this.axios.post(`${APIs.ADD_TEMPORARY_CLIENT}`, options)
  }

  addClientService(options) { // POST method add client service
    return this.axios.post(`${APIs.ADD_CLIENT_SERVICE}`, options)
  }

  editClientService(options) { // POST method edit client service
    return this.axios.post(`${APIs.EDIT_CLIENT_SERVICE}`, options)
  }

  addClientPackage(options) { // POST method create new individual package
    return this.axios.post(`${APIs.ADD_CLIENT_PACKAGE}`, options)
  }

  deleteClientPackage(options) { // POST method delete package
    return this.axios.post(`${APIs.DELETE_CLIENT_PACKAGE}`, options)
  }

  addClientFund(options) { // POST method add client funds
    return this.axios.post(`${APIs.ADD_CLIENT_FUND}`, options)
  }

  updateRisk(options) { // POST method edit client service
    return this.axios.patch(`${APIs.ADD_CLIENT}` + '/' + options.id + '/update-risk', options.data)
  }

  getTodayTasks (options) { // GET today's tasks
    if (typeof options.perPage === 'undefined') {
      return this.axios.get(`${APIs.GET_TODAY_TASKS}` + '?' + options.page + '&' + options.sort + '&' + options.viewAll + '&' + options.search)
    }

    return this.axios.get(`${APIs.GET_TODAY_TASKS}` + '/' + options.perPage + '?' + options.page + '&' + options.sort + '&' + options.viewAll + '&' + options.search)
  }

  getPastTasks (options) { // GET today's tasks
    if (typeof options.perPage === 'undefined') {
      return this.axios.get(`${APIs.GET_PAST_TASKS}` + '?' + options.page + '&' + options.sort + '&' + options.search)
    }

    return this.axios.get(`${APIs.GET_PAST_TASKS}` + '/' + options.perPage + '?' + options.page + '&' + options.sort + '&' + options.search)
  }

  getTomorrowTasks (options) { // GET tomorrow's tasks
    if (typeof options.perPage === 'undefined') {
      return this.axios.get(`${APIs.GET_TOMORROW_TASK}` + '?' + options.page + '&' + options.sort + '&' + options.search)
    }

    return this.axios.get(`${APIs.GET_TOMORROW_TASK}` + '/' + options.perPage + '?' + options.page + '&' + options.sort + '&' + options.search)
  }

  addTomorrowTasks (options) {
    return this.axios.post(`${APIs.ADD_TOMORROW_TASK}`, options)
  }

  updatePastTasks (options) {
    return this.axios.post(`${APIs.UPDATE_PAST_TASK}`, options)
  }

  getEmployees () { // GET employees
    return this.axios.post(`${APIs.GET_EMPLOYEES}`)
  }

  getReminders (options) { // GET Reminders
    if (typeof options.perPage === 'undefined') {
      return this.axios.get(`${APIs.GET_REMINDERS}` + '?' + options.page + '&' + options.sort + '&' + options.search)
    }

    return this.axios.get(`${APIs.GET_REMINDERS}` + '/' + options.perPage + '?' + options.page + '&' + options.sort + '&' + options.search)
  }

  getUnpaidService (param) {
      if (typeof param.perPage === 'undefined') {
        return this.axios.get(`${APIs.UNPAID_SERVICES}` + '/' +  param.client_id  + '/' + param.is_auto_generated + '?' + param.page + '&' + param.sort + '&' + param.search)
      }
      return this.axios.get(`${APIs.UNPAID_SERVICES}`+ '/' +  param.client_id + '/' + param.is_auto_generated + '/' + param.perPage + '?' + param.page + '&' + param.sort + '&' + param.search)
  }

  addClientServicePayment (options) {
    return this.axios.post(`${APIs.ADD_CLIENT_SERVICE_PAYMENT}`, options)
  }

  switchClientServiceCost (options) {
    return this.axios.get(`${APIs.SWITCH_CLIENT_SERVICE_COST}`+ '/' + options.client_id + '/' + options.service_profile_id)
  }

  getClientsByIds (options) {
    return this.axios.post(`${APIs.GET_CLIENTS_BY_IDS}`, options)
  }

  addClientsRemark (options) {
    return this.axios.post(`${APIs.ADD_CLIENTS_REMARK}`, options)
  }

  getClientsRemarks (options) {
    return this.axios.get(`${APIs.GET_CLIENTS_REMARKS}/${options}`)
  }

  getPayServices (options) {
    return this.axios.get(`${APIs.GET_PAY_SERVICES}/${options}`)
  }

  addClientPayment(options) { // Add Group Payment
    return this.axios.post(`${APIs.ADD_CLIENT_PAYMENT}`, options)
  }

  getQRData(options) {
    return this.axios.post(`${APIs.GET_QR_DATA}`, options)
  }

  updatePassportMonitor(options) {
    return this.axios.post(`${APIs.UPDATE_PASSPORT_MONITOR}`, options)
  }
}

export default new Clients()
