/**
 * Financing
 */

import Resource from './resource'
import APIs from './apis'

class Financing extends Resource {
  constructor () {
    super('financing')
  }

  getBanks (options) { // get banks
    return this.axios.get(`${APIs.GET_BANKS}` + options.options)
  }

  addBank (options) {
    return this.axios.post(`${APIs.ADD_BANK}`, options)
  }

  editBank (id, options) {
    return this.axios.patch(`${APIs.EDIT_BANK}` + '/' + id, options)
  }

  addQuotation (options) {
    return this.axios.post(`${APIs.ADD_QUOTATION}`, options)
  }

  editQuotation (id, options) {
    return this.axios.patch(`${APIs.EDIT_QUOTATION}` + '/' + id, options)
  }

  getProjectCollectables (param) {
      return this.axios.get(`${APIs.GET_PROJECT_COLLECTABLES}`+ '?' + param.page + '&' + param.sort + '&' + param.search + '&'  + param.project_id+ '&'  + param.filter)
  }  

  getFinancing (date,branch_id) { // get financing
    return this.axios.get(`${APIs.GET_FINANCING}`+ '/' +  date + '/' + branch_id)
  }

  getBorrowed (trans_type,branch_id) { // get borrowed
    return this.axios.get(`${APIs.GET_BORROWED}`+ '/' +  trans_type + '/' + branch_id)
  }

  getReqUsers () { // get req users
    return this.axios.get(`${APIs.GET_REQ_USERS}`)
  }

  addFinance (options) {
    return this.axios.post(`${APIs.ADD_FINANCE}`, options)
  }

  updateFinance (id, options) {
    return this.axios.patch(`${APIs.ADD_FINANCE}` + '/' + id, options)
  }

  // financing delivery
  getFinancingDelivery (date) { // get financing delivery
    return this.axios.get(`${APIs.GET_FINANCING_DELIVERY}`+ '/' +  date)
  }

  addPurchasingBudget (options) {
    return this.axios.post(`${APIs.ADD_PURCHASING_BUDGET}`, options)
  }

  addDeliveryFinance (options) {
    return this.axios.post(`${APIs.ADD_DELIVERY_FINANCE}`, options)
  }

  updateDeliveryFinance (id, options) {
    return this.axios.patch(`${APIs.UPDATE_DELIVERY_FINANCE}` + '/' + id, options)
  }

  updateDeliveryRow (id, options) {
    return this.axios.patch(`${APIs.UPDATE_DELIVERY_ROW}` + '/' + id, options)
  }

  deleteDeliveryRow (id, options) {
    return this.axios.delete(`${APIs.DELETE_DELIVERY_ROW}` + '/' + id, options)
  }

  getReturnList (trans_type) { // get borrowed
    return this.axios.get(`${APIs.GET_RETURN_LIST}`+ '/' +  trans_type )
  }

  exportFinancial (options) {
    const config = {
      method: 'post',
      responseType: 'blob',
      data: options,
      headers: { 'Content-Type': 'application/json;charset=UTF-8'},
      url: `${APIs.EXPORT_FINANCIAL}`,
      xsrfCookieName: 'XSRF-TOKEN',
      xsrfHeaderName: 'X-XSRF-TOKEN',
    };
    return this.axios(config)
  }

}

export default new Financing()
