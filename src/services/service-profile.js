import Resource from './resource'
import APIs from './apis'

class ServiceProfile extends Resource {
  constructor () {
    super('service_profiles')
  }

  getAllServiceProfiles (id, options) {
    return this.axios.get(`${APIs.SERVICE_PROFILE}`, options)
  }

  getServiceProfile (id, options) {
    return this.axios.get(`${APIs.SERVICE_PROFILE}` + '/' + id, options)
  }

  addServiceProfile (options) {
    return this.axios.post(`${APIs.SERVICE_PROFILE}`, options)
  }

  updateServiceProfile (options) {
    return this.axios.patch(`${APIs.SERVICE_PROFILE}` + '/' + options.id, options)
  }

  deleteServiceProfile (id, options) {
    return this.axios.delete(`${APIs.SERVICE_PROFILE}` + '/' + id, options)
  }

  getClientsGroups (id) {
    return this.axios.get(`${APIs.GET_CLIENTS_GROUPS}` + '/' + id)
  }
}

export default new ServiceProfile()
