/**
 * Branch services
 */

import Resource from './resource'
import APIs from './apis'

class Attendance extends Resource {
  constructor () {
    super('attendance')
  }

  getAllAttendance (options) {
      return this.axios.get(`${APIs.GET_ALL_ATTENDANCE}` + '?' + options.month + '&&' + options.year+ '&&' + options.project_id)
  }

  generatePayroll (options) {
      return this.axios.get(`${APIs.GENERATE_PAYROLL}` + '?' + options.month + '&&' + options.year+ '&&' + options.project_id)
  }

  computePayroll (options) {
      return this.axios.get(`${APIs.COMPUTE_PAYROLL}` + '?' + options.month + '&&' + options.year+ '&&' + options.project_id)
  }

  testKernel (options) {
      return this.axios.post(`${APIs.TEST_KERNEL}`, options)
  }
  
}

export default new Attendance()
