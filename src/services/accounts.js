/**
 * Accounts services
 */

import Resource from './resource'
import APIs from './apis'

class Accounts extends Resource {
  constructor () {
    super('accounts')
  }

  // CPANEL ACCOUNTS
  getCpanelUsers (options) {
    return this.axios.get(`${APIs.GET_ALL_CPANEL_USERS}`, options)
  }

  addCpanelUser (options) { // POST method adding cpanel user
    return this.axios.post(`${APIs.ADD_CPANEL_USER}`, options)
  }

  getCpanelUser (id, options) { // get specific user
    return this.axios.get(`${APIs.GET_CPANEL_USER}` + '/' + id, options)
  }

  updateCpanelUser (data, id) { // edit cpanel user
    return this.axios.patch(`${APIs.UPDATE_CPANEL_USER}` + '/' + id, data)
  }

  deleteCpanelUser (id, options) { // delete specific user
    return this.axios.delete(`${APIs.DELETE_CPANEL_USER}` + '/' + id, options)
  }

  getWorker (id, options) { // get specific worker
    return this.axios.get(`${APIs.GET_WORKER}` + '/' + id, options)
  }

  updateWorker (data, id) { // edit worker
    return this.axios.patch(`${APIs.UPDATE_WORKER}` + '/' + id, data)
  }  

  addNewWorker (data) { // add new worker
    return this.axios.post(`${APIs.ADD_NEW_WORKER}`, data)
  }

  getAllWorkers (options) { 
    return this.axios.get(`${APIs.GET_ALL_WORKERS}`, options)
  }

  saveWorkerAttendance (options) { // POST method adding cpanel user
    return this.axios.post(`${APIs.SAVE_WORKER_ATTENDANCE}`, options.payload)
  }   

  saveWorkerAttendanceMultiple (options) { // POST method adding cpanel user
    return this.axios.post(`${APIs.SAVE_WORKER_ATTENDANCE_MULTIPLE}`, options.payload)
  }  

  getDayAttendance (options) { // POST method adding cpanel user
    return this.axios.post(`${APIs.GET_DAY_ATTENDANCE}`, options.payload)
  }
}

export default new Accounts()
