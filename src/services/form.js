import Resource from './resource'
import APIs from './apis'

class Form extends Resource {
  constructor () {
    super('form')
  }

  getForms (id, options) {
    return this.axios.get(`${APIs.FORM}`, options)
  }
  
}

export default new Form()
