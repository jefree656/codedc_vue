/**
 * Departments services
 */

import Resource from './resource'
import APIs from './apis'

class Departments extends Resource {
  constructor () {
    super('departments')
  }

  getAllDepartments(){
    return this.axios.get(`${APIs.GET_ALL_DEPARTMENTS}`)
  }
  
}

export default new Departments()
