/**
 * Branch services
 */

import Resource from './resource'
import APIs from './apis'

class Actions extends Resource {
  constructor () {
    super('actions')
  }

  getActions (id, options) {
    return this.axios.get(`${APIs.GET_ACTIONS}`, options)
  }
  
}

export default new Actions()
