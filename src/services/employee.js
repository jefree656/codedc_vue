/**
 * Group services
 */

import Resource from './resource'
import APIs from './apis'

class Employee extends Resource {
  constructor () {
    super('employee')
  }

  getEmployeeDocsOnHand (options) {
    let filter= "";
    if(options.search){
      filter = "?search="+options.search;
    }
    return this.axios.get(`${APIs.GET_EMPLOYEE_ON_HAND_DOCUMENTS}${filter}`)
  }

  getClientsInDocsOnHand (options) {
    let filter= "";
    if(options.search){
      filter = "?search="+options.search;
    }
    return this.axios.get(`${APIs.GET_CLIENTS_ON_HAND_DOCUMENTS}${filter}`)
  }

  exportEmployeeDocsOnHand (options) {
    //return this.axios.post(`${APIs.EXPORT_EMPLOYEE_ON_HAND_DOCUMENTS}`, options)
    const config = {
      method: 'post',
      responseType: 'blob',
      data: options,
      headers: { 'Content-Type': 'application/json;charset=UTF-8'},
      url: `${APIs.EXPORT_EMPLOYEE_ON_HAND_DOCUMENTS}`,
      xsrfCookieName: 'XSRF-TOKEN',
      xsrfHeaderName: 'X-XSRF-TOKEN',
    };

    return this.axios(config)
  }

}

export default new Employee()
