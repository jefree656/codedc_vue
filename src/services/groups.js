/**
 * Group services
 */

import Resource from './resource'
import APIs from './apis'

class Groups extends Resource {
  constructor () {
    super('groups')
  }

  getManageGroups (options) {
    return this.axios.get(`${APIs.GET_GROUPS_PAGINATE}` + options.options)
  }

  getGroupProfile (id) {
    return this.axios.get(`${APIs.GET_GROUP}` + '/' + id)
  }

  addGroup (options) {
    return this.axios.post(`${APIs.GET_GROUP}`, options)
  }

  editGroup (id, options) {
    return this.axios.patch(`${APIs.GET_GROUP}` + '/' + id, options)
  }

  addGroupMembers (options) {
    return this.axios.post(`${APIs.ADD_GROUP_MEMBERS}`, options)
  }

  addGroupService(options) { // POST method add client service
    return this.axios.post(`${APIs.ADD_GROUP_SERVICES}`, options)
  }

  editGroupCommission (options) {
    return this.axios.patch(`${APIs.UPDATE_GROUP_COMMISSION}`+ '/' + options.id, options)
  }

  assignGroupRole (options) {
    return this.axios.post(`${APIs.ASSIGN_GROUP_ROLE}`, options)
  }

  getGroupByMembers (param) {
    if (typeof param.perPage === 'undefined') {
      return this.axios.get(`${APIs.GET_MEMBERS_BY_GROUP}` + '/' +  param.id  + '?' + param.page + '&' + param.sort + '&' + param.search + '&' + param.ids + '&' + param.from)
    }
    return this.axios.get(`${APIs.GET_MEMBERS_BY_GROUP}`+ '/' +  param.id + '/' + param.perPage + '?' + param.page + '&' + param.sort + '&' + param.search + '&' + param.ids + '&' + param.from)
  }

  getGroupMembers (param) {
    if (typeof param.perPage === 'undefined') {
      return this.axios.get(`${APIs.GET_GROUP_MEMBERS}` + '/' +  param.id  + '?' + param.page + '&' + param.sort + '&' + param.search)
    }
    return this.axios.get(`${APIs.GET_GROUP_MEMBERS}`+ '/' +  param.id + '/' + param.perPage + '?' + param.page + '&' + param.sort + '&' + param.search)
  }


  addFunds (options) {
    return this.axios.post(`${APIs.ADD_GROUP_FUNDS}`, options)
  }

  getGroupFunds (param) {
    return this.axios.get(`${APIs.GET_GROUP_FUNDS}`+ '/' +  param.group_id + '/' + param.perPage + '?' + param.page)
  }

  getGroupMemberPackages (param) {
    return this.axios.get(`${APIs.GET_GROUP_MEMBER_PACKAGES}`+ '/' +  param.client_id + '/' + param.group_id)
  }

  getMembersPackages (param) {
    if (typeof param.perPage === 'undefined') {
      return this.axios.get(`${APIs.GET_CLIENTS_PAGINATE}` + '?' + param.page + '&' + param.sort+ '&' + param.search)
    }
    return this.axios.get(`${APIs.GET_MEMBERS_AND_PACKAGES}` + '/' +  param.group_id + '/' + param.perPage + '?' + param.page + '&' + param.sort + '&' + param.search)
  }

  //FOR BY BATCH TAB
  getGroupByBatch (param) {
      if (typeof param.perPage === 'undefined') {
        return this.axios.get(`${APIs.GET_GROUP_MEMBER_PACKAGES_BYBATCH}` + '/' +  param.group_id  + '?' + param.page + '&' + param.sort + '&' + param.search)
      }
      return this.axios.get(`${APIs.GET_GROUP_MEMBER_PACKAGES_BYBATCH}`+ '/' +  param.group_id + '/' + param.perPage + '?' + param.page + '&' + param.sort + '&' + param.search)
  }

  getCurrentBatchPage (param) {
      return this.axios.get(`${APIs.GET_CURRENT_BATCH_PAGE}`+ '/' +  param.group_id +  '?' + param.client_id + '&' + param.clientServiceId)
  }


  //FOR BY BATCH MEMBERS
  getMemberByBatch (options) {
    return this.axios.post(`${APIs.BY_BATCH_MEMBER}`, options)
  }

  getMemberByService(options) {
    return this.axios.post(`${APIs.BY_MEMBERS_SERVICE}`, options)
  }

  getServicesByMember(options) {
    return this.axios.post(`${APIs.BY_SERVICE_MEMBER}`, options)
  }
 //END


 distributePayment(options) {
   return this.axios.post(`${APIs.DISTRIBUTE_PAYMENT}`, options)
 }


 //FOR EXPORT
  getByBatch (param) {
      return this.axios.get(`${APIs.GET_GROUP_MEMBER_BYBATCH}`+ '/' +  param.group_id + '/' + param.perPage + '?' + param.page + '&' + param.sort + '&' + param.search + '&' + param.lang + '&' + param.dates)
  }




  editGroupServices (options) {
      return this.axios.patch(`${APIs.UPDATE_GROUP_SERVICES}`, options)
  }


  getGroupByService (param) {
      if (typeof param.perPage === 'undefined') {
        return this.axios.get(`${APIs.GET_GROUP_MEMBER_PACKAGES_BYSERVICE}` + '/' +  param.group_id  + '?' + param.page + '&' + param.sort + '&' + param.search)
      }
      return this.axios.get(`${APIs.GET_GROUP_MEMBER_PACKAGES_BYSERVICE}`+ '/' +  param.group_id + '/' + param.perPage + '?' + param.page + '&' + param.sort + '&' + param.search)
  }

  getUnpaidService (param) {
      if (typeof param.perPage === 'undefined') {
        return this.axios.get(`${APIs.GET_UNPAID_SERVICES}` + '/' +  param.group_id  + '/' + param.is_auto_generated + '?' + param.page + '&' + param.sort + '&' + param.search)
      }
      return this.axios.get(`${APIs.GET_UNPAID_SERVICES}`+ '/' +  param.group_id + '/' + param.is_auto_generated + '/' + param.perPage + '?' + param.page + '&' + param.sort + '&' + param.search)
  }


  addGroupServicePayment (options) {
    return this.axios.post(`${APIs.ADD_GROUP_SERVICE_PAYMENT}`, options)
  }

  editGroupServicePayment(options) {
    return this.axios.post(`${APIs.EDIT_GROUP_SERVICE_PAYMENT}`, options)
  }


  getGroupServicesByProfile(param){
      return this.axios.get(`${APIs.GET_GROUP_SERVICES_BY_PROFILE}`+ '/' +  param.group_id + '/' + param.branch_id)
  }

  groupSearch (param) { // group search
    return this.axios.get(`${APIs.GROUPS_SEARCH}` + '?search=' + param.keyword)
  }

  deleteGroup (id) {
    return this.axios.delete(`${APIs.GET_BRANCHES}` + '/' + id)
  }

  deleteGroupMember (options) { // Delete group member
    return this.axios.post(`${APIs.DELETE_GROUP_MEMBER}`, options)
  }

  getAllGroups (options) {
    return this.axios.get(`${APIs.GET_MANAGE_GROUPS}`, options)
  }

  getGroupMemberServices (options) {
    return this.axios.post(`${APIs.GET_GROUP_MEMBER_SERVICES}`, options)
  }

  transferService (options) {
    return this.axios.post(`${APIs.TRANSFER_SERVICES}`, options)
  }

  transferMember (options) {
    return this.axios.post(`${APIs.TRANSFER_MEMBER}`, options)
  }

  checkIfMemberExist (options) {
    return this.axios.post(`${APIs.CHECK_IF_MEMBER_EXIST}`, options)
  }



  addGroupPayment(options) { // Add Group Payment
    return this.axios.post(`${APIs.ADD_GROUP_PAYMENT}`, options)
  }

  switchBranch (options) {
    return this.axios.get(`${APIs.SWITCH_GROUP_BRANCH}`+ '/' + options.group_id + '/' + options.branch_id)
  }

  switchServiceCost (options) {
    return this.axios.get(`${APIs.SWITCH_SERVICE_COST}`+ '/' + options.group_id + '/' + options.service_profile_id)
  }

  updateRisk (options){
      return this.axios.patch(`${APIs.UPDATE_GROUP_RISK}` + '/' + options.id + '/update-risk', options.data)
  }

  getServiceDates (options){
      return this.axios.get(`${APIs.GET_SERVICE_DATES}` + '/' + options.id)
  }

  getServiceAdded (options){
      return this.axios.get(`${APIs.GET_SERVICE_ADDED}` + '/' + options.id+ '/' + options.date + '?' + options.from_date + '&' + options.to_date)
  }

  addGroupRemark (options){
    return this.axios.post(`${APIs.ADD_GROUP_REMARK}`, options)
  }

  getGroupHistory (options){
    return this.axios.post(`${APIs.GET_GROUP_HISTORY}`, options)
  }

  getGroupDocumentLogs (options){
    return this.axios.get(`${APIs.GET_GROUP_DOCUMENT_LOGS}` + '/' + options)
  }

  getGroupDocumentsOnhand (options){
    return this.axios.get(`${APIs.GET_GROUP_DOCUMENTS_ONHAND}` + '/' + options)
  }

  getGroupServices(options){
    return this.axios.post(`${APIs.GET_GROUP_SERVICES}`, options)
  }

  exportExcelGroupSummary (options) {

    const config = {
     method: 'post',
     responseType: 'blob',
     data: options,
     //responseType: 'application/vnd.ms-excel',
     headers: { 'Content-Type': 'application/json;charset=UTF-8'},
     url: `${APIs.EXPORT_EXCEL_GROUP_SUMMARY}`,
     xsrfCookieName: 'XSRF-TOKEN',
     xsrfHeaderName: 'X-XSRF-TOKEN',
    };

    return this.axios(config)
  }

  getByService (param) {
      if (typeof param.perPage === 'undefined') {
        return this.axios.get(`${APIs.GET_GROUP_MEMBER_BYSERVICE}` + '/' +  param.group_id  + '?' + param.page + '&' + param.sort + '&' + param.search)
      }
      return this.axios.get(`${APIs.GET_GROUP_MEMBER_BYSERVICE}`+ '/' +  param.group_id + '/' + param.perPage + '?' + param.page + '&' + param.ids + '&' + param.sort + '&' + param.search + '&' + param.from_date + '&' + param.to_date + '&' +  param.lang + '&' +  param.status)
  }

  previewReport(options){
    //  return this.axios.get(`${APIs.PREVIEW_REPORT}` + '/' + options.id + '?' + options.lang + '&' + options.type + '&' + options.data)

      const config = {
       method: 'post',
       //responseType: 'blob',
       data: options,
       url: `${APIs.PREVIEW_REPORT}`,
       xsrfCookieName: 'XSRF-TOKEN',
       xsrfHeaderName: 'X-XSRF-TOKEN',
      };

      return this.axios(config)

  }

  getFundList (options){
    return this.axios.post(`${APIs.GET_FUND_LIST}`, options)
  }

  exportFundsSummary (options) {
    const config = {
      method: 'post',
      responseType: 'blob',
      data: options,
      headers: { 'Content-Type': 'application/json;charset=UTF-8'},
      url: `${APIs.EXPORT_PDF_FUNDS_SUMMARY}`,
      xsrfCookieName: 'XSRF-TOKEN',
      xsrfHeaderName: 'X-XSRF-TOKEN',
    };

    return this.axios(config)
  }


}

export default new Groups()
