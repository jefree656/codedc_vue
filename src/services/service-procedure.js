import Resource from './resource'
import APIs from './apis'

class ServiceProcedure extends Resource {
  constructor () {
    super('service_procedure')
  }

  getServiceProcedures (id, options) {
    return this.axios.get(`${APIs.SERVICE_PROCEDURE}/service` + '/' + id, options)
  }

  getServiceProcedure (id, options) {
    return this.axios.get(`${APIs.SERVICE_PROCEDURE}` + '/' + id, options)
  }

  addServiceProcedure (options) {
    return this.axios.post(`${APIs.SERVICE_PROCEDURE}`, options)
  }

  updateServiceProcedure (options) {
    return this.axios.patch(`${APIs.SERVICE_PROCEDURE}` + '/' + options.id, options)
  }

  deleteServiceProcedure (id) {
    return this.axios.delete(`${APIs.SERVICE_PROCEDURE}` + '/' + id)
  }

  sortServiceProcedures (options) {
    return this.axios.post(`${APIs.SERVICE_PROCEDURE}/sort`, options)
  }
  
}

export default new ServiceProcedure()
