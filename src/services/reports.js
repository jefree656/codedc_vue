/**
 * Clients services
 */

import Resource from './resource'
import APIs from './apis'

class Reports extends Resource {
  constructor () {
    super('reports')
  } 

  plReports (options) {
    return this.axios.get(`${APIs.PL_REPORTS}` + '/' + options.perPage + '?' + options.page + '&' + options.sort + '&' + options.category + '&' + options.project + '&' + options.date + '&' + options.period + '&' + options.mode + '&' + options.pricing + '&' + options.matview)
  }

  materialReports (options) {
    return this.axios.get(`${APIs.MATERIAL_REPORTS}` + '/' + options.perPage + '?' + options.page + '&' + options.sort + '&' + options.project)
  }

  getPLDetails(payload) { 
    return this.axios.post(`${APIs.GET_PL_DETAILS}`, payload.payload)
  }   

  getPLDetails2(payload) { 
    return this.axios.post(`${APIs.GET_PL_DETAILS2}`, payload.payload)
  } 

  exportPL (options) {
    const config = {
     method: 'post',
     responseType: 'blob',
     data: options,
     //responseType: 'application/vnd.ms-excel',
     headers: { 'Content-Type': 'application/json;charset=UTF-8'},
     url: `${APIs.EXPORT_PL_REPORT}`,
     xsrfCookieName: 'XSRF-TOKEN',
     xsrfHeaderName: 'X-XSRF-TOKEN',
    };

    return this.axios(config)
    //return this.axios.get(`${APIs.EXPORT_POC}`+ '?' + param.ids)
  }

  exportMaterialReport (options) {
    const config = {
     method: 'post',
     responseType: 'blob',
     data: options,
     //responseType: 'application/vnd.ms-excel',
     headers: { 'Content-Type': 'application/json;charset=UTF-8'},
     url: `${APIs.EXPORT_MATERIAL_REPORT}`,
     xsrfCookieName: 'XSRF-TOKEN',
     xsrfHeaderName: 'X-XSRF-TOKEN',
    };

    return this.axios(config)
    //return this.axios.get(`${APIs.EXPORT_POC}`+ '?' + param.ids)
  }

}

export default new Reports()
