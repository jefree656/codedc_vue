/**
 * Inventories
 */

import Resource from './resource'
import APIs from './apis'

class Companies extends Resource {
  constructor () {
    super('company')
  }

  getCompany (data) {
    return this.axios.get(`${APIs.GET_COMPANY}`, data)
  }

  addCompany (data) {
    return this.axios.post(`${APIs.ADD_COMPANY}`, data)
  }

  editCompany (data) {
    return this.axios.post(`${APIs.EDIT_COMPANY}`, data)
  }

  deleteCompany (data) {
    return this.axios.post(`${APIs.DELETE_COMPANY}`, data)
  }



}

export default new Companies()
