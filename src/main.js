window.moment = require('moment');

import Vue from 'vue'

import element from 'element-ui'
import locale from 'element-ui/lib/locale/lang/en'
import VueZoomer from 'vue-zoomer'
import vueTabevents from 'vue-tabevents';
import VueHtmlToPaper from 'vue-html-to-paper';
import GanttElastic from "gantt-elastic";
import GanttHeader from "gantt-elastic-header";
import VueEasyLightbox from 'vue-easy-lightbox';
import VueScheduler from 'v-calendar-scheduler';
// import { sync } from 'vuex-router-sync'

import App from './app'
import i18n from './i18n'
import store from './store'
import router from './router'
import plugins from './plugins'

/**
 * Import styles
 */

import './assets/styles/element.scss'
import './assets/styles/main.scss'
import 'v-calendar-scheduler/lib/main.css';
/**
 * Use plugins
 */

Vue.use(element, { locale })
Vue.use(plugins)
Vue.use(VueZoomer)
Vue.use(vueTabevents);
Vue.use(GanttElastic);
Vue.use(GanttHeader);
Vue.use(VueEasyLightbox);
Vue.use(VueScheduler);
// sync(store, router, { moduleName: 'route' })

const options = {
  name: '_blank',
  specs: [
    'fullscreen=yes',
    'titlebar=yes',
    'scrollbars=yes'
  ],
  styles: [
     'https://unpkg.com/element-ui/lib/theme-chalk/index.css',
     'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css',
    './assets/styles/print.css'
  ]


}

Vue.use(VueHtmlToPaper, options);

// or, using the defaults with no stylesheet
Vue.use(VueHtmlToPaper);


/**
 * Config
 */

Vue.config.debug = process.env.NODE_ENV === 'development'
Vue.config.silent = process.env.NODE_ENV === 'production'
Vue.config.devtools = true
Vue.config.productionTip = false



/**
 * Initial
 */

// ...

// Filters

Vue.filter('capitalize', function (value) {
  if (!value) return ''
  value = value.toString()
  return value.charAt(0).toUpperCase() + value.slice(1)
})

Vue.filter('invoiceFormat', function(value) {
  if (value) {
    return moment(String(value)).format("YYYY, MMMM Do");
  }
});

Vue.filter('timeSince', function(value) {
  if (value) {
    return moment(value).fromNow();
  }
});

Vue.filter('age', function(value) {
  if (value) {
    return moment().diff(value, 'years');
  }
  return '--';
});

Vue.filter('currency', function (value) {
  if(value!=null){
    if(value%1 != 0){
       return  numberWithCommas(parseFloat(value).toFixed(2));
    }
    else{
       return  numberWithCommas(parseInt(value));
    }
  }
  else if(value==null){
    return null;
  }
  else{
    return 0;
  }
});

Vue.filter('volume', function (value) {
  if(value!=null){
    if(value%1 != 0){
       return  parseFloat(value).toFixed(7);
    }
    else{
       return  parseInt(value);
    }
  }
  else if(value==null){
    return null;
  }
  else{
    return 0;
  }
});

Vue.filter('titleCase', function (value) {
  if ((value===null) || (value==='')){
    return false;
  }else{
    value = value.toString();
    return value.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
  }
});

Vue.filter('commonDate', function(value) {
  if (value) {
    return moment(String(value)).format("MMM DD, YYYY");
  }
});

Vue.filter('commonDateWithTime', function(value) {
  if (value) {
    return moment(String(value)).format("MMM DD, YYYY , hh:mm A");
  }
});

window.numberWithCommas = function(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

// End Filters

/**
 * Root app
 */


const app = new Vue({
  name: 'root',
  i18n: i18n,
  store: store,
  router: router,
  render: h => h(App)
})

/**
 * Mount to `#app` element
 */

app.$mount('#app')
