import Vue from 'vue'
import I18n from 'vue-i18n'
import locale from 'element-ui/lib/locale'

Vue.use(I18n)

// https://kazupon.github.io/vue-i18n/dynamic.html
const i18n = new I18n({
  locale: 'en',
  messages: {
    en: require('./locales/en'),
    cn: require('./locales/cn')
  }
})

// locale.i18n((key, value) =>i18n.t(key, value));
export default i18n
