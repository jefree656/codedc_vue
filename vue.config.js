/**
 * Override convention configuration
 * https://cli.vuejs.org/config/
 */

module.exports = {
  publicPath: '',
  assetsDir: 'assets',
  lintOnSave: false,
  productionSourceMap: false,
  css: {
    // extract: true,
    sourceMap: true
  },
  chainWebpack: config => {
    config.module.rules.delete('eslint')
  },
  devServer: {
    // proxy: 'http://localhost:8082',
    open: false,
    host: 'localhost',
    port: 8082,
    https: false,
    before: app => { }
  }
}
